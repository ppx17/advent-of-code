module Day3

include("Aoc.jl")
using .Aoc

input = Aoc.input_lines(3)
prio  = c -> Int(c) |> n -> n - (n >= 97 ? 96 : 38)
line  = l -> intersect(l[begin:length(l)÷2], l[1+length(l)÷2:end])[1] |> prio
group = g -> intersect(g...)[1] |> prio
part1 = () -> line.(input) |> sum
part2 = () -> group.([input[i:i+2] for i in 1:3:length(input)]) |> sum

end