#!/bin/bash

DAY=$(date +'%-d')

CODEFILE="./days/day${DAY}.ts"
INDEXFILE="./days/index.ts"
INPUTFILE="../../input/input-day${DAY}.txt"
EXPECTEDFILE="../../expected/day${DAY}.txt"

if [[ -f "${CODEFILE}" ]]; then
  echo "Code file already exists"
else
  cat <<FILE > "${CODEFILE}"
import { Day } from "../aoc";

export class Day${DAY} extends Day {
  day = (): number => ${DAY};
  part1 = () => {
    return '';
  }
  part2 = () => {
    return '';
  }
}
FILE
  echo "export { Day${DAY} } from './day${DAY}';" >> "${INDEXFILE}"
  git add "${CODEFILE}"
  echo "Typescript file created"
fi

if [[ -f "${INPUTFILE}" ]]; then
  echo "Input file already exists"
else
  touch "${INPUTFILE}"
  echo "Input file created";
fi

if [[ -f "${EXPECTEDFILE}" ]]; then
  echo "Expected file already exists"
else
  cat <<FILE > "${EXPECTEDFILE}"
Part 1:
Part 2:
FILE
  echo "Expected file created";
fi