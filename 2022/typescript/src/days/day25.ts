import { Day, sum } from "../aoc";

export class Day25 extends Day {
  day = (): number => 25;
  part1 = () => decsna(this.input.map(snadec).reduce(sum))
  part2 = () => 'ok'
}

const chars = {'2': 2, '1': 1, '0': 0, '-': -1, '=': -2,}

export const snadec = (snafu: string): number =>
  snafu.split('').reverse().reduce((acc, chr, i) => {
    const x = i === 0 ? 1 : 5 ** i;
    const v = (chars[chr] * x);
    return acc + v;
  }, 0);

export const decsna = (decimal: number): string => {
  if (decimal >= 0 && decimal <= 2) return String(decimal);
  if (decimal % 5 === 3) return decsna(Math.floor(decimal / 5) + 1) + '='
  if (decimal % 5 === 4) return decsna(Math.floor(decimal / 5) + 1) + '-'
  return decsna(Math.floor(decimal / 5)) + decsna(decimal % 5);
}