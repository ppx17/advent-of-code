import { Day, Vec, Vector } from "../aoc";

export class Day15 extends Day {
  day = (): number => 15;
  private pairs: Pair[];
  part1 = () => {
    const row = 2_000_000;

    const inRange = this.pairs.filter((p => Math.abs(p.sensor.y - row) <= p.range))
    const min = Math.min(...inRange.map(p => p.sensor.x - p.range));
    const max = Math.max(...inRange.map(p => p.sensor.x + p.range));

    let count = 0;
    for (let x = min; x <= max; x++) {
      count += Number(inRange.some(p => p.sensor.manhattan(new Vector(x, row)) <= p.range));
    }

    const beacons = inRange.map(p => p.beacon).filter(b => b.y === row).map(b => b.serialize());

    return count - new Set(beacons).size;
  }
  part2 = () => {
    const limit = 4_000_000;
    for(const p of this.pairs) {
      for(const v of edge(p)) {
        if(v.x < 0 || v.y < 0 || v.x > limit || v.y > limit) continue;
        if (this.pairs.every(cp => cp.sensor.manhattan(v) > cp.range)) return (v.x * 4_000_000) + v.y;
      }
    }
    return 'not found';
  }

  protected setup() {
    this.pairs = this.input.map(l => l.match(/Sensor at x=(?<sx>[-\d]+), y=(?<sy>[-\d]+): closest beacon is at x=(?<bx>[-\d]+), y=(?<by>[-\d]+)/).groups)
      .map(m => {
        const sensor = Vector.deserialize(`${m.sx}:${m.sy}`);
        const beacon = Vector.deserialize(`${m.bx}:${m.by}`);
        return {sensor, beacon, range: sensor.manhattan(beacon)}
      });
  }
}

function* edge(pair: Pair): Generator<Vec> {
  const NE = Vector.east().add(Vector.north());
  const SE = Vector.east().add(Vector.south());
  const SW = Vector.west().add(Vector.south());
  const NW = Vector.west().add(Vector.north());
  let v = new Vector(pair.sensor.x - (pair.range + 1), pair.sensor.y);
  while(v.x !== pair.sensor.x) {
    yield v;
    v = v.add(NE)
  }
  while(v.y !== pair.sensor.y) {
    yield v;
    v = v.add(SE);
  }
  while(v.x !== pair.sensor.x) {
    yield v;
    v = v.add(SW)
  }
  while(v.y !== pair.sensor.y) {
    yield v;
    v = v.add(NW);
  }
}

type Pair = {
  sensor: Vector;
  beacon: Vector;
  range: number;
}
