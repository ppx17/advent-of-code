import { Day, desc, reduce } from "../aoc";

export class Day16 extends Day {
  day = (): number => 16;
  part1 = () => {
    const valves = this.graph();
    const valvesWithFlow = [...valves.values()].filter(v => v.flow > 0 && v.name !== 'AA');
    return calculateBestRoute(valves.get('AA'), valvesWithFlow, 30);
  }

  part2 = () => {
    const valves = this.graph();
    const valvesWithFlow = [...valves.values()].filter(v => v.flow > 0 && v.name !== 'AA');

    return reduce(groups(valvesWithFlow), (high, {a, b}) =>
      Math.max(
        high,
        calculateBestRoute(valves.get('AA'), a, 26) + calculateBestRoute(valves.get('AA'), b, 26)
      ), 0);
  }

  private graph() {
    const valves = new Map<string, Valve>();
    const groups = this.input.map(l => regex.exec(l).groups);
    groups.forEach(g => valves.set(g.name, new Valve(g.name, Number(g.flow))));
    groups.forEach(g => valves.get(g.name).neighbors.push(...g.neighbors.split(', ').map(n => valves.get(n))));
    return valves;
  }
}

function* groups<T>(list: T[]): Generator<{ a: T[], b: T[] }> {
  const desiredLeftSize = Math.ceil(list.length / 2);
  for (let bits = 0; bits <= 2 ** list.length; bits++) {
    if (hammingWeight(bits) !== desiredLeftSize) continue;
    const a = [];
    const b = [];
    list.forEach((el, i) => ((bits & 2 ** i) > 0 ? a : b).push(el));
    yield {a, b};
  }
}

const hammingWeight = (x: number) => {
  x = (x & (0x55555555)) + ((x >> 1) & (0x55555555));
  x = (x & (0x33333333)) + ((x >> 2) & (0x33333333));
  x = (x & (0x0f0f0f0f)) + ((x >> 4) & (0x0f0f0f0f));
  x = (x & (0x00ff00ff)) + ((x >> 8) & (0x00ff00ff));
  x = (x & (0x0000ffff)) + ((x >> 16) & (0x0000ffff));
  return x;
}

const calculateBestRoute = (position: Valve, options: Valve[], minutes: number, totalYield = 0): number => {
  if (minutes < 1 || options.length === 0) return totalYield;

  const routes = options.flatMap(option => {
    const distance = position.distances().get(option.name)
    const minutesLeft = minutes - distance - 1;
    const potential = option.yield(minutesLeft)
    if (potential <= 0) return [];
    return [calculateBestRoute(
      option,
      options.filter(o => o.name !== option.name),
      minutesLeft,
      totalYield + potential
    )];
  });

  return routes.length > 0 ? routes.sort(desc)[0] : totalYield;
}

const regex = /Valve (?<name>[A-Z]+) has flow rate=(?<flow>\d+); tunnels? leads? to valves? (?<neighbors>[A-Z, ]+)/

class Valve {
  public readonly neighbors: Valve[] = [];
  private valvesAtDistance: Map<string, number>;
  constructor(public name: string, public flow: number) {
  }
  yield = (minutes: number) => minutes * this.flow;

  distances = () => this.valvesAtDistance ??= this.calculateDistances();

  private calculateDistances() {
    const distanceTo = new Map<string, number>();
    const queue: QI[] = [{v: this, distance: 0}];
    while (queue.length > 0) {
      const el = queue.pop();
      if (distanceTo.has(el.v.name) && distanceTo.get(el.v.name) <= el.distance) continue;
      distanceTo.set(el.v.name, el.distance);
      queue.push(...el.v.neighbors.map(v => ({
        v,
        distance: el.distance + 1
      })));
    }
    return distanceTo;
  }
}

type QI = { v: Valve; distance: number };