import { findCodePosition, isStartCode } from "./day6";

describe('day 6', () => {
  describe('isStartCode', () => {
    it.each([
      ['mjqj', false],
      ['jqjq', false],
      ['qjpq', false],
      ['jpqm', true],
    ])('detects start codes %p is %p', (buffer, expected) => {
      expect(isStartCode(buffer.split(''))).toBe(expected);
    });
  });

  describe('findCodePosition', () => {
    it.each([
      ['mjqjpqmgbljsphdztnvjfqwrcgsmlb', 4, 7],
      ['bvwbjplbgvbhsrlpgdmjqwftvncz', 4, 5],
      ['nppdvjthqldpwncqszvftbrmjlhg', 4, 6],
      ['nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg', 4, 10],
      ['zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw', 4, 11],

      ['mjqjpqmgbljsphdztnvjfqwrcgsmlb', 14, 19],
      ['bvwbjplbgvbhsrlpgdmjqwftvncz', 14, 23],
      ['nppdvjthqldpwncqszvftbrmjlhg', 14, 23],
      ['nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg', 14, 29],
      ['zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw', 14, 26],
    ])('in data %p with codelength=%p the codePosition=%p', (data, length, position) => {
      expect(findCodePosition(data.split(''), length)).toBe(position);
    })
  })
})