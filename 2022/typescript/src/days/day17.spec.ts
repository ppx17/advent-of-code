import { Jets } from "./day17";

describe('day17', () => {

  describe('jets', () => {
    it('gives west with <', () => {
      const sut = new Jets('<');
      expect(sut.jet().serialize()).toBe('-1:0');
      expect(sut.jet().serialize()).toBe('-1:0');
      expect(sut.jet().serialize()).toBe('-1:0');
      expect(sut.jet().serialize()).toBe('-1:0');
    })
    it('gives east with >', () => {
      const sut = new Jets('>');
      expect(sut.jet().serialize()).toBe('1:0');
      expect(sut.jet().serialize()).toBe('1:0');
      expect(sut.jet().serialize()).toBe('1:0');
      expect(sut.jet().serialize()).toBe('1:0');
    })

    it('can handle some mixed pattern', () => {
      const sut = new Jets('>>><');
      expect(sut.jet().serialize()).toBe('1:0');
      expect(sut.jet().serialize()).toBe('1:0');
      expect(sut.jet().serialize()).toBe('1:0');
      expect(sut.jet().serialize()).toBe('-1:0');

      expect(sut.jet().serialize()).toBe('1:0');
      expect(sut.jet().serialize()).toBe('1:0');
      expect(sut.jet().serialize()).toBe('1:0');
      expect(sut.jet().serialize()).toBe('-1:0');
    })
  })
})