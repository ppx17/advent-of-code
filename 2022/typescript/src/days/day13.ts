import { Day, product, sum } from "../aoc";
import assert from "assert";

export class Day13 extends Day {
  day = (): number => 13;
  part1 = () =>
    this.rawInput()
      .split(/\r?\n\r?\n/)
      .map(p => p.split(/\r?\n/).map(d))
      .filter(p => p[0] !== undefined && p[1] !== undefined)
      .map((p, i) => Number(s([...p].sort(compare)) === s(p)) * (i + 1))
      .reduce(sum)
  part2 = () =>
    this.input
      .filter(l => l !== '')
      .concat(...dividers)
      .map(d)
      .sort(compare)
      .flatMap((l, i) => dividers.includes(s(l)) ? [i + 1] : [])
      .reduce(product)
}
const dividers = ['[[2]]', '[[6]]'];
const s = (input) => JSON.stringify(input);
const d = eval;

const compare = (p1: number | number[], p2: number | number[]): number => {
  if(isNumber(p1) && isNumber(p2)) return p1 - p2;
  if (!isNumber(p1) && isNumber(p2)) return compare(p1, [p2]);
  if (isNumber(p1) && !isNumber(p2)) return compare([p1], p2);
  assert(Array.isArray(p1) && Array.isArray(p2));

  for (let i = 0; i < p2.length; i++) {
    if (p1[i] === undefined) return -1;

    const result = compare(p1[i], p2[i]);
    if (result === 0) continue;
    return result;
  }
  return p1.length - p2.length;
}

type N = number | number[] | N[];
const isNumber = (x: N): x is number => typeof x === "number";