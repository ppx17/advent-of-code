import { Day, product } from "../aoc";

export class Day8 extends Day {
  day = (): number => 8;
  private g: Grid;
  part1 = () => new ForestVisibility(this.g).visibleTreeCount();
  part2 = () => new ScenicScorer(this.g).highestScenicScore();
  setup = () => this.g = new Grid(this.input)
}

type Line = readonly number[];

class Grid {
  public readonly grid: ReadonlyArray<ReadonlyArray<number>>;
  private cols = [];
  constructor(lines: string[]) {
    this.grid = lines.map(row => row.split('').map(Number));
  }
  width = () => this.grid[0].length;
  height = () => this.grid.length;
  row = (y: number): Line => this.grid[y];
  col = (x: number): Line => this.cols[x] ??= this.grid.map(row => row[x]);
  forEachInnerTree = (lambda: (x, y) => void) => {
    for (let y = 1; y < this.height() - 1; y++)
      for (let x = 1; x < this.width() - 1; x++)
        lambda(x, y);
  }
}

class ForestVisibility {
  constructor(private g: Grid) {}
  visibleTreeCount = () => {
    let count = 0;
    this.g.forEachInnerTree((x, y) => count += Number(this.treeVisible(x, y)));
    return count + this.g.width() * 2 + this.g.height() * 2 - 4;
  }
  private treeVisible = (x, y) => this.isVisible(this.g.row(y), x) || this.isVisible(this.g.col(x), y);
  private isVisible = (l: Line, i: number) => this.isVisibleLeft(l, i) || this.isVisibleRight(l, i);
  private isVisibleLeft = (l: Line, i: number) => {
    for (let j = 0; j < i; j++)
      if (l[j] >= l[i]) return false;
    return true;
  }
  private isVisibleRight = (l: Line, i: number) => {
    for (let j = i + 1; j < l.length; j++)
      if (l[j] >= l[i]) return false;
    return true;
  }
}

class ScenicScorer {
  constructor(private g: Grid) {}
  highestScenicScore = () => {
    let topScore = 0;
    for (let y = 1; y < this.g.height() - 1; y++)
      for (let x = 1; x < this.g.width() - 1; x++)
        topScore = Math.max(topScore, this.scenicScore(x, y))
    return topScore;
  }
  private scenicScore = (x, y) => [
    this.treesToLeft(this.g.row(y), x),
    this.treesToRight(this.g.row(y), x),
    this.treesToLeft(this.g.col(x), y),
    this.treesToRight(this.g.col(x), y),
  ].reduce(product);

  private treesToLeft = (line: Line, index: number) => {
    let score = 0;
    for (let i = index - 1; i >= 0; i--) {
      score++;
      if (line[i] >= line[index]) return score;
    }
    return score;
  }
  private treesToRight = (line: Line, index: number) => {
    let score = 0;
    for (let i = index + 1; i < line.length; i++) {
      score++;
      if (line[i] >= line[index]) return score;
    }
    return score;
  }
}

