import { Day, sum, Vec, Vector } from "../aoc";

export class Day18 extends Day {
  day = (): number => 18;
  private droplet = new VectorSet();
  private cubes;
  part1 = () => this.cubes
    .map(cube => neighbors
      .map(n => n.add(cube))
      .filter(cubeNeighbor => !this.droplet.has(cubeNeighbor))
      .length
    )
    .reduce(sum)

  part2 = () => {
    const topLeft = new Vector(
      Math.min(...this.cubes.map(c => c.x)) - 1,
      Math.min(...this.cubes.map(c => c.y)) - 1,
      Math.min(...this.cubes.map(c => c.z)) - 1,
    );
    const bottomRight = new Vector(
      Math.max(...this.cubes.map(c => c.x)) + 1,
      Math.max(...this.cubes.map(c => c.y)) + 1,
      Math.max(...this.cubes.map(c => c.z)) + 1,
    );

    const visited = new VectorSet();
    const queue = new VectorSet();
    queue.add(topLeft);

    let count = 0;

    while (queue.notEmpty()) {
      const cube = queue.pop();
      if (visited.has(cube)) continue;
      visited.add(cube);

      const neighborVectors = neighbors
        .map(n => n.add(cube))
        .filter(c => c.between(topLeft, bottomRight));

      count += neighborVectors.filter(v => this.droplet.has(v)).length;

      const newOptions = neighborVectors
        .filter(c => !this.droplet.has(c) && !visited.has(c) && !queue.has(c));

      queue.add(...newOptions);
    }

    return count;
  }

  setup() {
    this.cubes = this.input.map(v => Vector.deserialize(v, ','));
    this.droplet.add(...this.cubes);
  }
}

const neighbors = ['-1:0:0', '1:0:0', '0:-1:0', '0:1:0', '0:0:1', '0:0:-1']
  .map(v => Vector.deserialize(v));

class VectorSet {
  private readonly vectors = new Map<string, Vec>();
  add = (...vectors: Vec[]) => {
    vectors.forEach(v => this.vectors.set(v.serialize(), v));
  }
  has = (v: Vec) => this.vectors.has(v.serialize());
  delete = (v: Vec) => this.vectors.delete(v.serialize());
  get size() {
    return this.vectors.size;
  }
  pop(): Vec | undefined {
    if (this.empty()) return undefined;
    const k = this.vectors.keys().next().value;
    const v = this.vectors.get(k);
    this.vectors.delete(k);
    return v;
  }
  notEmpty = () => !this.empty();
  empty = () => this.vectors.size === 0;
}