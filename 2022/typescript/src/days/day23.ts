import { Day, range, reduce, Vector } from "../aoc";
import assert from "assert";

export class Day23 extends Day {
  day = (): number => 23;
  part1 = () => {
    const considerations = makeConsiderations();
    const board = makeBoard(this.input);

    reduce(range(1, 10), () => {
      this.executeRound(board, considerations);
      considerations.push(considerations.shift());
    });

    return ((board.bottomRight.x + 1 - board.topLeft.x) * (board.bottomRight.y + 1 - board.topLeft.y)) - board.count;
  }
  part2 = () => {
    const considerations = makeConsiderations();
    const board = makeBoard(this.input);

    let round = 0;
    for (let elvesMoved = true; elvesMoved; round++) {
      elvesMoved = this.executeRound(board, considerations);
      considerations.push(considerations.shift());
    }

    return round;
  }

  private executeRound = (board: Board<Elf>, considerations: Consideration[]): boolean => {
    const proposalMap = new Board<Elf[]>(() => []);
    for (const elf of board.values()) {
      elf.proposal = undefined;
      if (nobodyAround(elf, board)) continue;

      const consideration = availableConsideration(elf, considerations, board);
      if (consideration !== undefined) {
        elf.proposal = elf.position.add(consideration.target);
        proposalMap.get(elf.proposal).push(elf);
      }
    }

    let elvesMoved = false;
    // Load iterator into array first, otherwise board.move() keeps updating the collection and this loop never ends.
    for (const elf of [...board.values()]) {
      // Check if we're the only one with the same proposal, and then execute.
      if (elf.proposal === undefined) continue;
      if (proposalMap.get(elf.proposal).length > 1) continue;
      elvesMoved = true;
      board.move(elf.position, elf.proposal);
      elf.position = elf.proposal;
    }
    return elvesMoved;
  }
}

const nobodyAround = (elf: Elf, board: Board<Elf>) => surroundings.map(v => v.add(elf.position)).every(p => !board.has(p));
const availableConsideration = (elf: Elf, considerations: Consideration[], board: Board<Elf>) =>
  considerations.find(consideration => consideration.checks.map(v => v.add(elf.position)).every(v => !board.has(v)));

class Board<T> {
  private map = new Map<string, T>();
  private vectorCache: Vector[];

  constructor(private defaultFactory: () => T = undefined) {
  }

  public set(pos: Vector, val: T) {
    this.vectorCache = undefined;
    this.map.set(pos.serialize(), val);
  }

  public get(pos: Vector): T {
    const v = this.map.get(pos.serialize());
    if (v !== undefined) return v;
    if (this.defaultFactory === undefined) return undefined;
    const d = this.defaultFactory();
    this.set(pos, d);
    return d;
  }

  public move(from: Vector, to: Vector) {
    const item: T = this.map.get(from.serialize());
    assert(item !== undefined, `Could not move from pos ${from.serialize()} as it was not set.`);
    assert(!this.has(to), `Destination spot ${to.serialize()} was not empty.`)
    this.vectorCache = undefined;
    this.map.delete(from.serialize());
    this.map.set(to.serialize(), item);
  }

  public has = (pos: Vector): boolean => this.map.has(pos.serialize());

  public values(): IterableIterator<T> {
    return this.map.values();
  }

  get count() {
    return this.map.size;
  }

  get vectors(): Vector[] {
    return this.vectorCache ??= [...this.map.keys()].map(k => Vector.deserialize(k));
  }

  get topLeft() {
    return new Vector(
      Math.min(...this.vectors.map(p => p.x)),
      Math.min(...this.vectors.map(p => p.y)),
    );
  }

  get bottomRight() {
    return new Vector(
      Math.max(...this.vectors.map(p => p.x)),
      Math.max(...this.vectors.map(p => p.y)),
    );
  }
}

type Elf = {
  position: Vector;
  proposal: Vector | undefined;
}

type Consideration = {
  target: Vector;
  checks: Vector[];
}
const makeConsiderations: () => Array<Consideration> = () => [
  {target: Vector.north(), checks: [Vector.north(), Vector.NE(), Vector.NW()]},
  {target: Vector.south(), checks: [Vector.south(), Vector.SE(), Vector.SW()]},
  {target: Vector.west(), checks: [Vector.west(), Vector.NW(), Vector.SW()]},
  {target: Vector.east(), checks: [Vector.east(), Vector.NE(), Vector.SE()]},
];

const makeBoard = (input: string[]) => {
  const board = new Board<Elf>();

  input.map((line, y) => line.split('').forEach((tile, x) => {
    if (tile !== '#') return;
    const v = new Vector(x, y);
    board.set(v, {position: v, proposal: undefined});
  }));

  return board;
}

const surroundings: Vector[] = [
  Vector.north(), Vector.NE(), Vector.east(), Vector.SE(), Vector.south(), Vector.SW(), Vector.west(), Vector.NW(),
]