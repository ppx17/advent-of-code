import { Day, Vector, AStar, NodeGenerator, AStarNode } from "../aoc";


export class Day12 extends Day {
  private map: MapGenerator;
  day = (): number => 12;
  part1 = () => this.lengthFrom(this.map.start)
  part2 = () => {
    const candidates: Set<Vector> = new Set();
    this.map.map.forEach((row, x) => row.forEach((level, y) => {
      if (level !== 0) return;
      const me = new Vector(x, y);
      if (this.neighbors.map(n => n.add(me)).some(n => this.map.level(n) === 1)) {
        candidates.add(me);
      }
    }));
    return Math.min(...Array.from(candidates).map(v => this.lengthFrom(v)))
  }
  private lengthFrom = (start: Vector): number =>
    new AStar(this.map).run(MapGenerator.node(start), MapGenerator.node(this.map.end)).length - 1;
  setup = () => this.map = new MapGenerator(this.input)
  private neighbors = [
    Vector.north(),
    Vector.east(),
    Vector.south(),
    Vector.west()
  ]
}

class MapGenerator implements NodeGenerator {
  public map: number[][];
  public start: Vector;
  public end: Vector;
  constructor(input: string[]) {
    this.map = input.map((row, x) => row.split('').map((c, y) => {
      if (c === 'S') {
        this.start = new Vector(x, y);
        return 0;
      }
      if (c === 'E') {
        this.end = new Vector(x, y);
        return 25;
      }
      return c.charCodeAt(0) - 'a'.charCodeAt(0);
    }));
  }

  calculateEstimatedCost = (a: AStarNode, b: AStarNode) => a.vector.manhattan(b.vector);
  calculateRealCost = () => 1;
  private neighbors = [Vector.north(), Vector.east(), Vector.south(), Vector.west()]
  generateAdjacentNodes(node: AStarNode): AStarNode[] {
    return this.neighbors
      .map(d => d.add(node.vector))
      .filter(v => this.level(v) !== undefined && this.level(v) - this.level(node.vector) <= 1)
      .map((v?): AStarNode => MapGenerator.node(v, node));
  }
  level = (v: Vector): number | undefined => this.map?.[v.x]?.[v.y];

  static node = (v: Vector, parent?: AStarNode): AStarNode => {
    const n = new AStarNode(
      v.serialize(),
      parent
    );
    n.vector = v;
    return n;
  }
}
