import { Day, Vec, Vector } from "../aoc";
import assert from "assert";

export class Day22 extends Day {
  private instructions: ReturnType<typeof instructionGenerator>;
  day = (): number => 22;
  part1 = () => {
    const board = this.makeBoard();
    let player = {dir: Vector.east(), pos: board.topLeft()}

    board.visit(player);
    for (const instruction of this.instructions()) {
      if (isLeftTurn(instruction)) {
        player.dir = new Vector(player.dir.y, -player.dir.x);
        board.visit(player);
        continue;
      }
      if (isRightTurn(instruction)) {
        player.dir = new Vector(-player.dir.y, player.dir.x);
        board.visit(player);
        continue;
      }

      // South: x: 0, y: 1
      // East: x: 1, y: 0
      for (let i = 0; i < instruction; i++) {
        let target = player.pos.add(player.dir);
        if (board.isOutside(target)) {
          const flipped = new Vector(-player.dir.x, -player.dir.y);
          let opposite = player.pos;
          while (!board.isOutside(opposite)) {
            opposite = opposite.add(flipped);
          }
          target = opposite.add(player.dir);
        }

        if (board.isWall(target)) break;

        player.pos = target;
        board.visit(player);
      }
    }

    return (1000 * (player.pos.y + 1)) + (4 * (player.pos.x + 1)) + this.facingScore(player.dir);
  }
  part2 = () => {
    type RawBoundary = {
      l: string;
      min: string;
      max: string;
      dir: Vector,
      transform: (player: Player) => Player;
    }
    const rawBoundaries: RawBoundary[] = [
      {
        l: '1A',
        min: '50:-1',
        max: '99:-1',
        dir: Vector.north(),
        transform: (p) => ({dir: Vector.east(), pos: new Vector(0, p.pos.x + 100)})
      },
      {
        l: '1B',
        min: '-1:150',
        max: '-1:199',
        dir: Vector.west(),
        transform: (p) => ({dir: Vector.south(), pos: new Vector(p.pos.y - 100, 0)})
      },
      {
        l: '4A',
        min: '49:0',
        max: '49:49',
        dir: Vector.west(),
        transform: (p) => ({dir: Vector.east(), pos: new Vector(0, 149 - p.pos.y)})
      },
      {
        l: '4B',
        min: '-1:100',
        max: '-1:149',
        dir: Vector.west(),
        transform: (p) => ({dir: Vector.east(), pos: new Vector(50, 49 - (p.pos.y - 100))})
      },
      {
        l: '5A',
        min: '49:50',
        max: '49:99',
        dir: Vector.west(),
        transform: (p) => ({dir: Vector.south(), pos: new Vector(p.pos.y - 50, 100)})
      },
      {
        l: '5B',
        min: '0:99',
        max: '49:99',
        dir: Vector.north(),
        transform: (p) => ({dir: Vector.east(), pos: new Vector(50, p.pos.x + 50)})
      },
      {
        l: '7A',
        min: '100:-1',
        max: '149:-1',
        dir: Vector.north(),
        transform: (p) => ({dir: Vector.north(), pos: new Vector(p.pos.x - 100, p.pos.y + 200)})
      },
      {
        l: '7B',
        min: '0:200',
        max: '49:200',
        dir: Vector.south(),
        transform: (p) => ({dir: Vector.south(), pos: new Vector(p.pos.x + 100, 0)})
      },
      {
        l: '8A',
        min: '100:50',
        max: '149:50',
        dir: Vector.south(),
        transform: (p) => ({dir: Vector.west(), pos: new Vector(99, p.pos.x - 50)})
      },
      {
        l: '8B',
        min: '100:50',
        max: '100:99',
        dir: Vector.east(),
        transform: (p) => ({dir: Vector.north(), pos: new Vector(p.pos.y + 50, 49)})
      },
      {
        l: '11A',
        min: '50:150',
        max: '99:150',
        dir: Vector.south(),
        transform: (p) => ({dir: Vector.east(), pos: new Vector(49, p.pos.x + 100)})
      },
      {
        l: '11B',
        min: '50:150',
        max: '50:199',
        dir: Vector.east(),
        transform: (p) => ({dir: Vector.north(), pos: new Vector(p.pos.y - 100, 149)})
      },
      {
        l: '12A',
        min: '150:0',
        max: '150:49',
        dir: Vector.east(),
        transform: (p) => ({dir: Vector.west(), pos: new Vector(99, 149 - p.pos.y)})
      },
      {
        l: '12B',
        min: '100:100',
        max: '100:149',
        dir: Vector.east(),
        transform: (p) => ({dir: Vector.west(), pos: new Vector(149, 49 - (p.pos.y - 100))})
      },
    ];

    type Boundary = {
      l: string;
      min: Vector;
      max: Vector;
      dir: Vector,
      transform: (player: Player) => Player;
    }

    const boundaries = rawBoundaries.map(b => ({
      ...b,
      min: Vector.deserialize(b.min),
      max: Vector.deserialize(b.max),
    }));

    const board = this.makeBoard();
    let player = {dir: Vector.east(), pos: board.topLeft()}

    board.visit(player);
    for (const instruction of this.instructions()) {
      // console.log(`instruction ${instruction}`)
      if (isLeftTurn(instruction)) {
        player.dir = new Vector(player.dir.y, -player.dir.x);
        board.visit(player);
        continue;
      }
      if (isRightTurn(instruction)) {
        player.dir = new Vector(-player.dir.y, player.dir.x);
        board.visit(player);
        continue;
      }

      for (let i = 0; i < instruction; i++) {
        let target = {
          pos: player.pos.add(player.dir),
          dir: player.dir,
        };
        if (board.isOutside(target.pos)) {

          const matchingBoundaries = boundaries.filter(b => target.pos.between(b.min, b.max) && player.dir.is(b.dir));

          assert(
            matchingBoundaries.length === 1,
            `Not exactly 1 boundary found. pos=${target.pos.serialize()} dir=${player.dir.serialize()}, boundaries=${matchingBoundaries.map(b => b.l).join(', ')}`
          );

          const transformed = matchingBoundaries[0].transform(target);
          // console.log(`Wrapping around ${matchingBoundaries[0].l} from ${target.pos.serialize()} to ${transformed.pos.serialize()}`);
          // console.log(`Direction went from ${arrow(target.dir)} to ${arrow(transformed.dir)}`);


          target = transformed;
        }

        if (board.isWall(target.pos)) break;

        player = target;
        board.visit(player);
      }
    }

    // 102159 too low
    // 102156
    //  99255
    //  18350

    return (1000 * (player.pos.y + 1)) + (4 * (player.pos.x + 1)) + this.facingScore(player.dir);
    return '';
  }

  private facingScore = (dir: Vec): number => {
    return {
      [Vector.east().serialize()]: 0,
      [Vector.west().serialize()]: 2,
      [Vector.north().serialize()]: 3,
      [Vector.south().serialize()]: 1,
    }[dir.serialize()] ?? NaN;
  }

  protected setup() {
    this.instructions = instructionGenerator(this.input[this.input.indexOf('') + 1]);
  }

  private makeBoard(): Board {
    const lines = this.rawInput().split(/\r?\n/);
    const i = lines.indexOf('');
    return new Board(lines.slice(0, i));
  }
}

const instructionGenerator = (instructions: string) => function* (): Generator<Instruction> {
  for (const m of instructions.matchAll(/(?<dist>\d+)|(?<dir>[R|L])/g))
    yield m.groups.dist ? Number(m.groups.dist) : m.groups.dir as Turn;
}

class Board {
  public readonly map: Tile[][];
  constructor(input: string[]) {
    this.map = input.map(l => l.split('') as Tile[]);
  }
  topLeft = (): Vector => new Vector(this.map[0].indexOf('.'), 0);
  tile = (pos: Vec): Tile => this.map[pos.y]?.[pos.x] ?? ' ';
  isOpen = (pos: Vec) => ![' ', '#'].includes(this.tile(pos))
  isOutside = (pos: Vec) => this.tile(pos) === ' ';
  isWall = (pos: Vec) => this.tile(pos) === '#';
  visit = (p: Player) => {
    assert(this.isOpen(p.pos), `${p.pos.serialize()} was not open, cannot visit.`);
    this.map[p.pos.y][p.pos.x] = arrow(p.dir);
    // this.print();
    // console.log(`\n\n`)
  }

  print = () => console.log(this.map.map(l => l.join('')).join(`\n`));
}

const arrow = (dir: Vec) => {
  const arrows: Record<string, Tile> = {
    [Vector.east().serialize()]: '>',
    [Vector.west().serialize()]: '<',
    [Vector.north().serialize()]: '^',
    [Vector.south().serialize()]: 'v',
  }
  assert(arrows[dir.serialize()], `No arrow for ${dir.serialize()}`);
  return arrows[dir.serialize()];
}

type Tile = ' ' | '.' | '#' | '^' | 'v' | '<' | '>'
type Turn = 'R' | 'L'
type Instruction = number | Turn;
export const isTurn = (i: Instruction): i is Turn => i === 'R' || i === 'L';
export const isLeftTurn = (i: Instruction): i is 'L' => i === 'L';
export const isRightTurn = (i: Instruction): i is 'R' => i === 'R';
type Player = { pos: Vec, dir: Vec };