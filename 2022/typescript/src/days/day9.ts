import { Day, MutableVector, Vec, Vector } from "../aoc";

export class Day9 extends Day {
  private moves: Move[];
  day = (): number => 9;
  part1 = () => this.solve(1);
  part2 = () => this.solve(9)

  solve = (tailLength: number) => {
    const h = Vector.zero(2).mutable();
    const tails = Array.from({length: tailLength}).map(() => new Tail(0, 0));

    const visited = new Set();

    for (const m of this.moves) {
      for (let i = 0; i < m.distance; i++) {
        h.add(m.direction);
        tails.forEach((tail, ti) => tail.follow(ti === 0 ? h : tails[ti - 1]))
        visited.add(tails[tailLength - 1].serialize());
      }
    }
    return visited.size;
  }

  setup = () => this.moves = this.input.map(l => l.split(' ')).map(p => ({
    direction: directions[p[0]],
    distance: Number(p[1])
  }))
}

class Tail extends MutableVector {
  follow(h: Vec) {
    const dist = h.fixed().sub(this);
    if (this.manhattan(h) >= 3 || Math.max(...dist.abs().values) >= 2) this.add(dist.sign());
  }
}

const directions = {
  'U': Vector.north(),
  'D': Vector.south(),
  'L': Vector.west(),
  'R': Vector.east(),
}

type Move = {
  direction: Vector;
  distance: number;
}
