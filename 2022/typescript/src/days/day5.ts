import { Day, Tools } from "../aoc";

export class Day5 extends Day {
  day = (): number => 5;

  part1 = () => this.solve(false);
  part2 = () => this.solve(true);

  private solve = (retainOrder: boolean) => {
    const state = this.stacks();
    this.moves().forEach(m => moveCrates(m, state, retainOrder));
    return topCrates(state);
  }
  private stacks = () => {
    const stack = []
    Tools.inputString(this.day()).split(/\r?\n/).slice(0, this.input.indexOf('') - 1).reverse().forEach(l => {
      for (let i = 1, s = 0; i < l.length; i += 4, s++) {
        stack[s] ??= [];
        if (l[i] !== ' ') stack[s].push(l[i]);
      }
    });
    return stack;
  }
  private moves = () => [...Tools.inputString(this.day()).matchAll(regex)].map(m => ({
    count: Number(m.groups.count), from: Number(m.groups.from), to: Number(m.groups.to)
  }))
}

const topCrates = (stack: string[][]) => stack.map(s => s[s.length - 1]).join('');
const moveCrates = (m: Move, state: string[][], retainOrder: boolean) => {
  const buffer = []
  for (let i = 0; i < m.count; i++) {
    buffer.push(state[m.from - 1].pop());
  }
  state[m.to - 1].push(...(retainOrder ? buffer.reverse() : buffer));
}
const regex = /move (?<count>[0-9]+) from (?<from>[0-9]+) to (?<to>[0-9]+)/g;
type Move = { count: number; from: number; to: number; }
