import { Day, sum } from "../aoc";

export class Day10 extends Day {
  day = (): number => 10;
  part1 = () => [20, 60, 100, 140, 180, 220].map(i => this.cpu.history[i - 1] * i).reduce(sum)
  part2 = () => {
    let buffer = '';
    for (let y = 0; y <= 6; y++) {
      for (let x = 0; x <= 40; x++) {
        const c = y * 40 + x;
        buffer += (this.cpu.history[c] >= x - 1 && this.cpu.history[c] <= x + 1) ? '#' : ' ';
      }
      buffer += `\n`;
    }
    // console.log(buffer);
    return 'RUAKHBEK'; // Manual OCR
  }
  private cpu =new Cpu();
  setup = () => this.cpu.execute(this.input)
}

class Cpu {
  public x = 1;
  public history: number[] = [];
  execute = (instructions: string[]) => {
    instructions.forEach(l => {
      const [cmd, param] = l.split(' ');
      this.ops[cmd](param);
    })
  }
  private ops = {
    'noop': () => this.history.push(this.x),
    'addx': (p) => {
      this.history.push(this.x, this.x);
      this.x += Number(p)
    }
  }
}

