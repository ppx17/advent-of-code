import { Day, desc } from "../aoc";
import { sum } from "../aoc";

export class Day1 extends Day {
  day = (): number => 1;

  part1 = () => this.sortedElves()[0]
  part2 = () => this.sortedElves().slice(0, 3).reduce(sum)

  private sortedElves = () => Array.from(this.groups(this.input)).sort(desc);
  private *groups(list) {
    for (let i = 0; i !== -1; i = list.indexOf('', i + 1))
      yield [...list, ''].slice(i, list.indexOf('', i + 1)).map(Number).reduce(sum, 0);
  }
}