import { Day, sum } from "../aoc";

export class Day3 extends Day {
  day = (): number => 3;

  part1 = () => this.input.map(both).reduce(sum)
  part2 = () =>
    Array.from(batches(this.input, 3))
      .map(b => b.map(s => [...new Set(s.split(''))]))
      .map(b => intersect(...b))
      .map(b => val(b[0]))
      .reduce(sum);
}

const batches = function* <T>(list: T[], size): Generator<T[]> {
  for (let i = 0; i < list.length; i += size)
    yield list.slice(i, i + size);
}
const both = (line: string) => val(intersect(...[line.substring(0, line.length / 2), line.substring(line.length / 2)]
  .map(l => l.split('')))[0])
const intersect = <T>(...sets: T[][]): T[] => sets.reduce((r, s) => !r ? s : r.filter(c => s.includes(c)));
const val = (c: string) => c.charCodeAt(0) - (c.charCodeAt(0) >= 97 ? 96 : 38)