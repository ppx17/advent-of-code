import { Day } from "../aoc";

export class Day4 extends Day {
  day = (): number => 4;
  part1 = () => this.solve(isContained)
  part2 = () => this.solve(isOverlap);
  private solve = (fn) => this.input
    .map(l => l.split(',').map(e => e.split('-').map(Number)))
    .filter(p => fn(p[0], p[1]) || fn(p[1], p[0]))
    .length;
}

const isContained = (a, b) => a[0] >= b[0] && a[1] <= b[1];
const isOverlap = (a, b) => a[0] >= b[0] && a[0] <= b[1] || a[1] >= b[0] && a[1] <= b[1];
