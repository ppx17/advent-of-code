import { Day, sum } from "../aoc";

export class Day20 extends Day {
  private readonly key = 811589153;
  day = (): number => 20;
  part1 = () => {
    const original = this.numbers();
    return this.coords(this.mix(original, [...original]));
  }
  part2 = () => {
    const original = this.numbers(this.key);
    return this.coords(
      Array.from({length: 10})
        .reduce<N[]>((acc) => this.mix(original, acc), [...original]));
  }

  numbers = (key = 1) => this.input.map((num, i): N => ({
    num: Number(num) * key,
  }));

  private mix = (original: N[], input: N[]) => {
    const mod = original.length;
    original.forEach(o => {
      const idx = input.findIndex(n => n === o);
      input.splice(idx, 1);
      input.splice((idx + o.num) % (mod - 1), 0, o);
    });
    return input;
  }

  private coords = (numbers: N[]): number => {
    const zeroOffset = numbers.findIndex(n => n.num === 0);
    return [1000, 2000, 3000].map(i => numbers[(zeroOffset + i) % numbers.length].num).reduce(sum);
  }
}

type N = { num: number; }