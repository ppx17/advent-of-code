import { Day, desc, product } from "../aoc";

export class Day11 extends Day {
  day = (): number => 11;
  part1 = () => this.solve(this.parseMonkeys(), w => Math.floor(w / 3), 20)
  part2 = () => {
    const monkeys = this.parseMonkeys();
    const gcm = monkeys.map(m => m.mod).reduce(product);
    return this.solve(monkeys, w => w % gcm, 10_000);
  }

  private solve = (monkeys: Monkey[], manageWorry: (w: number) => number, ops: number) => {
    const opCount = monkeys.map(() => 0);

    for(let r = 0; r < ops; r++) {
      monkeys.forEach((m, mk) => {
        while(m.items.length > 0) {
          const item = manageWorry(m.operation(m.items.shift()));
          monkeys[m.throwTo(item)].items.push(item);
          opCount[mk]++;
        }
      });
    }
    return opCount.sort(desc).slice(0, 2).reduce(product);
  }

  private parseMonkeys = (): Monkey[] => {
    return this.rawInput().split(/\n\n/)
      .map(p => regex.exec(p).groups)
      .map((g): Monkey => {
        const op = g.op === '*' ? (a, b) => a * b : (a, b) => a + b;
        return {
          items: g.items.split(', ').map(Number),
          mod: Number(g.mod),
          operation: (old: number) => op(old, g.op_param === 'old' ? old : Number(g.op_param)),
          throwTo: (input: number) => input % Number(g.mod) === 0 ? Number(g.true) : Number(g.false)
        };
      });
  }
}

const regex = /Monkey \d+:\s+Starting items: (?<items>[\d ,]+)\s+Operation: new = old (?<op>[+*]) (?<op_param>\w+)\s+Test: divisible by (?<mod>\d+)\s+ If true: throw to monkey (?<true>\d+)\s+If false: throw to monkey (?<false>\d+)/m

type Monkey = {
  items: number[],
  mod: number,
  operation: (old: number) => number,
  throwTo: (input: number) => number,
}
