import { asc, Day, sum } from "../aoc";

export class Day7 extends Day {
  day = (): number => 7;
  part1 = () => Object.values(this.dirs()).filter(d => d < 100_000).reduce(sum)
  part2 = () => {
    const fsSize = 70_000_000;
    const needed = 30_000_000;

    const options = Object.values(this.dirs()).sort(asc);
    const used = options[options.length - 1];
    const toClean = needed - (fsSize - used);

    return options.find(o => o >= toClean);
  }

  private dirs = () => {
    const directories: Record<string, number> = {};
    const path: string[] = [];
    this.input.forEach(l => {
      const matchCd = l.match(/^\$ cd (?<dir>.*)$/)
      if(matchCd) {
        if(matchCd.groups['dir'] === '..') {
          path.pop();
          return;
        }
        path.push(matchCd.groups['dir']);
        directories[path.join('/')] = 0;
        return;
      }

      const matchFile = l.match(/^(?<size>\d+) [a-z.]+$/);
      if(matchFile) {
        for(let i = 1; i <= path.length; i++)
          directories[path.slice(0, i).join('/')] += Number(matchFile.groups.size);
      }
    })
    return directories;
  }
}
