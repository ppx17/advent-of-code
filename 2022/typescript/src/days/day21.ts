import { Day } from "../aoc";
import assert from "assert";

export class Day21 extends Day {
  day = (): number => 21;
  part1 = () => {
    const nodeMap = parseNodes(this.input);
    return nodeMap.get('root').resolve();
  }
  part2 = () => {
    const nodeMap = parseNodes(this.input);

    const r = nodeMap.get('root');
    const h = nodeMap.get('humn');

    assert(r instanceof Calculation && h instanceof Digits);
    const a = nodeMap.get(r.a);
    const b = nodeMap.get(r.b);

    const reference = branchHasNode(a, 'humn') ? b : a;
    const mutate = branchHasNode(a, 'humn') ? a : b;
    const target = reference.resolve();

    h.value = giveMe(mutate, target);
    assert(a.resolve() === b.resolve(), `Whoops, A and B are not equal yet.`);
    return h.value;
  }
}

const giveMe = (node: Node, wanted: number) => {
  if(node instanceof Calculation) {
    const a = node.children[0];
    const b = node.children[1];

    if(branchHasNode(a, 'humn')) {
      return giveMe(a, invertLeft(node.op, wanted, b.resolve()));
    }
    return giveMe(b, invertRight(node.op, wanted, a.resolve()));
  }
  assert(node.name === 'humn', `Requested to mutate non mutable monkey ${node.name}`)
  return wanted;
}

const branchHasNode = (branch: Node, nodeName: string) => {
  if(branch.name === nodeName) return true;
  if(branch.children.length === 0) return false;
  return branch.children.some(b => branchHasNode(b, nodeName));
}

abstract class Node {
  name: string;
  abstract resolve(): number
  public children: Node[] = [];
}

class Digits extends Node {
  constructor(public name: string, public value: number) {
    super();
  }
  resolve() {
    return this.value;
  }
}

type Op = '+' | '-' | '/' | '*';
class Calculation extends Node {
  constructor(public name: string, public a: string, public b: string, public op: Op) {
    super();
  }

  resolve(): number {
    return performOp(this.op, this.children[0].resolve(), this.children[1].resolve());
  }
}

const ops: Record<Op, (a: number, b: number) => number> = {
  '*': (a,b) => a * b,
  '/': (a, b) => a / b,
  '+': (a, b) => a + b,
  '-': (a, b) => a - b,
};
const leftInverseOps: Record<Op, (w: number, b: number) => number> = {
  '+': (w, b) => w - b,
  '-': (w, b) => w + b,
  '*': (w,b) => w / b,
  '/': (w, b) => w * b,
};
const rightInverseOps: Record<Op, (w: number, a: number) => number> = {
  ...leftInverseOps,
  '-': (w, a) => a - w,
  '/': (w, b) => w / b,
};

const performOp = (op: Op, a: number, b: number) => ops[op](a, b);
const invertLeft = (op: Op, a: number, b: number) => leftInverseOps[op](a, b);
const invertRight = (op: Op, a: number, b: number) => rightInverseOps[op](a, b);

const numberRegex = /^(?<name>[a-z]{4}): (?<value>\d+)$/
const calculationRegex = /(?<name>[a-z]{4}): (?<a>[a-z]{4}) (?<op>[+\-*/]) (?<b>[a-z]{4})/

const parseNodes = (input: string[]) => {
  const nodes =  input.map(l => {
    const parsedNumber = numberRegex.exec(l);
    if (parsedNumber !== null) {
      return new Digits(parsedNumber.groups.name, Number(parsedNumber.groups.value))
    }

    const parsedCalculation = calculationRegex.exec(l);
    if (parsedCalculation !== null) {
      return new Calculation(
        parsedCalculation.groups.name,
        parsedCalculation.groups.a,
        parsedCalculation.groups.b,
        parsedCalculation.groups.op as Op,
      );
    }
  });
  const nodeMap = new Map<string, Node>();
  nodes.forEach(n => {
    nodeMap.set(n.name, n);
  });

  nodes.forEach(n => {
    if(n instanceof Calculation) {
      n.children.push(nodeMap.get(n.a), nodeMap.get(n.b));
    }
  })

  return nodeMap;
}