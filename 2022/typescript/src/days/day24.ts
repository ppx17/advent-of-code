import { Day, Vector } from "../aoc";
import assert from "assert";

export class Day24 extends Day {
  private target: Vector;
  day = (): number => 24;
  part1 = () => this.simulate(parseBlizzards(this.input), 1, startPosition, this.target).minute
  part2 = () => {
    const r1 = this.simulate(parseBlizzards(this.input), 1, startPosition, this.target);
    const r2 = this.simulate(r1.blizzards, r1.minute + 1, this.target, startPosition);
    const r3 = this.simulate(r2.blizzards, r2.minute + 1, startPosition, this.target);
    return r3.minute;
  }

  private newBoard = () => new Board(this.input[0].length, this.input.length, this.target)

  private simulate(blizzards: Blizzard[], startMinute, start, finish) {
    let expeditions = new Expeditions();
    expeditions.add(start);
    let board = this.newBoard();

    for(let minute = startMinute; minute < 10_000; minute++) {
      blizzards = blizzards.map(b => moveBlizzard(b, board));

      const nextExpeditions = new Expeditions();
      for(const vec of expeditions.vectors) {
        const children = options
          .map(o => o.add(vec))
          .filter(v => this.target.is(v) || (!board.isWall(v) && !board.isBlizzard(v)));

        for(const child of children) {
          if(child.is(finish)) {
            return { minute, blizzards };
          }
          nextExpeditions.add(child);
        }
      }
      board = this.newBoard();
      expeditions = nextExpeditions;
      assert(!expeditions.empty(), `No expeditions remaining in minute ${minute}`)
    }

    return { minute: -1, blizzards: [] };

  }

  setup() {
    this.target = new Vector(this.input[0].length - 2, this.input.length - 1);
  }
}
const startPosition = new Vector(1, 0);

const moveBlizzard = (blizzard: Blizzard, board: Board) => {
  let dest = blizzard.pos.add(blizzard.dir);
  if(board.isWall(dest)) {
    const back = blizzard.dir.inverse();
    dest = dest.add(back);
    while(!board.isWall(dest)) {
      dest = dest.add(back);
    }
    dest = dest.add(blizzard.dir);
  }
  blizzard.pos = dest;
  board.add(blizzard);
  return blizzard;
}

const options = [
  Vector.south(),
  Vector.west(),
  Vector.north(),
  Vector.east(),
  new Vector(0, 0),
];

type Blizzard = {
  dir: Vector;
  pos: Vector;
}

class Expeditions {
  private set = new Set<string>();
  public vectors: Vector[] = [];
  add(v: Vector) {
    if(!this.set.has(v.serialize())) {
      this.set.add(v.serialize());
      this.vectors.push(v);
    }
  }
  empty = () => this.vectors.length === 0;
}

const parseBlizzards = (input:string[]): Blizzard[] => {
  const directions = {
    '^': Vector.north(),
    'v': Vector.south(),
    '<': Vector.west(),
    '>': Vector.east(),
  }
  const list: Blizzard[] = [];
  input.map((l, y) => l.split('').map((d, x) => {
    const dir = directions[d];
    if(dir !== undefined) {
      list.push({
        dir,
        pos: new Vector(x, y)
      })
    }
  }));
  return list;
}

class Board {
  private isOccupied = new Map<string, boolean>();
  constructor(private width: number, private height: number, private finish: Vector) {
  }

  add(blizzard: Blizzard) {
    this.isOccupied.set(blizzard.pos.serialize(), true);
  }

  isWall = (v: Vector) => {
    if(startPosition.is(v) || this.finish.is(v)) return false;
    return (v.x <= 0 || v.x >= this.width - 1 || v.y <= 0 || v.y >= this.height - 1);
  }
  isBlizzard = (v: Vector) => this.isOccupied.get(v.serialize()) ?? false;
}
