import { Day, reduce } from "../aoc";
import assert from "assert";

export class Day19 extends Day {
  day = (): number => 19;
  part1 = () =>
    reduce<Blueprint, number>(blueprints(this.input), (sum, blueprint) => {
      const result = simulateRandomOptions(blueprint, 24, 100_000);
      return sum + (blueprint.id * result.geode);
    }, 0)
  part2 = () =>
    reduce<Blueprint, number>(blueprints(this.input.slice(0, 3)), (sum, blueprint) => {
      const result = simulateRandomOptions(blueprint, 32, 500_000);
      return sum * result.geode;
    }, 1)
}

const allMaterials = ['ore', 'clay', 'obsidian', 'geode'] as const;

// Not always correct, increase iterations to improve accuracy.
const simulateRandomOptions = (blueprint: Blueprint, minutes: number, iterations: number): State => {
  let bestState = undefined;

  for (let i = 0; i < iterations; i++) {

    const state = cleanState();

    for (let minute = 1; minute <= minutes; minute++) {
      const building = decideWhatToBuild(state, blueprint);

      if (building !== undefined) {
        blueprint[building].forEach(c => {
          state[c.resource] -= c.amount;
        })
      }

      allMaterials.forEach(m => {
        state[m] += state[`${m}Robots`];
      });

      if (building !== undefined) {
        state[`${building}Robots`]++;
      }
    }

    if (bestState === undefined || state.geode > bestState.geode) {
      bestState = state;
    }
  }

  return bestState;
}

const optionallyBuild = ['obsidian', 'clay', 'ore'] as const;
const decideWhatToBuild = (state: State, blueprint: Blueprint): Building => {
  if (canAfford(state, blueprint.geode)) {
    return 'geode';
  }

  for (const material of optionallyBuild) {
    if (canAfford(state, blueprint[material]) && randomize()) return material;
  }

  return undefined;
}

const randomize = () => Math.random() < 0.5;


const cleanState = () => ({
  minute: 0,

  oreRobots: 1,
  clayRobots: 0,
  obsidianRobots: 0,
  geodeRobots: 0,

  ore: 0,
  clay: 0,
  obsidian: 0,
  geode: 0,
});

type Resource = 'ore' | 'clay' | 'obsidian' | 'geode';
type Cost = { resource: Resource, amount: number };
type Building = Resource | undefined;
type Blueprint = {
  id: number,
  ore: Cost[],
  clay: Cost[],
  obsidian: Cost[],
  geode: Cost[],
}

type State = {
  minute: number,

  oreRobots: number,
  clayRobots: number,
  obsidianRobots: number,
  geodeRobots: number,

  ore: number,
  clay: number,
  obsidian: number,
  geode: number,
}

const canAfford = (state: State, costs: Cost[]) => costs.every(c => state[c.resource] >= c.amount);


const blueprints = function* (input: string[]): Generator<Blueprint> {
  const regex = /Blueprint (?<id>\d+): Each ore robot costs (?<ore_ore>\d+) ore. Each clay robot costs (?<clay_ore>\d+) ore. Each obsidian robot costs (?<obsidian_ore>\d+) ore and (?<obsidian_clay>\d+) clay. Each geode robot costs (?<geode_ore>\d+) ore and (?<geode_obsidian>\d+) obsidian./
  for (const line of input) {
    const match = regex.exec(line);
    assert(match, `Couldn't match ${line}`);
    const r = match.groups;
    yield {
      id: Number(r.id),
      ore: [{amount: Number(r.ore_ore), resource: 'ore'}],
      clay: [{amount: Number(r.clay_ore), resource: 'ore'}],
      obsidian: [{
        amount: Number(r.obsidian_ore),
        resource: 'ore'
      }, {amount: Number(r.obsidian_clay), resource: 'clay'}],
      geode: [{amount: Number(r.geode_ore), resource: 'ore'}, {
        amount: Number(r.geode_obsidian),
        resource: 'obsidian'
      }],
    }
  }
}


/*
 * The relics of my first attempt, although it is 100% accurate, it is also way to slow
 */


const simulateBlueprint = (blueprint: Blueprint): State => {
  console.log(`Simulating BP: ${blueprint.id}`)
  const queue: State[] = [];
  const seen = new Set<string>();
  let bestState = undefined;

  queue.push(cleanState());

  while (queue.length > 0) {
    const state = queue.pop();

    if (state.minute === 24) {
      if (bestState === undefined || bestState.geode < state.geode) {
        bestState = state;
        console.log(`Best state: ${bestState.geode} geodes, queue: ${queue.length}`);
      }
      continue;
    }

    const alternativeRealities: State[] = [];

    if (canAfford(state, blueprint.geode)) {
      const alternative = pay(state, blueprint.geode);
      alternative.geodeRobots++;
      alternativeRealities.push(alternative);
    }
    if (canAfford(state, blueprint.obsidian)) {
      const alternative = pay(state, blueprint.obsidian);
      alternative.obsidianRobots++;
      alternativeRealities.push(alternative);
    }
    if (canAfford(state, blueprint.clay)) {
      const alternative = pay(state, blueprint.clay);
      alternative.clayRobots++;
      alternativeRealities.push(alternative);
    }
    if (canAfford(state, blueprint.ore)) {
      const alternative = pay(state, blueprint.ore);
      alternative.oreRobots++;
      alternativeRealities.push(alternative);
    }

    if (alternativeRealities.length < 3) { // when we can afford all of 'm it is never beneficial to buy nothing
      alternativeRealities.push({...state, minute: state.minute + 1});
    }

    alternativeRealities.forEach(reality => {
      reality.ore += state.oreRobots;
      reality.clay += state.clayRobots;
      reality.obsidian += state.obsidianRobots;
      reality.geode += state.geodeRobots;
    });

    const unseenRealities = alternativeRealities
      .filter(r => !(r.minute >= 23 && r.geodeRobots === 0)
        && !(r.minute >= 22 && r.obsidianRobots === 0)
        && !(r.minute >= 21 && r.clayRobots === 0)
        && !seen.has(serialize(r)));

    queue.push(...unseenRealities);
    unseenRealities.forEach(r => {
      assert(r, `R was undefined: ${r}, ${unseenRealities}`)
      seen.add(serialize(r))
    });
  }

  console.log(`Done with BP ${blueprint.id}, seen ${seen.size}`)

  return bestState;
}

const pay = (state: State, costs: Cost[]): State => {
  const newState = {...state, minute: state.minute + 1};
  costs.forEach(c => newState[c.resource] -= c.amount);
  return newState;
}

const serialize = (state: State): string => JSON.stringify(state);