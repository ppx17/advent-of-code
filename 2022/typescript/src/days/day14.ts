import { Day, Vec, Vector } from "../aoc";

export class Day14 extends Day {
  day = (): number => 14;
  private readonly start = new Vector(500, 0);
  private readonly routes = [
    Vector.south(),
    Vector.south().add(Vector.west()),
    Vector.south().add(Vector.east()),
  ];
  part1 = () => {
    const m = this.makeMap();
    for(let sandCount = 0; true; sandCount++) {
      const sand = this.dropSand(m, true);
      if (sand === undefined) return sandCount;
      m.set(sand, 'o');
    }
  }
  part2 = () => {
    const m = this.makeMap();
    for(let sandCount = 1; true; sandCount++) {
      const sand = this.dropSand(m, false);
      m.set(sand, 'o');
      if (sand.is(this.start)) return sandCount;
    }
  }
  dropSand(m: Map, checkOutside: boolean): Vector | undefined {
    let sand = this.start;
    while (true) {
      if (checkOutside && m.outside(sand)) return undefined;
      const r = this.routes.find(d => m.get(sand.add(d)) === undefined);
      if(r !== undefined) {
        sand = sand.add(r);
        continue;
      }
      return sand;
    }
  }

  private makeMap() {
    const m = new Map();
    this.input.forEach(line => {
      const vs = line.split(' -> ').map(v => Vector.deserialize(v, ','));
      for (let i = 0; i < vs.length - 1; i++) {
        for (const v of between(vs[i], vs[i + 1])) {
          m.set(v, '#');
        }
      }
    });

    return m;
  }
}

const between = function*(a: Vec, b: Vec) {
  if (a.x === b.x) {
    for (let y = Math.min(a.y, b.y); y <= Math.max(a.y, b.y); y++) {
      yield new Vector(a.x, y);
    }
  }
  if (a.y === b.y) {
    for (let x = Math.min(a.x, b.x); x <= Math.max(a.x, b.x); x++) {
      yield new Vector(x, a.y);
    }
  }
};

class Map {
  private map: string[][] = [];
  private lowest = 0;
  set(v: Vector, s: string) {
    this.map[v.x] ??= [];
    this.map[v.x][v.y] = s;
    if (s === '#' && v.y > this.lowest) this.lowest = v.y;
  }
  get = (v: Vector) => (v.y === this.lowest + 2) ? '#' : this.map?.[v.x]?.[v.y];
  outside = (v: Vector) => v.y > this.lowest;
}