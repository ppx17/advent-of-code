import { Day, Vec, Vector } from "../aoc";
import assert from "assert";

export class Day17 extends Day {
  day = (): number => 17;
  simulate = (totalRocks, startRock = 0, startJet = 0, chamber = new Chamber()) => {
    const jets = new Jets(this.input[0], startJet);
    const loopDetector = new LoopDetector(chamber, jets);

    for (let rock = startRock; rock < totalRocks; rock++) {
      const shape = shapes[rock % shapes.length];
      let pos = new Vector(2, chamber.height() + 3);

      while (true) {
        const afterJet = this.jetPush(jets, pos, shape, chamber);

        const afterDrop = afterJet.add(Vector.north()); // inverted Y axis
        if (chamber.collidesWith(shape, afterDrop)) {
          chamber.addShape(shape, afterJet);
          break;
        }
        pos = afterDrop;
      }

      if (!loopDetector.hasLoop()) continue;

      const loop = loopDetector.getLoop();
      const iterationsLeftAfterLoopPatternStarts = totalRocks - loop.shapeOffset;
      const factor = Math.floor(iterationsLeftAfterLoopPatternStarts / loop.numberOfShapes);

      const oldHeight = chamber.height();
      return loop.height * factor
        + loop.heightOffset
        + this.simulate(
          totalRocks,
          loop.shapeOffset + (loop.numberOfShapes * factor),
          loop.jetOffset + (loop.jet * factor),
          chamber
        ) - oldHeight;
    }
    return chamber.height();
  }

  private jetPush(jets: Jets, pos: Vector, shape: Shape, chamber: Chamber) {
    const afterPush = pos.add(jets.jet());
    return afterPush.x < 0
    || afterPush.x + shape.width - 1 >= 7
    || chamber.collidesWith(shape, afterPush)
      ? pos
      : afterPush;
  }
  part1 = () => this.simulate(2022);
  part2 = () => this.simulate(1_000_000_000_000);
}

type Hash = {
  hash: string;
  top: number;
  shapes: number;
  jets: number;
}

type Loop = {
  numberOfShapes: number;
  height: number;
  jet: number;
  shapeOffset: number;
  heightOffset: number;
  jetOffset: number;
}

class LoopDetector {
  private loop: Loop;
  constructor(private chamber: Chamber, private jets: Jets) {
    this.chamber.onShapeAdded = () => this.shapeAdded();
  }
  public shapeAdded() {
    if (this.chamber.height() > this.hashLength) this.addHash();
  }

  public hasLoop = () => this.loop !== undefined;
  public getLoop = () => this.loop;

  private hashes = new Map<string, Hash>();
  private readonly hashLength = 50;

  private addHash() {
    const now: Hash = {
      hash: this.chamber.map
        .slice(this.chamber.height() - this.hashLength, this.chamber.height())
        .map(r => r.map(v => v === true ? '#' : '.').join(''))
        .join('|'),
      shapes: this.chamber.shapeCount,
      top: this.chamber.height(),
      jets: this.jets.active,
    }
    if (!this.hashes.has(now.hash)) {
      this.hashes.set(now.hash, now);
      return;
    }
    const old = this.hashes.get(now.hash);
    this.loop = {
      height: now.top - old.top,
      heightOffset: old.top,
      numberOfShapes: now.shapes - old.shapes,
      shapeOffset: old.shapes,
      jet: now.jets - old.jets,
      jetOffset: old.jets,
    }
  }
}

class Chamber {
  public map: boolean[][] = [];
  public shapeCount = 0;
  public onShapeAdded: () => void;
  height = () => this.map.length;

  collidesWith(shape: Shape, location: Vec): boolean {
    if (location.y < 0) return true;
    for (const [y, row] of shape.layout.entries()) {
      for (const [x, s] of row.entries()) {
        const v = new Vector(x, y).add(location);
        if (s && this.isOccupied(v)) return true;
      }
    }
  }

  isOccupied = (vector: Vec) => this.map?.[vector.y]?.[vector.x] === true;
  occupy = (vector: Vec, state: boolean) => {
    this.map[vector.y] ??= [];
    this.map[vector.y][vector.x] ||= state;
  }

  addShape(shape: Shape, location: Vec) {
    this.shapeCount++;
    for (const [y, row] of shape.layout.entries()) {
      for (const [x, s] of row.entries()) {
        const v = new Vector(x, y).add(location);
        this.occupy(v, s);
      }
    }
    if(this.onShapeAdded) this.onShapeAdded();
  }
}

export class Jets {
  constructor(private readonly jets: string, public active = 0) {
  }
  jet = (): Vector => {
    const j = this.jets[this.active++ % this.jets.length];
    assert(j === '<' || j === '>', `Found unknown jet: ${j}`);
    return j === '<' ? Vector.west() : Vector.east();
  }
}

type Shape = {
  readonly height: number;
  readonly width: number;
  readonly layout: boolean[][];
}

const shapes = [
  {height: 1, width: 4, layout: [[true, true, true, true]]},
  {height: 3, width: 3, layout: [[false, true, false], [true, true, true], [false, true, false]]},
  {height: 3, width: 3, layout: [[true, true, true], [false, false, true], [false, false, true]]},
  {height: 4, width: 1, layout: [[true], [true], [true], [true]]},
  {height: 2, width: 2, layout: [[true, true], [true, true]]}
]

