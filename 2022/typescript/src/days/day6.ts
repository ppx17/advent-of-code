import { Day } from "../aoc";

export class Day6 extends Day {
  day = (): number => 6;
  part1 = () => findCodePosition(this.input[0].split(''), 4)
  part2 = () => findCodePosition(this.input[0].split(''), 14)
}

export const findCodePosition = (buffer: string[], codeSize: number) => {
  for (let i = codeSize; i < buffer.length; i++)
    if (isStartCode(buffer.slice(i - codeSize, i))) return i;
}

export const isStartCode = (buffer: string[]): boolean =>
  !buffer.some((c, i) => buffer.indexOf(c, i + 1) !== -1)