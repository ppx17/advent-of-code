import { AStar, AStarNode, Day, NodeGenerator, sum, Vec, Vector } from "../aoc";

export class Day17 extends Day {
  day = (): number => 17;
  part1 = () => this.solve(new RegularCrucibleGearIslandMap(this.parsedInput()))
  part2 = () => this.solve(new UltraCrucibleGearIslandMap(this.parsedInput()))

  private parsedInput = () => this.input.map(l => l.split('').map(Number));
  private solve = (map: NodeGenerator) => {
    const input = this.input.map(l => l.split('').map(Number));

    const start = nodeForVector(Vector.zero());
    start.stepsSinceTurn = 0;
    start.direction = Vector.east();

    const target = nodeForVector(new Vector(input[0].length - 1, input.length - 1));

    const aStar = new AStar(map);
    aStar.hasReachedGoal = (currentNode, goal) => currentNode.vector.is(goal.vector);

    return aStar.run(start, target).map(node => input[node.vector.y][node.vector.x]).slice(1).reduce(sum);
  }
}

const nodeForVector = (vector: Vector, parent: GearIslandNode | undefined = undefined): GearIslandNode => {
  const node = new GearIslandNode(nodeId(vector, Vector.south(), 0), parent);
  node.vector = vector;
  return node;
}

class GearIslandNode extends AStarNode {
  public direction: Vector;
  public stepsSinceTurn: number;
}

abstract class BaseGearIslandMap {
  constructor(protected readonly input: number[][]) {
  }
  calculateEstimatedCost = (a: AStarNode, b: AStarNode) => 1;
  calculateRealCost = (a: AStarNode, b: AStarNode) => this.input[b.vector.y][b.vector.x] ?? 99;

  protected abstract optionsForNode(node: GearIslandNode): [Vector, boolean][];

  generateAdjacentNodes = (node: GearIslandNode) =>
    this.optionsForNode(node)
      .map(([dir, isTurn]) => ({ pos: node.vector.add(dir), dir, isTurn }))
      .filter(({ pos }) => this.input[pos.y]?.[pos.x] !== undefined)
      .map(({ pos, dir, isTurn }) => {
        const stepsSinceTurn = isTurn ? 1 : node.stepsSinceTurn + 1;
        const n = new GearIslandNode(nodeId(pos, dir, stepsSinceTurn), node);
        n.vector = pos;
        n.stepsSinceTurn = stepsSinceTurn;
        n.direction = dir;
        return n;
      });
}

class RegularCrucibleGearIslandMap extends BaseGearIslandMap implements NodeGenerator {
  protected optionsForNode(node: GearIslandNode) {
    const options: [Vector, boolean][] = [
      [node.direction.rotateLeft(), true],
      [node.direction.rotateRight(), true],
    ];
    if (node.stepsSinceTurn < 3) options.push([node.direction, false])

    return options;
  }
}

class UltraCrucibleGearIslandMap extends BaseGearIslandMap implements NodeGenerator {
  optionsForNode(node: GearIslandNode) {
    const turns: [Vector, boolean][] = [
      [node.direction.rotateLeft(), true],
      [node.direction.rotateRight(), true],
    ];

    const options = [];
    if (node.stepsSinceTurn < 10) options.push([node.direction, false])
    if (node.stepsSinceTurn >= 4) options.push(...turns);

    return options;
  }
}

const nodeId = (position: Vec, direction: Vector, stepsSinceTurn: number) => `${position.toString()},${direction.toString()},${stepsSinceTurn}`;

const printPath = (map: number[][], path: AStarNode[]) => {
  const mapCopy = map.map(l => l.map(c => String(c)));
  path.forEach(node => {
    mapCopy[node.vector.y][node.vector.x] = `\x1b[32m${charForDir((node as GearIslandNode).direction)}\x1b[0m`;
  });
  console.log(mapCopy.map(l => l.join('')).join('\n'));
}

const charForDir = (dir: Vector): string => {
  if (dir.is(Vector.north())) return '^';
  if (dir.is(Vector.south())) return 'v';
  if (dir.is(Vector.east())) return '>';
  if (dir.is(Vector.west())) return '<';
  return '?';
}