import { Day, product } from "../aoc";

export class Day20 extends Day {
  day = (): number => 20;
  part1 = () => {
    const bus = parseInput(this.input);
    let highSignalCounter = 0, lowSignalCounter = 0;
    bus.signalHook = (signal) => {
      if (signal.isHigh) highSignalCounter++;
      else lowSignalCounter++;
    }
    for (let i = 0; i < 1000; i++) bus.pressButton();
    return highSignalCounter * lowSignalCounter;
  }
  part2 = () => {
    const bus = parseInput(this.input);
    const conjunctionModuleCount = bus.getModules().filter(m => m instanceof ConjunctionModule).length;

    const detector = new ConjunctionPatternDetector();
    bus.signalHook = (signal) => {
      if(!signal.isHigh && bus.getModule(signal.source) instanceof ConjunctionModule) detector.trackLowSignal(signal.source, bus.buttonPressCounter)
    }

    for (let presses = 1; true; presses++) {
      const conj = [...detector.state.values()];
      if(conj.length === conjunctionModuleCount - 1) {
        // Minus 1 because the last conjunction module is only triggered after $result number of presses
        return conj.map(v => v).reduce(product, 1);
      }
      bus.pressButton();
    }
  }
}

class SignalBus {
  private modules = new Map<string, Module>();
  public buttonPressCounter = 0;

  public signalHook: (signal: Signal) => void;

  public readonly processingQueue: Signal[] = [];

  sendSignal = (signal: Signal) => this.processingQueue.push(signal);
  registerModule = (module: Module) => this.modules.set(module.id, module);
  publishInputs = () =>
    this.modules.forEach(m => {
      m.targets.forEach(t => this.modules.get(t)?.registerInput(m.id));
    });

  pressButton() {
    const button = this.modules.get('button');
    if (button !== undefined && button instanceof ButtonModule) button.press();
    this.buttonPressCounter++;
    this.processQueue();
  }

  getModule = (id: string) => this.modules.get(id);
  getModules = () => [...this.modules.values()];

  private processQueue() {
    while (this.processingQueue.length > 0) this.processQueueItem();
  }

  private processQueueItem() {
    const signal = this.processingQueue.shift();
    if (this.signalHook) this.signalHook(signal);
    this.modules.get(signal.target)?.receiveSignal(signal);
  }
}

class ConjunctionPatternDetector {
  public state = new Map<string, number>();

  trackLowSignal(id: string, buttonPresses: number) {
    if (!this.state.has(id)) this.state.set(id, buttonPresses);
  }
}

type Signal = {
  source: string;
  target: string;
  isHigh: boolean;
}

abstract class Module {
  public readonly inputs = new Set<string>();
  constructor(public readonly id: string, private readonly bus: SignalBus, public readonly targets: string[]) {
  }

  registerInput = (input: string) => this.inputs.add(input);
  abstract receiveSignal(signal: Signal): void;
  protected sendToAllTargets = (isHigh: boolean) => this.targets
    .forEach(target => this.bus.sendSignal({ target, isHigh, source: this.id }));
}

class BroadcasterModule extends Module {
  receiveSignal = ({ isHigh }) => this.sendToAllTargets(isHigh);
}

class FlipFlopModule extends Module {
  private isHigh = false;

  receiveSignal({ isHigh }) {
    if (isHigh) return;
    this.isHigh = !this.isHigh;
    this.sendToAllTargets(this.isHigh);
  }
}

class ConjunctionModule extends Module {
  receiveSignal({ isHigh, source }) {
    if (!this.memory().has(source)) return;
    this.memory().set(source, isHigh);
    this.sendToAllTargets(![...this.memory().values()].every(v => v));
  }

  toString = () => `${this.id} ${[...this.memory().values()].map(b => b ? '1' : '0').join('')}`;

  private _memory: Map<string, boolean>;
  private memory = (): Map<string, boolean> => this._memory ??= this.initializeMemory();
  private initializeMemory = () => new Map<string, boolean>([...this.inputs].map(i => [i, false] as [string, boolean]));
}

class ButtonModule extends Module {
  receiveSignal = () => {
  };

  press = () => this.sendToAllTargets(false);
}

const parseInput = (input: string[]): SignalBus => {
  const bus = new SignalBus();
  input.map(line => {
    const [source, target] = line.split(' -> ');
    const targets = target.split(', ');
    if (source === 'broadcaster') return new BroadcasterModule(source, bus, targets);
    if (source[0] === '%') return new FlipFlopModule(source.substring(1), bus, targets);
    if (source[0] === '&') return new ConjunctionModule(source.substring(1), bus, targets);
  }).forEach(m => bus.registerModule(m));
  bus.registerModule(new ButtonModule('button', bus, ['broadcaster']));
  bus.publishInputs();

  return bus;
}