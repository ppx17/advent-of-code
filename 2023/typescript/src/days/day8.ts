import { Day } from "../aoc";

export class Day8 extends Day {
  day = (): number => 8;
  part1 = () => {
    const { directions, map } = parse(this.input);
    let s = 0;
    for(let pos = 'AAA'; pos !== 'ZZZ'; s++) {
      pos = map.get(pos)[directions[s % directions.length]];
    }

    return s;
  }
  part2 = () => {
    const { directions, map } = parse(this.input);
    const travelers = Array.from(map.keys()).filter(p => p.endsWith('A'));

    const shortestPathForTravelers = travelers.map(t => {
      let s = 0;
      let pos = t;
      while(!pos.endsWith('Z')) {
        pos = map.get(pos)[directions[s % directions.length]];
        s++;
      }
      return s;
    })

    return shortestPathForTravelers.reduce(lcm);
  }
}

const lcm = (a: number, b: number) => a * b / gcd(a, b);
const gcd = (a: number, b: number): number => b === 0 ? a : gcd(b, a % b);

const parse = (input: string[]) => {
  const directions = input[0].split('');
  const map = new Map<string, { L: string, R: string}>();
  input.slice(2).forEach(l => {
    const match = /(?<S>\w+) = \((?<L>\w+), (?<R>\w+)\)/.exec(l);
    map.set(match.groups['S'], { L: match.groups['L'], R: match.groups['R']});
  });
  return { directions, map };
}