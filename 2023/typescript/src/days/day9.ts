import { Day, sum } from "../aoc";

export class Day9 extends Day {
  day = (): number => 9;
  part1 = () =>
    parseSequences(this.input).map(currentStep =>
      extrapolateValue(
        resolveDifferenceSteps(currentStep),
        (step, last) => step[step.length - 1] + last
      )
    ).reduce(sum)

  part2 = () =>
    parseSequences(this.input).map(currentStep =>
      extrapolateValue(
        resolveDifferenceSteps(currentStep),
        (step, last) => step[0] - last
      )
    ).reduce(sum)
}

const parseSequences = (input: string[]) =>
  input.map(line => line.split(/\s+/).map(Number));

const resolveDifferenceSteps = (measurements: number[]): number[][] => {
  const steps = [measurements];
  while (!measurements.every(v => v === 0)) {
    measurements = measurements.flatMap((value, idx) =>
      (idx + 1 >= measurements.length) ? [] : [measurements[idx + 1] - value]
    );
    steps.push(measurements);
  }
  return steps;
}

const extrapolateValue = (differenceSteps: number[][], extrapolate: (step: number[], last: number) => number) =>
  differenceSteps.reverse().reduce((last, step) => extrapolate(step, last), 0)