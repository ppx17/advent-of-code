import { Day, Vector } from "../aoc";


export class Day18 extends Day {
  day = (): number => 18;
  part1 = () => solve(this.instructions())
  part2 = () => solve(this.instructions().map(i => convertColorToInstruction(i.color)))
  private instructions = () => parseInstructions(this.input);
}

const solve = (instructions: Instruction[]) => {
  const points: Vector[] = [];
  let current = Vector.zero();
  instructions.forEach((instruction) => {
    current = current.add(instruction.direction.times(instruction.distance));
    points.push(current);
  });

  const height = Math.max(...points.map((point) => point.y));

  return points.reduce((surface, a, i) => {
    const b = circularArray(points, i + 1);
    const prev = circularArray(points, i - 1);
    const next = circularArray(points, i + 2);

    if (a.x === b.x) return surface;

    let dist = a.manhattan(b);
    let y = height - a.y;
    if (a.x < b.x) {
      // move forward
      if (isDown(prev, a) && isUp(b, next)) dist--;
      if (isUp(prev, a) && isDown(b, next)) dist++;
      y++; // account for height of the digger
    } else if (a.x > b.x) {
      // move backward
      if (isDown(prev, a) && isUp(b, next)) dist++;
      if (isUp(prev, a) && isDown(b, next)) dist--;
      dist = -dist;
    }
    return surface + y * dist;
  }, 0);
}

const circularArray = <T>(array: T[], index: number) => array[(array.length + index) % array.length];
const isDown = (a: Vector, b: Vector) => a.y < b.y;
const isUp = (a: Vector, b: Vector) => a.y > b.y;

type Instruction = { direction: Vector; distance: number; color: string; }
const dirs = {
  'R': Vector.east(),
  'D': Vector.south(),
  'L': Vector.west(),
  'U': Vector.north()
};

const parseInstructions = (input: string[]): Instruction[] => {
  const regex = /([RLUD]) (\d+) \((#[a-f0-9]{6})\)/;
  return input.map((line) => {
    const [_, direction, distance, color] = line.match(regex);
    return {
      direction: dirs[direction],
      distance: parseInt(distance),
      color
    }
  });
}

const convertColorToInstruction = (color: string): Instruction => {
  return {
    direction: Object.values(dirs)[Number(color[6])],
    distance: parseInt(color.substring(1, 6), 16),
    color
  }
}
