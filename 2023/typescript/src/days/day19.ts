import { Day, product, sum } from "../aoc";

export class Day19 extends Day {
  day = (): number => 19;
  part1 = () =>
    this.acceptedParts()
      .map(({ x, m, a, s }) => x + m + a + s)
      .reduce(sum, 0);

  part2 = () => {
    const initial = initialInstructionGroup();

    const queue: QueueItem[] = [{ group: initial, workflow: 'in', stepIdx: 0 }];
    const accepted: InstructionGroup[] = [];

    while (queue.length > 0) {
      const { group, workflow, stepIdx } = queue.shift()!;
      const instruction = this.instructions().get(workflow)!;
      const step = instruction.steps[stepIdx];
      if (!step.condition) {
        if (step.target === 'A') accepted.push(group);
        else if (step.target !== 'R') queue.push({ group, workflow: step.target, stepIdx: 0 });
        continue;
      }
      const [matching, nonMatching] = splitInstructionGroup(group, step.condition);
      if (step.target === 'A') accepted.push(matching);
      else if (step.target !== 'R') queue.push({ group: matching, workflow: step.target, stepIdx: 0 })
      queue.push({ group: nonMatching, workflow, stepIdx: stepIdx + 1 });
    }

    return accepted.reduce((acc, group) => {
      return acc + (group.x.length * group.m.length * group.a.length * group.s.length);
    }, 0);
  }
  private acceptedParts = () => processInstructions(this.instructions(), parseObjects(this.rawInput()));
  private instructions = () => this.instructionCache ??= parseInstructions(this.rawInput());
  private instructionCache?: Instructions;
}

type InstructionGroup = {
  x: number[],
  m: number[],
  a: number[],
  s: number[],
}

type QueueItem = {
  group: InstructionGroup,
  workflow: string,
  stepIdx: number;
}

const initialInstructionGroup = (): InstructionGroup => ({
  x: Array.from({ length: 4000 }, (_, i) => i + 1),
  m: Array.from({ length: 4000 }, (_, i) => i + 1),
  a: Array.from({ length: 4000 }, (_, i) => i + 1),
  s: Array.from({ length: 4000 }, (_, i) => i + 1),
});

const splitInstructionGroup = (group: InstructionGroup, condition: Condition): InstructionGroup[] => {
  const matching = {
    ...group,
    [condition.property]: group[condition.property].filter(value => testOperator(condition.operator, value, condition.operand)),
  };
  const nonMatching = {
    ...group,
    [condition.property]: group[condition.property].filter(value => !testOperator(condition.operator, value, condition.operand)),
  };
  return [matching, nonMatching].filter(g => g[condition.property].length > 0);
};

const testOperator = (operator: Condition['operator'], value: number, operand: number) => {
  if (operator === '<') return value < operand;
  if (operator === '>') return value > operand;
  throw new Error(`Unknown operator ${operator}`);

}

type Workflow = {
  id: string,
  steps: Step[],
}

type Condition = {
  property: string,
  operator: '>' | '<',
  operand: number,
}

type Step = {
  condition?: Condition;
  target: string,
}

const processInstructions = (instructions: Instructions, objects: MachinePart[]) =>
  objects
    .filter(object => runWorkflow(instructions, instructions.get('in')!, object).result === 'accept');

const runWorkflow = (instructions: Instructions, workflow: Workflow, object: MachinePart) => {
  const next = workflow.steps.find(step => step.condition === undefined || testCondition(step.condition, object));
  if (next.target === 'A') return { result: 'accept', object };
  if (next.target === 'R') return { result: 'reject', object };
  return runWorkflow(instructions, instructions.get(next.target)!, object);
}

const testCondition = (condition: Condition, object: MachinePart) => {
  const { property, operator, operand } = condition;
  const value = object[property];
  return testOperator(operator, value, operand);
}

type Instructions = Map<string, Workflow>;
const parseInstructions = (input: string): Instructions =>
  input
    .split(/\n\n/, 2)[0].split(/\n/)
    .map(parseWorkflow)
    .reduce((map, workflow) => map.set(workflow.id, workflow), new Map<string, Workflow>())

type MachinePart = { x: number, m: number, a: number, s: number };
const parseObjects = (input: string): MachinePart[] =>
  input.split(/\n\n/, 2)[1].split(/\n/).filter(s => s).map(parseObject);

const parseObject = (input: string): MachinePart => {
  const regex = /\{x=(?<x>\d+),m=(?<m>\d+),a=(?<a>\d+),s=(?<s>\d+)}/;
  const { groups } = input.match(regex) ?? { groups: undefined };
  if (!groups) throw new Error(`Invalid object ${input}`);
  return {
    x: Number(groups.x),
    m: Number(groups.m),
    a: Number(groups.a),
    s: Number(groups.s),
  }
}


const parseWorkflow = (input: string): Workflow => {
  const [id, rawSteps] = input.substring(0, input.length - 1).split('{', 2);
  const steps = rawSteps.split(',').map(parseStep);
  return { id, steps };
}

const parseStep = (input: string): Step => {
  if (!input.includes(':')) return { target: input };
  const [condition, target] = input.split(':');
  return {
    condition: parseCondition(condition),
    target,
  }
}

const parseCondition = (input: string): Condition => {
  const [property, operator, operand] = input.split(/([<>])/);
  return {
    property,
    operator: operator as Condition['operator'],
    operand: Number(operand),
  }
}