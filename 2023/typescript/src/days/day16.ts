import { Day, Vector } from "../aoc";

export class Day16 extends Day {
  day = (): number => 16;
  part1 = () => fireBeam(this.input, { pos: new Vector(0, 0), dir: Vector.east() });
  part2 = () => {
    const initialBeams: Beam[] = [];
    for (let y = 0; y < this.input.length; y++) {
      initialBeams.push(
        { pos: new Vector(0, y), dir: Vector.east() },
        { pos: new Vector(this.input[y].length - 1, y), dir: Vector.west() }
      );
    }
    for (let x = 0; x < this.input[0].length; x++) {
      initialBeams.push(
        { pos: new Vector(x, 0), dir: Vector.south() },
        { pos: new Vector(x, this.input.length - 1), dir: Vector.north() }
      );
    }

    return Math.max(...initialBeams.map(b => fireBeam(this.input, b)));
  }
}

type Beam = { pos: Vector, dir: Vector };

const getTile = (map: string[], pos: Vector): string | undefined => map[pos.y]?.[pos.x];
const insideMap = (map: string[], pos: Vector): boolean => pos.x >= 0 && pos.y >= 0 && pos.y < map.length && pos.x < map[pos.y].length;
const serializeBeam = (beam: Beam): string => `${beam.pos.toString()},${beam.dir.toString()}`;

const fireBeam = (map: string[], beam: Beam): number => {
  const beams: Beam[] = [beam];
  const excited = new Set<string>();
  const hasDone = new Set<string>();

  while (beams.length > 0) {
    const beam = beams.shift()!;
    const tile = getTile(map, beam.pos);
    if (tile === undefined) continue;
    excited.add(beam.pos.toString());
    hasDone.add(serializeBeam(beam));
    const newDirs = mirrorDeflections[tile][beam.dir.toString()];
    newDirs.forEach(newDir => {
      const newPos = beam.pos.add(newDir);
      if (!insideMap(map, newPos)) return;
      const newBeam = { pos: beam.pos.add(newDir), dir: newDir };
      if (hasDone.has(serializeBeam(newBeam))) return;
      beams.push(newBeam);
    });
  }

  return excited.size;
}

const mirrorDeflections = {
  '.': {
    [Vector.north().toString()]: [Vector.north()],
    [Vector.east().toString()]: [Vector.east()],
    [Vector.south().toString()]: [Vector.south()],
    [Vector.west().toString()]: [Vector.west()],
  },
  '/': {
    [Vector.north().toString()]: [Vector.east()],
    [Vector.east().toString()]: [Vector.north()],
    [Vector.south().toString()]: [Vector.west()],
    [Vector.west().toString()]: [Vector.south()],
  },
  '\\': {
    [Vector.north().toString()]: [Vector.west()],
    [Vector.west().toString()]: [Vector.north()],
    [Vector.south().toString()]: [Vector.east()],
    [Vector.east().toString()]: [Vector.south()],
  },
  '-': {
    [Vector.north().toString()]: [Vector.east(), Vector.west()],
    [Vector.south().toString()]: [Vector.east(), Vector.west()],
    [Vector.east().toString()]: [Vector.east()],
    [Vector.west().toString()]: [Vector.west()],
  },
  '|': {
    [Vector.north().toString()]: [Vector.north()],
    [Vector.south().toString()]: [Vector.south()],
    [Vector.east().toString()]: [Vector.north(), Vector.south()],
    [Vector.west().toString()]: [Vector.north(), Vector.south()],
  }
}