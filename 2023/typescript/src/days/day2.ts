import { Day, product, sum } from "../aoc";

export class Day2 extends Day {
  day = (): number => 2;
  part1 = () =>
    games(this.input)
      .filter(game =>
        game.hands
          .every(handful => handful.every(set => (bagContentP1?.[set.color] ?? 0) >= set.count))
      ).map(game => game.id)
      .reduce(sum)
  part2 = () =>
    games(this.input)
      .map((game): number =>
        (Object.values(
          game.hands
            .reduce((min, handful) =>
              handful.reduce((min, set) => ({
                ...min,
                [set.color]: Math.max(min[set.color] ?? 0, set.count)
              }), min), {})) as number[])
          .reduce(product)
      )
      .reduce(sum)
}

const games = (lines: string[]): Game[] => lines.map(l => {
  const match = /Game (?<id>[0-9]+): (?<data>.*)/.exec(l);
  const hands = match?.groups?.data
    ?.split('; ')
    .map(handful =>
      handful
        .split(', ')
        .map(set => {
          const setPart = set.split(' ');
          return {
            count: Number(setPart[0]),
            color: setPart[1]
          };
        }));

  return { id: Number(match?.groups?.id ?? '0'), hands };
});

type Game = {
  id: number;
  hands: { count: number, color: string }[][];
}

const bagContentP1 = {
  'red': 12,
  'green': 13,
  'blue': 14,
}
