import { Day } from "../aoc";

export class Day7 extends Day {
  day = (): number => 7;
  part1 = () =>
    this.input.map(parseHand).sort(sortHands(handTypesP1, cardStrengthP1))
      .reverse()
      .reduce((acc, hand, idx) => hand.bid * (idx + 1) + acc, 0)
  part2 = () =>
    this.input
      .map(parseHand)
      .sort(sortHands(handTypesP2, cardStrengthP2))
      .reverse()
      .reduce((acc, hand, idx) => hand.bid * (idx + 1) + acc, 0)
}

const sortHands = (handTypes: HandTypes, cardStrengths: Record<string, number>) =>
  (a: Hand, b: Hand) => {
    const aType = getHandType(handTypes)(a.cards);
    const bType = getHandType(handTypes)(b.cards);
    if (aType.r !== bType.r) return bType.r - aType.r;
    for (let i = 0; i < a.cards.length; i++) {
      if (cardStrengths[a.cards[i]] !== cardStrengths[b.cards[i]]) return cardStrengths[b.cards[i]] - cardStrengths[a.cards[i]];
    }
    return 0;
  }

type Hand = {
  cards: string[];
  bid: number;
}

const parseHand = (hand: string): Hand => {
  const [cards, bid] = hand.split(' ', 2);
  return {
    cards: cards.split(''),
    bid: Number(bid),
  }
}

const cardStrengthP1 = {
  '2': 2,
  '3': 3,
  '4': 4,
  '5': 5,
  '6': 6,
  '7': 7,
  '8': 8,
  '9': 9,
  'T': 10,
  'J': 11,
  'Q': 12,
  'K': 13,
  'A': 14,
}

const cardStrengthP2 = {
  'J': 1,
  '2': 2,
  '3': 3,
  '4': 4,
  '5': 5,
  '6': 6,
  '7': 7,
  '8': 8,
  '9': 9,
  'T': 10,
  'Q': 12,
  'K': 13,
  'A': 14,
}

const getHandType = (handTypes: typeof handTypesP1) => (hand: string[]) => handTypes.find(t => t.f(hand));
type HandTypes = typeof handTypesP1;

const xOfAKind = (x: number, compare: (a, b) => boolean = (a, b) => a === b) =>
  (hand: string[]) =>
    hand.some(c => hand.filter(h => compare(c, h)).length === x);

const twoPair = (hand: string[]) => {
  const counts = new Map<string, number>();
  hand.forEach(c => counts.set(c, (counts.get(c) ?? 0) + 1));
  return Array.from(counts.values()).filter(c => c === 2).length === 2;
}
const xOfAKindJoker = (x: number) => xOfAKind(x, (a, b) => a === b || b === 'J');
const handTypesP1 = [
  { r: 7, l: 'Five of a kind', f: xOfAKind(5) },
  { r: 6, l: 'Four of a kind', f: xOfAKind(4) },
  { r: 5, l: 'Full house', f: (hand: string[]) => xOfAKind(3)(hand) && xOfAKind(2)(hand) },
  { r: 4, l: 'Three of a kind', f: xOfAKind(3) },
  { r: 3, l: 'Two pair', f: twoPair },
  { r: 2, l: 'One pair', f: xOfAKind(2) },
  { r: 1, l: 'High card', f: () => true },
];

const handTypesP2 = [
    { r: 7, l: 'Five of a kind', f: xOfAKindJoker(5) },
    { r: 6, l: 'Four of a kind', f: xOfAKindJoker(4) },
    {
      r: 5,
      l: 'Full house',
      f: (hand: string[]) => {
        const groups = Array.from(new Set(hand)).map(c => hand.filter(h => h === c));
        const ownThree = groups.filter(h => h.length === 3);
        const ownTwo = groups.filter(h => h.length === 2);
        if (ownThree.length === 1 && ownTwo.length === 1) return true;

        const ownTwoWithoutJoker = ownTwo.filter(two => two[0] !== 'J');
        return ownTwoWithoutJoker.length === 2 && hand.includes('J');
      }
    },
    { r: 4, l: 'Three of a kind', f: xOfAKindJoker(3) },
    {
      // If you can create two pair with a joker, you're better off making three of a kind with the joker
      r: 3, l: 'Two pair', f: twoPair
    },
    { r: 2, l: 'One pair', f: xOfAKindJoker(2) },
    { r: 1, l: 'High card', f: () => true }
    ,
  ]
;