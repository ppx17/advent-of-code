import { Day, sum } from "../aoc";

export class Day13 extends Day {
  day = (): number => 13;
  part1 = () =>
    getGrids(this.rawInput())
      .map(summarizeGrid(0))
      .reduce(sum)
  part2 = () =>
    getGrids(this.rawInput())
      .map(summarizeGrid(1))
      .reduce(sum)
}

const getGrids = (input: string): string[][] => input.split(/\n\n/).map(g => g.split(/\n/));

const countSmudgesInReflection = (grid: string[], y: number): number => {
  let smudges = 0;
  for (let a = y, b = y + 1; a >= 0 && b < grid.length; a--, b++)
    for (let x = 0; x < grid[a].length; x++)
      smudges += Number(grid[a][x] !== grid[b][x]);
  return smudges;
}

const summarizeGrid = (expectedSmudges = 0) => (horizontalGrid: string[]) => {
  let summary = 0;

  for (let y = 0; y < horizontalGrid.length - 1; y++)
    summary += Number(countSmudgesInReflection(horizontalGrid, y) === expectedSmudges) * 100 * (y + 1);

  const verticalGrid = rotateGridLeft(horizontalGrid);
  for (let x = 0; x < verticalGrid.length - 1; x++)
    summary += Number(countSmudgesInReflection(verticalGrid, x) === expectedSmudges) * (x + 1);

  return summary;
}

const rotateGridLeft = (grid: string[]) => {
  const rotated = [];
  for (let y = 0; y < grid[0].length; y++) {
    rotated[y] = '';
    for (let x = 0; x < grid.length; x++) rotated[y] += grid[x][y];
  }
  return rotated;
}