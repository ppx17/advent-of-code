import { Day, sum, Vector } from "../aoc";

export class Day14 extends Day {
  day = (): number => 14;
  part1 = () => {
    const platform = parseMap(this.input);
    const result = tilt(platform, Vector.north());

    // printPlatform(platform);
    // console.log();
    // printPlatform(result);
    return weight(result);
  }
  part2 = () => {
    const cycles = 1_000_000_000;
    const patternDetectionLength = 10;
    let platform = parseMap(this.input);
    const pattern = new Map<string, number>();
    const history: number[] = [];
    for (let i = 0; i < cycles; i++) {
      for (const direction of [Vector.north(), Vector.west(), Vector.south(), Vector.east()]) {
        platform = tilt(platform, direction);
      }
      const w = weight(platform);
      history.push(w);
      if (i > patternDetectionLength) {
        const id = history.slice(i - patternDetectionLength, i).join(',');
        if (pattern.has(id)) {
          const cycleStart = pattern.get(id)!;
          const cycleLength = i - cycleStart;
          const pos = ((cycles - cycleStart) % cycleLength) + cycleStart - 1;
          return history[pos];
        }
        pattern.set(id, i);
      }
    }
    return '';
  }
}

type Platform = {
  rounded: Map<string, Vector>;
  square: Map<string, Vector>;
  width: number;
  height: number;
}

const parseMap = (input: string[]): Platform => {
  const rounded = new Map<string, Vector>();
  const square = new Map<string, Vector>();

  input.forEach((line, y) =>
    line.split('').forEach((char, x) => {
      const pos = new Vector(x, y);
      if (char === '#') square.set(pos.toString(), pos);
      if (char === 'O') rounded.set(pos.toString(), pos);
    }));

  return { rounded, square, height: input.length, width: input[0].length };
}

const tilt = (platform: Platform, direction: Vector): Platform => {
  const { rounded, square } = platform;

  const newRounded = new Map<string, Vector>(rounded.entries());

  for (let moved = true; moved;) {
    moved = false;
    newRounded.forEach((pos) => {
      // move as far in the direction as possible
      const newPos = pos.add(direction);
      if (
        newPos.values.some(x => x < 0)
        || newPos.x >= platform.width
        || newPos.y >= platform.height
        || newRounded.has(newPos.toString())
        || square.has(newPos.toString())
      ) return;
      newRounded.delete(pos.toString());
      newRounded.set(newPos.toString(), newPos);
      moved = true;
    });
  }

  return { rounded: newRounded, square, width: platform.width, height: platform.height };
}

const weight = (platform: Platform): number => {
  return Array.from(platform.rounded.values())
    .map((pos) => platform.height - pos.y)
    .reduce(sum, 0);
}