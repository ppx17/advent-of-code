import { Day } from "../aoc";

export class Day5 extends Day {
  day = (): number => 5;
  part1 = () => {
    const { seeds, conversions } = parseInput(this.input);

    const locations = seeds.map(seedToLocation(conversions));
    return locations.reduce((a, b) => Math.min(a, b));
  }
  part2 = () => {
    const { seeds, conversions, reverseConversions } = parseInput(this.input);
    const converter = locationToSeed(reverseConversions);

    const seedRanges = [];
    for (let i = 0; i < seeds.length; i += 2) {
      seedRanges.push({
        start: seeds[i],
        end: seeds[i] + seeds[i + 1]
      });
    }

    for (let location = 100_000_000; location < 200_000_000; location++) {
      const seed = converter(location);
      if (seedRanges.some(range => seed >= range.start && seed < range.end)) return location;
    }

    return 0;
  }
}

const seedToLocation = (conversionsMap: Map<string, Conversion>) => (value: number) => {
  let type = 'seed';
  while (type !== 'location') {
    const conversion = conversionsMap.get(type);
    if (!conversion) throw new Error(`No conversion found for type ${type}`);
    value = conversion.convert(value);
    type = conversion.to;
  }
  return value;
}

const locationToSeed = (conversionsMap: Map<string, Conversion>) => (value: number) => {
  let type = 'location';
  while (type !== 'seed') {
    const conversion = conversionsMap.get(type);
    if (!conversion) throw new Error(`No conversion found for type ${type}`);
    value = conversion.revert(value);
    type = conversion.from;
  }
  return value;
}

type ValueMapper = {
  destStart: number;
  sourceStart: number;
  length: number;
  map: (value: number) => { mapped: boolean, value: number };
  revert: (value: number) => { mapped: boolean, value: number };
}
const makeMapper = (destStart: number, sourceStart: number, length: number): ValueMapper => ({
  destStart,
  sourceStart,
  length,
  map(value: number) {
    if (value < sourceStart || value >= sourceStart + length) return { mapped: false, value };
    return {
      mapped: true,
      value: destStart + (value - sourceStart)
    };
  },

  revert(value: number) {
    if (value < destStart || value >= destStart + length) return { mapped: false, value };
    return {
      mapped: true,
      value: sourceStart + (value - destStart)
    };
  }
});

type Conversion = {
  from: string;
  to: string;
  valueMappers: ValueMapper[];
  convert: (value: number) => number;
  revert: (value: number) => number;
}

const makeConversion = (from: string, to: string, mappers: ValueMapper[]): Conversion => ({
  from,
  to,
  valueMappers: mappers,
  convert: (value: number) => {
    for (const mapper of mappers) {
      const { mapped: m, value: v } = mapper.map(value);
      if (m) return v;
    }
    return value;
  },

  revert: (value: number) => {
    for (const mapper of mappers) {
      const { mapped: m, value: v } = mapper.revert(value);
      if (m) return v;
    }
    return value;
  }
});

const parseInput = (input: string[]) => {
  const seeds = input[0].substring(7).split(/\s+/).map(Number);
  const conversions = new Map<string, Conversion>();
  const reverseConversions = new Map<string, Conversion>();
  let from: string | undefined, to: string | undefined;
  let mappers: ValueMapper[] = [];
  for (const line of input.slice(1)) {
    if (line.trim() === '') {
      if (from && to) {
        const conversion = makeConversion(from, to, mappers);
        conversions.set(from, conversion);
        reverseConversions.set(to, conversion);
        from = undefined;
        to = undefined;
        mappers = [];
      }
      continue;
    }
    if (line.includes('map:')) {
      [, from, to] = /([a-z]+)-to-([a-z]+) map:/.exec(line) ?? [];
      continue;
    }
    const [destStart, sourceStart, length] = line.split(/\s+/).map(Number);
    mappers.push(makeMapper(destStart, sourceStart, length));
  }
  if (from && to) {
    const conversion = makeConversion(from, to, mappers);
    conversions.set(from, conversion);
    reverseConversions.set(to, conversion);
  }
  return { seeds, conversions, reverseConversions };
}