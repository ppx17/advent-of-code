import { Day, Vector } from "../aoc";

export class Day21 extends Day {
  day = (): number => 21;
  part1 = () => {
    return reachPlots(this.input, findStartPoint(this.input), 6);
  }
  part2 = () => {
    const emulatedSteps = 150;
    const hundred = reachInfinite(this.input, findStartPoint(this.input), emulatedSteps);
    for(let i = 1; i <= emulatedSteps; i++) {
      console.log(`${i}: ${hundred.get(i)?.size}  factor=${hundred.get(i)?.size! / hundred.get(i - 1)?.size!}`);
    }

    for(const x of [6, 10, 50, 100]) {
      console.log(`${x}: ${hundred.get(x).size}`);
    }

    return '';
  }
}

const findStartPoint = (map: string[]): Vector => {
  for (let y = 0; y < map.length; y++) {
    for (let x = 0; x < map[y].length; x++) {
      if (map[y][x] === 'S') return new Vector(x, y);
    }
  }
  throw new Error('No start point found');
}

type QueueItem = {
  pos: Vector,
  steps: number,
}
const reachPlots = (map: string[], start: Vector, stepsTarget: number) => {
  const reachable = new Set<string>();
  const queue: QueueItem[] = [{ pos: start, steps: 0 }];
  const visited = new Set<string>();
  while (queue.length > 0) {
    const { pos, steps } = queue.shift()!;
    neighbours(pos)
      .filter(v => map[v.y][v.x] !== '#' && v.x >= 0 && v.x < map[0].length && v.y >= 0 && v.y < map.length)
      .forEach(n => {
        const qi = { pos: n, steps: steps + 1 };
        const id = `${qi.pos.toString()}:${qi.steps}`;
        if (qi.steps === stepsTarget) {
          reachable.add(id);
          return;
        }
        if (visited.has(id)) return;
        visited.add(id);
        queue.push(qi);
      });
  }
  return reachable.size;
}

const reachInfinite = (map: string[], start: Vector, stepsTarget: number) => {
  let reachable = new Map<number, Set<string>>();
  const queue: QueueItem[] = [{ pos: start, steps: 0 }];
  const visited = new Set<string>();
  while (queue.length > 0) {
    const { pos, steps } = queue.shift()!;
    neighbours(pos)
      .filter(v => readInfiniteMap(map, v) !== '#')
      .forEach(n => {        const qi = { pos: n, steps: steps + 1 };
        const id = `${qi.pos.toString()}:${qi.steps}`;
        if (!reachable.has(qi.steps)) reachable.set(qi.steps, new Set());
        reachable.get(qi.steps)!.add(id);
        if (qi.steps === stepsTarget) {
          return;
        }
        if (visited.has(id)) return;
        visited.add(id);
        queue.push(qi);
      });
  }
  return reachable;
}

const readInfiniteMap = (map: string[], pos: Vector): string => {
  const h = map.length;
  const w = map[0].length;
  const y = (h + (pos.y % h)) % h;
  const x = (w + (pos.x % w)) % w;
  return map[y][x];
}

const neighbours = (pos: Vector): Vector[] =>
  [Vector.north(), Vector.east(), Vector.south(), Vector.west()]
    .map(v => v.add(pos));