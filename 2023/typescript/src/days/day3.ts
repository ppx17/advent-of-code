import { Day, product, sum } from "../aoc";

export class Day3 extends Day {
  day = (): number => 3;
  part1 = () => {
    let numbers = [];
    for (let y = 0; y < this.input.length; y++) {
      for (let x = 0; x < this.input[0].length; x++) {
        const c = this.input[y][x];
        if (!isDigit(c)) continue;
        let number = c;
        let hasSymbol = symbolNextTo(this.input, x, y);
        while (isDigit(this.input[y][x + 1])) {
          number += this.input[y][++x];
          hasSymbol ||= symbolNextTo(this.input, x, y);
        }
        if (hasSymbol) numbers.push(Number(number));
      }
    }
    return numbers.reduce(sum, 0);
  }
  part2 = () => {
    let ratios = [];
    for (let y = 0; y < this.input.length; y++) {
      for (let x = 0; x < this.input[0].length; x++) {
        const c = this.input[y][x];
        if (!isGear(c)) continue;

        let adjacent = new Map<string, number>();
        for (let sy = y - 1; sy <= y + 1; sy++) {
          for (let sx = x - 1; sx <= x + 1; sx++) {
            if (isDigit(this.input[sy]?.[sx])) {
              let scanLeft = sx;
              while (isDigit(this.input[sy]?.[scanLeft])) scanLeft--;
              scanLeft++;
              const startX = scanLeft;
              let number = '';
              while (isDigit(this.input[sy]?.[scanLeft])) number += this.input[sy][scanLeft++];
              adjacent.set(`${startX},${sy}`, Number(number));
            }
          }
        }
        const ratio = Array.from(adjacent.values());
        if(ratio.length === 2) ratios.push(ratio.reduce(product, 1));
      }
    }
    return ratios.reduce(sum);
  }
}

const
  symbolNextTo = (input: string[], x: number, y: number) =>
    isSymbol(input[y + 1]?.[x]) || isSymbol(input[y - 1]?.[x]) || isSymbol(input[y][x + 1]) || isSymbol(input[y][x - 1]) || isSymbol(input[y + 1]?.[x + 1]) || isSymbol(input[y - 1]?.[x - 1]) || isSymbol(input[y + 1]?.[x - 1]) || isSymbol(input[y - 1]?.[x + 1]);

const
  isDigit = (c: string | undefined) => c !== undefined && c >= '0' && c <= '9';
const
  isSymbol = (c: string | undefined) => c !== undefined && c !== `\n` && c !== ` ` && !isDigit(c) && c !== `.`;
const
  isGear = (c: string | undefined) => c === '*';