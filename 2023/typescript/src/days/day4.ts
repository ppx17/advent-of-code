import { Day, sum } from "../aoc";

export class Day4 extends Day {
  day = (): number => 4;
  part1 = () => getPointList(this.input)
    .map((count) => {
      if (count === 0) return 0;
      if (count === 1) return 1;
      return 2 ** (count - 1);
    })
    .reduce(sum);
  part2 = () => {
    const pointList = getPointList(this.input);
    const cardCount = new Map<number, number>();
    pointList.forEach((p, i) => cardCount.set(i, 1));
    for (let i = 0; i < pointList.length; i++) {
      const p = pointList[i];
      if (p === 0) continue;
      const currentCount = cardCount.get(i);
      for (let j = 1; j <= p; j++)
        cardCount.set(i + j, cardCount.get(i + j) + currentCount);
    }
    return Array.from(cardCount.values()).reduce(sum, 0);
  }
}

const getPointList = (input: string[]) => input.map(l => {
  const [_, numbers] = l.split(':', 2);
  const [winningSeq, mineSeq] = numbers.split(' | ', 2);
  const winning = winningSeq.split(/\s+/).map(Number);
  const mine = mineSeq.split(/\s+/).map(Number).filter(n => n > 0);
  return { winning, mine };
}).map(({ winning, mine }) => {
  return winning.filter(n => mine.includes(n)).length;
});
