import { Day, PriorityQueue, Vector } from "../aoc";
import * as process  from 'node:process';

export class Day23 extends Day {
  day = (): number => 23;
  part1 = () => {
    return '';
  }
  part2 = () => {
    const result = solve(this.input);
    const tooLow = 5547;
    const not = [5570, 6354];

    if (result <= tooLow) console.log(`\x1b[31m    !!!!\n${result} is too low\n    !!!!\x1b[0m`);
    if (not.includes(result)) console.log(`\x1b[31m    !!!!\n${result} is not the answer\n    !!!!\x1b[0m`);

    return result;
  }
}

const solve = (map: string[]) => {
  const vertices = getVertices(map);

  const start = vertices.get(getStart(map).toString())!;
  const end = vertices.get(getEnd(map).toString())!;

  const finalRoutes: Route[] = [];
  let longestFinalRoute = 0;

  const prioForRoute = (route: Route) => route.visited.size;

  const priorityQueue = new PriorityQueue<RouteWithId>();

  priorityQueue.add(addId({ vertex: start, visited: new Set<string>([start.id]), distance: 0 }), 0);

  const longestRouteToVertex = new Map<string, number>();

  let i = 0;
  while (!priorityQueue.isEmpty()) {
    if(i++ % 100_000 === 0) {
      process.stdout.write('\x1B[1A\x1B[2K');
      console.log(`Queue size: ${priorityQueue.size}, routes found: ${finalRoutes.length}, longest route: ${finalRoutes.reduce((max, route) => Math.max(max, route.distance), 0)}`);
    }
    const { vertex, visited, distance } = priorityQueue.extractBest();

    // drawMapWithHighlightedVectors(this.input, vertex.pos, vertex.edges.map(e => e.to.pos));

    // console.log(`Vertex ${vertex.id}; dist=${distance} seen=${visited.size} queue=${priorityQueue.size} edges=${vertex.edges.length}`);

    vertex.edges.forEach(edge => {
      const to = edge.to;
      if (visited.has(to.id)) return;
      const newRoute: Route = {
        vertex: to,
        visited: new Set<string>(visited).add(to.id),
        distance: distance + edge.distance
      };

      if (to.id === end.id) {
        if(newRoute.distance > longestFinalRoute) {
          finalRoutes.push(newRoute);
          longestFinalRoute = newRoute.distance;
        }
        return;
      }
      if (longestRouteToVertex.has(to.id) && longestRouteToVertex.get(to.id)! > newRoute.distance + 5000) return;
      longestRouteToVertex.set(to.id, newRoute.distance);
      priorityQueue.add(addId(newRoute), prioForRoute(newRoute));
    });
  }

  return finalRoutes.reduce((max, route) => Math.max(max, route.distance), 0);
}

type Route = {
  vertex: Vertex;
  visited: Set<string>;
  distance: number;
}

type RouteWithId = Route & { id: string };
const addId = (route: Route): RouteWithId => ({ ...route, id: `${route.vertex.id}:${[...route.visited].join(',')}` });

type Vertex = {
  id: string;
  pos: Vector;
  edges: Edge[];
  connections: Vector[];
}

type Edge = {
  from: Vertex;
  to: Vertex;
  distance: number;
}

const makeVertex = (pos: Vector, connections: Vector[]): Vertex => ({
  id: pos.toString(),
  pos,
  edges: [],
  connections
});

const getStart = (map: string[]): Vector => {
  const x = map[0].split('').findIndex(c => c === '.')!;
  return new Vector(x, 0);
}

const getEnd = (map: string[]): Vector => {
  const y = map.length - 1;
  const x = map[y].split('').findIndex(c => c === '.')!;
  return new Vector(x, y);
}
const directions = [Vector.north(), Vector.south(), Vector.east(), Vector.west()];

const getTraversableNeighborsForVector = (map: string[], pos: Vector) =>
  directions
    .map(d => d.add(pos))
    .filter(p => map[p.y]?.[p.x] !== undefined && map[p.y][p.x] !== '#');

const getVertices = (map: string[]) => {
  const startVertex = makeVertex(getStart(map), getTraversableNeighborsForVector(map, getStart(map)));
  const endVertex = makeVertex(getEnd(map), getTraversableNeighborsForVector(map, getEnd(map)));
  const vertices = new Map<string, Vertex>([[startVertex.id, startVertex], [endVertex.id, endVertex]]);
  map.forEach((line, y) => {
    line.split('').forEach((c, x) => {
      if (c === '#') return;
      const pos = new Vector(x, y);
      const neighbors = getTraversableNeighborsForVector(map, pos);
      if (neighbors.length < 3) return; // only consider junctions
      const vertex = makeVertex(pos, neighbors);
      vertices.set(vertex.id, vertex);
    });
  });

  // drawMapWithHighlightedVectors(map, startVertex.pos, [...vertices.values()].map(v => v.pos));

  // find edges between vertices
  vertices.forEach(vertex => {
    vertex.edges = vertex.connections.flatMap(connection => {
      const seen = new Set<string>([vertex.id, connection.toString()]);
      let distance = 1;
      let next = connection;
      // let path: Vector[] = [];
      while (!vertices.has(next.toString())) {
        const neighbors = directions
          .map(d => d.add(next))
          .filter(p => map[p.y]?.[p.x] !== undefined && map[p.y][p.x] !== '#' && !seen.has(p.toString()));

        if (neighbors.length < 1) return []; // Dead end
        next = neighbors[0];
        seen.add(next.toString());
        distance++;
      }

      return [{ from: vertex, to: vertices.get(next.toString())!, distance }];
    })
  });

  return vertices;
}

const drawMapWithHighlightedVectors = (map: string[], pos: Vector, vectors: Vector[]) => {
  const lines = map.map(l => l.split(''));
  vectors.forEach(v => lines[v.y][v.x] = '█');
  lines[pos.y][pos.x] = '▒';
  console.log(lines.map(l => l.join('')).join('\n'));
}