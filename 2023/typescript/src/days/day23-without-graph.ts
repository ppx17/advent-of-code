import { Day, PriorityQueue, Vec, Vector } from "../aoc";


const TUNE_ALLOW_SAME_PATH_LIMIT = 200;
const makePriority = (end: Vec, mapSize: number) => (node: Node) => node.pos.manhattan(end);


export class Day23 extends Day {
  day = (): number => 23;
  // part1 = () => traverseMap(this.input, getStart(this.input), getEnd(this.input))
  part1 = () => '';
  part2 = () => {
    const tooLow = 5547;
    const not = [5570];
    const result = traverseMap(this.input, getStart(this.input), getEnd(this.input), true)

    if(result <= tooLow) console.log(`\x1b[31m    !!!!\n${result} is too low\n    !!!!\x1b[0m`);
    if(not.includes(result)) console.log(`\x1b[31m    !!!!\n${result} is not the answer\n    !!!!\x1b[0m`);

    return result;
  }
  // part2 = () => '';
  // 4946 too low
  // 5546 too low
  // 5547 too low
}


const getStart = (map: string[]): Vector => {
  const x = map[0].split('').findIndex(c => c === '.')!;
  return new Vector(x, 0);
}

const getEnd = (map: string[]): Vector => {
  const y = map.length - 1;
  const x = map[y].split('').findIndex(c => c === '.')!;
  return new Vector(x, y);
}

type Node = {
  pos: Vector,
  visited: Set<string>
};

type NodeWithId = Node & { id: string };
const addId = (node: Node): NodeWithId => ({ ...node, id: `${node.pos.toString()}:${node.visited.size}` });

const oneWay: Record<string, Vector> = {
  '^': Vector.north(),
  'v': Vector.south(),
  '>': Vector.east(),
  '<': Vector.west(),
}

const traverseMap = (map: string[], start: Vector, end: Vector, ignoreOneWay = false) => {
  const mapSize = map.map(l => l.split('')).flat().filter(c => c === '.').length;

  const routes: Node[] = [];
  const routeLengths = new Map<string, number>();
  const queue = new PriorityQueue<NodeWithId>();
  const priority = makePriority(end, mapSize);
  const startNode = { pos: start, visited: new Set<string>() };
  queue.add(addId(startNode), priority(startNode))

  let i = 0;
  while (!queue.isEmpty()) {
    if(i++ % 10_000 === 0) console.log(queue.size);
    const { pos, visited } = queue.extractBest();

    // if((routeLengths.get(pos.toString()) ?? 0) > visited.size) continue;

    [Vector.north(), Vector.south(), Vector.east(), Vector.west()]
      .map(v => v.add(pos))
      .filter(v => map[v.y]?.[v.x] !== undefined && map[v.y][v.x] !== '#' && !visited.has(v.toString()))
      .filter(v => {
        if(ignoreOneWay) return true;
        const direction = v.sub(pos);
        const tile = map[v.y][v.x];
        return tile === '.' || !!oneWay[tile]?.is(direction);
      })
      .forEach(v => {
        const newNode = { pos: v, visited: new Set<string>(visited).add(v.toString()) };
        if(v.is(end)) {
          routes.push(newNode);
          return;
        }
        // if((routeLengths.get(newNode.pos.toString()) ?? 0) > newNode.visited.size + TUNE_ALLOW_SAME_PATH_LIMIT) return;
        routeLengths.set(newNode.pos.toString(), newNode.visited.size);
        queue.add(addId(newNode), priority(newNode));
      });
  }

  const longestRoute = routes.sort((a, b) => b.visited.size - a.visited.size)[0];

  // print map with the longest route
  map.forEach((l, y) => {
    const line = l.split('');
    l.split('').forEach((c, x) => {
      const pos = new Vector(x, y);
      if(longestRoute.visited.has(pos.toString())) line[x] = '█';
    });
    console.log(line.join(''));
  });

  return routes.reduce((acc, route) => Math.max(acc, route.visited.size), 0);
}