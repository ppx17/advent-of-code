import { Day, Vector } from "../aoc";

export class Day11 extends Day {
  day = (): number => 11;
  part1 = () => solve(this.input);
  part2 = () => solve(this.input, 999_999);
}

const solve = (input: string[], expansion = 1) => {
  const map = input
    .map(line => line.split(''));

  const isColEmpty = (x: number) => map.map(line => line[x]).every(c => c === '.');
  const emptyCols = map.map((line, y) => isColEmpty(y));

  const galaxies: Vector[] = [];
  let emptyY = 0;
  for (let y = 0; y < map.length; y++) {
    let emptyX = 0;
    if (map[y].every((c) => c === '.')) emptyY++;
    for (let x = 0; x < map[y].length; x++) {
      if (emptyCols[x] ?? false) emptyX++;
      if (map[y][x] === '#') {
        const v = new Vector(x + (emptyX * expansion), y + (emptyY * expansion), 0);
        galaxies.push(v);
      }
    }
  }

  const seen = new Set<string>();
  let sum = 0;
  for (let ai = 0; ai < galaxies.length - 1; ai++) {
    for (let bi = ai + 1; bi < galaxies.length; bi++) {
      const a = galaxies[ai];
      const b = galaxies[bi];
      if (seen.has(a.serialize() + b.serialize())) continue;
      sum += a.manhattan(b);
      seen.add(a.serialize() + b.serialize());
    }
  }
  return sum;
}
