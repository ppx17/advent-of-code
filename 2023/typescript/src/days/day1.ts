import { Day, sum } from "../aoc";

export class Day1 extends Day {
    day = (): number => 1;
    part1 = () => this.input
        .map(l => Number(`${firstDigit(l)}${lastDigit(l)}`))
        .reduce(sum)
    part2 = () => this.input
        .map(l => Number(`${firstDigitPart2(l)}${lastDigitPart2(l)}`))
        .reduce(sum)
}


const isDigit = (c: string) => c >= '0' && c <= '9';
const firstDigit = (c: string) => {
    for (let i = 0; i < c.length; i++) if (isDigit(c[i])) return c[i];
}
const lastDigit = (c: string) => {
    for (let i = c.length - 1; i >= 0; i--) if (isDigit(c[i])) return c[i];
}

const words: [string, string][] = [
    ['one', '1'],
    ['two', '2'],
    ['three', '3'],
    ['four', '4'],
    ['five', '5'],
    ['six', '6'],
    ['seven', '7'],
    ['eight', '8'],
    ['nine', '9'],
]

const firstDigitPart2 = (l: string) => {
    for (let i = 0; i < l.length; i++) {
        if (isDigit(l[i])) return l[i];
        for (const [word, digit] of words) {
            if (l.substring(i, i + word.length) === word) return digit;
        }
    }
}

const lastDigitPart2 = (l: string) => {
    for (let i = l.length - 1; i >= 0; i--) {
        if (isDigit(l[i])) return l[i];
        for (const [word, digit] of words) {
            if (l.substring(i - word.length + 1, i + 1) === word) return digit;
        }
    }
}