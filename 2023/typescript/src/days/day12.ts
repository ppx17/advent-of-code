import { Day, sum } from "../aoc";

export class Day12 extends Day {
  day = (): number => 12;
  part1 = () =>
    parseInput(this.input)
      .map(([springs, groups]) => solveForLine(springs, groups))
      .reduce(sum, 0)
  part2 = () =>
    parseInput(this.input)
      .map(([springs, groups]): [string, number[]] => [
        [springs, springs, springs, springs, springs].join('?'),
        [...groups, ...groups, ...groups, ...groups, ...groups]
      ])
      .map(([springs, groups]) => solveForLine(springs, groups))
      .reduce(sum, 0)
}

const solveForLine = (springs: string, groups: number[]) => {
  springs = springs + '.';
  const maxDamagedSprings = makeArray(springs.length, () => 0);
  for (let i = 0; i < springs.length; i++) {
    if (springs[i] === '.') continue;
    maxDamagedSprings[i + 1] = maxDamagedSprings[i] + 1;
  }

  const possibleOptions = makeArray(groups.length + 1,
    () => makeArray(springs.length + 1, () => 0)
  );

  possibleOptions[0][0] = 1;
  for (let i = 0; i < springs.length; i++) {
    if (springs[i] === '#') break;
    possibleOptions[0][i + 1] = 1;
  }

  for (let i = 0; i < groups.length; i++) {
    const group = groups[i];
    for (let springIdx = 0; springIdx < springs.length; springIdx++) {
      if (springs[springIdx] === '#') continue;
      possibleOptions[i + 1][springIdx + 1] = possibleOptions[i + 1][springIdx] + Number(group <= maxDamagedSprings[springIdx] && possibleOptions[i][springIdx - group]);
    }
  }

  return possibleOptions[groups.length][springs.length];
}

const parseInput = (input: string[]) => {
  return input.map((line): [string, number[]] => {
    const [springs, groupsString] = line.split(' ');
    const groups = groupsString.split(',').map((g) => Number(g));
    return [springs, groups];
  });
}

const makeArray = <T>(length: number, value: () => T): T[] => Array.from({ length }).map(() => value());