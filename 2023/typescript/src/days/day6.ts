import { Day, product } from "../aoc";

export class Day6 extends Day {
  day = (): number => 6;
  part1 = () => {
    const times = this.input[0].split(/\s+/).slice(1).map(Number);
    const distances = this.input[1].split(/\s+/).slice(1).map(Number);
    return times.map((time, idx) => {
      let sum = 0;
      for(let hold = 1; hold < time; hold++) {
        sum += Number((time - hold) * hold > distances[idx]);
      }
      return sum;
    }).reduce(product);
  }
  part2 = () => {
    const time = Number(this.input[0].replaceAll(/\D/g, ''));
    const distance = Number(this.input[1].replaceAll(/\D/g, ''));
    let sum = 0;
    const minFactor = Math.floor(distance / time);
    for(let hold = minFactor; hold < (time - minFactor); hold++) {
      sum += Number((time - hold) * hold > distance);
    }
    return sum;
  }
}
