import { Day, sum, Vector } from "../aoc";

function whoSupportsWho(settled: Brick[]) {
  const bricksInPositions = new Map<string, number>();
  settled.forEach((b, i) => b.cubes.forEach(c => bricksInPositions.set(c.toString(), i)));

  const isSupportedBy: Array<number[]> = settled.map(() => []);
  settled.forEach((brick, index) => {
    brick.cubes.forEach(c => {
      const below = c.add(down);
      const belowBrick = bricksInPositions.get(below.toString());
      if (belowBrick !== undefined && belowBrick !== index) {
        isSupportedBy[belowBrick].push(index);
      } else if (below.z === 0) {
        // Supported by ground
        isSupportedBy[settled.length] ??= [];
        isSupportedBy[settled.length].push(index);
      }
    });
  });
  return isSupportedBy;
}

export class Day22 extends Day {
  day = (): number => 22;
  part1 = () => {
    const settled = settle(parseInput(this.input));
    const isSupportedBy = whoSupportsWho(settled);

    return isSupportedBy
      .map((_, i) => Number((new Set(isSupportedBy.slice(0, i).concat(isSupportedBy.slice(i + 1)).flat())).size === settled.length))
      .reduce(sum);
  }
  part2 = () => {
    const bricks = parseInput(this.input);
    const settled = settle(bricks);
    const isSupportedBy = whoSupportsWho(settled);
    return bricks.map((_, i) => letTheCookieCrumble(isSupportedBy, i)).reduce(sum);
  }
}

const letTheCookieCrumble = (imSupporting: number[][], crumbleIdx: number) => {
  const floor = imSupporting.length - 1;

  const imSupportedBy = imSupporting.map(() => new Set<number>());
  imSupporting.forEach((supporting, supporter) => {
    supporting.forEach(s => imSupportedBy[s].add(supporter));
  });


  const fallingBrickIndices = [crumbleIdx];
  const alreadyFallen = new Set<number>([crumbleIdx]);
  while (fallingBrickIndices.length > 0) {
    const brickIdx = fallingBrickIndices.shift()!;
    const supportedIndices = imSupporting[brickIdx];

    if (supportedIndices.length === 0) continue;

    supportedIndices.forEach((supportedIdx) => imSupportedBy[supportedIdx].delete(brickIdx));

    imSupportedBy.forEach((supporters, brick) => {
      if (supporters.size !== 0 || alreadyFallen.has(brick) || brick === floor) return;
      alreadyFallen.add(brick);
      fallingBrickIndices.push(brick);
    });
  }

  return alreadyFallen.size - 1;
}

type  Brick = {
  cubes: Vector[],
  myBricks: Set<string>,
}

const settle = (bricks: Brick[]): Brick[] => {
  const currentlyOccupied = new Set<string>();
  bricks.forEach(b => b.cubes.forEach(c => currentlyOccupied.add(c.toString())));

  let hasMoved = true;
  while (hasMoved) {
    hasMoved = false;
    for (const [i, brick] of bricks.entries()) {
      const movedBrick = moveDown(brick);
      if (movedBrick.cubes.every(c => {
        if (c.z <= 0) return false;
        if (brick.myBricks.has(c.toString())) return true;
        return !currentlyOccupied.has(c.toString());
      })) {
        bricks[i] = movedBrick;
        brick.myBricks.forEach(b => currentlyOccupied.delete(b));
        movedBrick.myBricks.forEach(b => currentlyOccupied.add(b));
        hasMoved = true;
      }
    }
  }

  return bricks;
}


const parseInput = (input: string[]): Brick[] => {
  return input.map(line => {
    const cubes: Vector[] = [];
    const [startString, endString] = line.split('~');
    const start = new Vector(...startString.split(',').map(Number));
    const end = new Vector(...endString.split(',').map(Number));

    for (let x = start.x; x <= end.x; x++) {
      for (let y = start.y; y <= end.y; y++) {
        for (let z = start.z; z <= end.z; z++) {
          cubes.push(new Vector(x, y, z));
        }
      }
    }

    return { cubes, myBricks: new Set(cubes.map(c => c.toString())) };
  });
}

const down = new Vector(0, 0, -1);
const moveDown = (brick: Brick): Brick => {
  const cubes = brick.cubes.map(c => c.add(down));
  return {
    cubes,
    myBricks: new Set(cubes.map(c => c.toString())),
  }
}