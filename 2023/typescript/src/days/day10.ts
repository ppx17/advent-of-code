import { Day, Vector } from "../aoc";

const DRAW_MAP = false;

export class Day10 extends Day {
  day = (): number => 10;
  part1 = () => {
    const { tiles, startTile } = parseInput(this.input);
    const traveler = initializeTraveler({ tiles, startTile });
    return travelPipe(traveler, tiles).history.size / 2;
  }
  part2 = () => {
    // Just to make sure we ain't squeezing out of the map
    const input = padMatrix(this.input);

    const { tiles, startTile } = parseInput(input);
    const traveler = travelPipe(initializeTraveler({ tiles, startTile }), tiles);
    startTile.connections = findSurroundingTilesPointingAt(tiles, startTile).map(t => t.location.sub(startTile.location));
    const regions = groupRegions(tiles, traveler);

    const edgeTilesSet = new Set<string>(Array.from(tiles.values())
      .filter(t => t.location.x === 0 || t.location.y === 0 || t.location.x === input[0].length - 1 || t.location.y === input.length - 1)
      .map(t => t.location.serialize())
    );

    // filter out regions that touch the edge of the map
    const edgeRegions = regions.filter(r => r.some(t => edgeTilesSet.has(t.location.serialize())));
    const nonEdgeRegions = regions.filter(r => !r.some(t => edgeTilesSet.has(t.location.serialize())));

    const inEdgeRegions = new Set<string>();
    for (const region of edgeRegions) {
      for (const tile of region) {
        inEdgeRegions.add(tile.location.serialize());
      }
    }

    const enclosed = new Set<string>();
    for (const region of nonEdgeRegions) {
      for (const tile of region) {
        enclosed.add(tile.location.serialize());
      }
    }

    if (DRAW_MAP) drawMap(input, enclosed, edgeTilesSet, inEdgeRegions);

    return enclosed.size;
  }
}

const drawMap = (input: string[], enclosed: Set<string>, edgeTilesSet: Set<string>, inEdgeRegions: Set<string>) => {
  for (let y = 0; y < input.length; y++) {
    let line = '';
    for (let x = 0; x < input[y].length; x++) {
      const v = new Vector(x, y).serialize();
      const char = enclosed.has(v) ?
        '▇'
        : (edgeTilesSet.has(v)
          ? '▒'
          : (inEdgeRegions.has(v) ?
            '░'
            : input[y][x]));
      line += char;
    }
    console.log(line);
  }
}

type Traveler = {
  history: Set<string>;
  currentTile: Tile;
  previousTile: Tile;
}

const padMatrix = (matrix: string[]) => {
  const width = matrix[0].length + 2;
  return [
    '.'.repeat(width),
    ...matrix.map(l => `.${l}.`),
    '.'.repeat(width),
  ];
}

const initializeTraveler = ({ tiles, startTile }: ParsedMap): Traveler =>
  findSurroundingTilesPointingAt(tiles, startTile)
    .map((t): Traveler => ({
      history: new Set([startTile, t].map(t => t.location.serialize())),
      currentTile: t,
      previousTile: startTile
    }))[0];

const travelPipe = (traveler: Traveler, tiles: Map<string, Tile>) => {
  while (true) {
    const options = traveler.currentTile.connections
      .map(connectionVector => traveler.currentTile.location.add(connectionVector))
      .map(locationVector => tiles.get(locationVector.serialize()))
      .filter(tile => !traveler.previousTile.location.is(tile.location));

    if (options.length !== 1) throw new Error(`Unexpected number of options: ${options.length}`);
    const nextTile = options[0];

    if (traveler.history.has(nextTile.location.serialize())) {
      return traveler;
    }

    traveler.history.add(nextTile.location.serialize());
    const previousTile = traveler.currentTile;
    traveler.currentTile = nextTile;
    traveler.previousTile = previousTile;
  }
};

const isPartOfLoop = (tile: Tile, traveler: Traveler) => isVectorPartOfLoop(tile.location, traveler);
const isVectorPartOfLoop = (vector: Vector, traveler: Traveler) => traveler.history.has(vector.serialize());

const groupRegions = (map: Map<string, Tile>, traveler: Traveler) => {
  const nonLoopTiles = Array.from(map.values()).filter(t => !isPartOfLoop(t, traveler));
  const regions: Tile[][] = [];
  const inSomeRegion = new Set<string>();

  for (const tile of nonLoopTiles) {
    if (inSomeRegion.has(tile.location.serialize())) continue;
    const inThisRegion = new Set<string>();
    const seenByThisQueue = new Set<string>([tile.location.serialize()]);
    const queue: (Tile | SqueezeThroughOpportunity)[] = [tile];
    const region: Tile[] = [];
    while (queue.length) {
      const currentTile = queue.shift()!;
      if (isTile(currentTile)) {
        if (inThisRegion.has(currentTile.location.serialize())) continue;
        inThisRegion.add(currentTile.location.serialize());
        inSomeRegion.add(currentTile.location.serialize());
        region.push(currentTile);
        const neighbors = surroundings
          .map(v => currentTile.location.add(v))
          .map(v => map.get(v.serialize()))
          .filter(t => t !== undefined && !isPartOfLoop(t, traveler) && !seenByThisQueue.has(t.location.serialize()));

        const squeezeThroughOpportunities = squeezeGaps
          .map(({ dirs, noConTo, direction }) => ({
            noConTo,
            direction,
            dirs: [map.get(currentTile.location.add(dirs[0]).serialize()), map.get(currentTile.location.add(dirs[1]).serialize())]
          }))
          .filter(({ dirs, noConTo }) => {
            const [left, right] = dirs;
            const [leftNoCon, rightNoCon] = noConTo;
            return left !== undefined
              && right !== undefined
              && isPartOfLoop(left, traveler)
              && isPartOfLoop(right, traveler)
              && !left.connections.some(c => c.is(leftNoCon))
              && !right.connections.some(c => c.is(rightNoCon))
          }).map(({ dirs, direction }) => {
            const [left, right] = dirs;
            return <SqueezeThroughOpportunity>{
              a: left,
              b: right,
              direction,
            }
          }).filter(o => !seenByThisQueue.has(serializeSqueezeThroughOpportunity(o)));

        const squeezeDiagonally = [
          { direction: Vector.NE(), pipes: [Vector.north(), Vector.east()] },
          { direction: Vector.SE(), pipes: [Vector.south(), Vector.east()] },
          { direction: Vector.SW(), pipes: [Vector.south(), Vector.west()] },
          { direction: Vector.NW(), pipes: [Vector.north(), Vector.west()] },
        ].flatMap(({ direction, pipes }) => {
          const shouldBePartOfPipe = pipes
            .map(p => currentTile.location.add(p))
            .map(v => map.get(v.serialize()))
            .filter(t => t !== undefined && isPartOfLoop(t, traveler));
          if (shouldBePartOfPipe.length !== 2) return [];

          const tileOnOtherSide = map.get(currentTile.location.add(direction).serialize());
          if (tileOnOtherSide === undefined || isPartOfLoop(tileOnOtherSide, traveler)) return [];

          return [tileOnOtherSide];
        }).filter(t => !seenByThisQueue.has(t.location.serialize()))

        queue.push(...neighbors, ...squeezeThroughOpportunities, ...squeezeDiagonally);
        neighbors.forEach(t => seenByThisQueue.add(t.location.serialize()));
        squeezeDiagonally.forEach(t => seenByThisQueue.add(t.location.serialize()));
        squeezeThroughOpportunities.forEach(o => seenByThisQueue.add(serializeSqueezeThroughOpportunity(o)));
      } else {
        for (const instruction of nextSqueezeInstructions(currentTile)) {
          const opp: SqueezeThroughOpportunity = {
            a: map.get(instruction.aLoc.serialize()),
            b: map.get(instruction.bLoc.serialize()),
            direction: instruction.direction
          };

          const notPartOfLoop = [opp.a, opp.b]
            .filter(t => t !== undefined
              && !isPartOfLoop(t, traveler)
            );
          if (notPartOfLoop.length) {
            // Squeezed through to another region, start exploring it
            const canExplore = notPartOfLoop.filter(t => !seenByThisQueue.has(t.location.serialize()));
            queue.push(...canExplore);
            canExplore.forEach(t => seenByThisQueue.add(t.location.serialize()));
            continue;
          }

          if (opp.a === undefined || opp.b === undefined) {
            continue;
          }

          const noConTo = squeezeGaps.find(g => g.direction.is(opp.direction))!.noConTo;

          const [leftNoCon, rightNoCon] = noConTo;
          if (!opp.a.connections.some(c => c.is(leftNoCon)) && !opp.b.connections.some(c => c.is(rightNoCon))) {
            // We can continue squeezing
            queue.push(opp);
            seenByThisQueue.add(serializeSqueezeThroughOpportunity(opp));
          }
        }
      }
    }
    regions.push(region);
  }
  return regions;
};

type SqueezeInstruction = { direction: Vector, aLoc: Vector, bLoc: Vector };
type SqueezeInstructions = SqueezeInstruction[];
const nextSqueezeInstructions = (currentOpportunity: SqueezeThroughOpportunity): SqueezeInstructions => {

  const instructions = [{
    direction: currentOpportunity.direction,
    aLoc: currentOpportunity.a.location.add(currentOpportunity.direction),
    bLoc: currentOpportunity.b.location.add(currentOpportunity.direction),
  }];

  if (currentOpportunity.direction.is(Vector.north())) {
    const left = currentOpportunity.a.location.x < currentOpportunity.b.location.x ? currentOpportunity.a : currentOpportunity.b;
    const right = currentOpportunity.a.location.x < currentOpportunity.b.location.x ? currentOpportunity.b : currentOpportunity.a;
    instructions.push({
        direction: currentOpportunity.direction.rotateLeft(),
        aLoc: right.location.add(currentOpportunity.direction).add(currentOpportunity.direction.rotateLeft()),
        bLoc: left.location,
      },
      {
        direction: currentOpportunity.direction.rotateRight(),
        aLoc: left.location.add(currentOpportunity.direction).add(currentOpportunity.direction.rotateRight()),
        bLoc: right.location
      });
  }

  if (currentOpportunity.direction.is(Vector.south())) {
    const left = currentOpportunity.a.location.x < currentOpportunity.b.location.x ? currentOpportunity.a : currentOpportunity.b;
    const right = currentOpportunity.a.location.x < currentOpportunity.b.location.x ? currentOpportunity.b : currentOpportunity.a;
    instructions.push({
        direction: currentOpportunity.direction.rotateLeft(),
        aLoc: right.location,
        bLoc: left.location.add(currentOpportunity.direction).add(currentOpportunity.direction.rotateLeft())
      },
      {
        direction: currentOpportunity.direction.rotateRight(),
        aLoc: left.location,
        bLoc: right.location.add(currentOpportunity.direction).add(currentOpportunity.direction.rotateRight()),
      });
  }

  if (currentOpportunity.direction.is(Vector.east())) {
    const top = currentOpportunity.a.location.y < currentOpportunity.b.location.y ? currentOpportunity.a : currentOpportunity.b;
    const bottom = currentOpportunity.a.location.y < currentOpportunity.b.location.y ? currentOpportunity.b : currentOpportunity.a;
    instructions.push({
        direction: currentOpportunity.direction.rotateLeft(),
        aLoc: top.location,
        bLoc: bottom.location.add(currentOpportunity.direction).add(currentOpportunity.direction.rotateLeft()),
      },
      {
        direction: currentOpportunity.direction.rotateRight(),
        aLoc: bottom.location,
        bLoc: top.location.add(currentOpportunity.direction).add(currentOpportunity.direction.rotateRight()),
      });
  }

  if (currentOpportunity.direction.is(Vector.west())) {
    const top = currentOpportunity.a.location.y < currentOpportunity.b.location.y ? currentOpportunity.a : currentOpportunity.b;
    const bottom = currentOpportunity.a.location.y < currentOpportunity.b.location.y ? currentOpportunity.b : currentOpportunity.a;
    instructions.push({
        direction: currentOpportunity.direction.rotateLeft(),
        aLoc: top.location.add(currentOpportunity.direction).add(currentOpportunity.direction.rotateLeft()),
        bLoc: bottom.location
      },
      {
        direction: currentOpportunity.direction.rotateRight(),
        aLoc: bottom.location.add(currentOpportunity.direction).add(currentOpportunity.direction.rotateRight()),
        bLoc: top.location,
      });
  }

  return instructions;
};

const squeezeGaps = [
  { dirs: [Vector.SW(), Vector.south()], noConTo: [Vector.east(), Vector.west()], direction: Vector.south() },
  { dirs: [Vector.south(), Vector.SE()], noConTo: [Vector.east(), Vector.west()], direction: Vector.south() },

  { dirs: [Vector.NW(), Vector.north()], noConTo: [Vector.east(), Vector.west()], direction: Vector.north() },
  { dirs: [Vector.north(), Vector.NE()], noConTo: [Vector.east(), Vector.west()], direction: Vector.north() },

  { dirs: [Vector.NE(), Vector.east()], noConTo: [Vector.south(), Vector.north()], direction: Vector.east() },
  { dirs: [Vector.east(), Vector.SE()], noConTo: [Vector.south(), Vector.north()], direction: Vector.east() },

  { dirs: [Vector.NW(), Vector.west()], noConTo: [Vector.south(), Vector.north()], direction: Vector.west() },
  { dirs: [Vector.west(), Vector.SW()], noConTo: [Vector.south(), Vector.north()], direction: Vector.west() },
];

const isTile = (tileOrOpportunity: Tile | SqueezeThroughOpportunity): tileOrOpportunity is Tile =>
  (tileOrOpportunity as Tile).connections !== undefined;

const serializeSqueezeThroughOpportunity = (opportunity: SqueezeThroughOpportunity) =>
  `squeeze-${[opportunity.a, opportunity.b].sort(sortTiles).map(o => o.location.serialize()).join('-')}}`;

const sortTiles = (a: Tile, b: Tile) => a.location.serialize().localeCompare(b.location.serialize());

type SqueezeThroughOpportunity = {
  a: Tile;
  b: Tile;
  direction: Vector;
}

type Tile = {
  isPipe: boolean;
  location: Vector;
  connections: Vector[];
}

const tileTypes = {
  '|': { isPipe: true, connections: [Vector.north(), Vector.south()] },
  '-': { isPipe: true, connections: [Vector.east(), Vector.west()] },
  'L': { isPipe: true, connections: [Vector.north(), Vector.east()] },
  'J': { isPipe: true, connections: [Vector.north(), Vector.west()] },
  '7': { isPipe: true, connections: [Vector.south(), Vector.west()] },
  'F': { isPipe: true, connections: [Vector.south(), Vector.east()] },
  '.': { isPipe: false, connections: [] },
  'S': { isPipe: true, isStart: true },
};

const surroundings = [
  Vector.north(),
  Vector.south(),
  Vector.east(),
  Vector.west(),
];

const findSurroundingTilesPointingAt = (tiles: Map<string, Tile>, tile: Tile) => {
  const surroundingTiles = surroundings.map(s => tiles.get(tile.location.add(s).serialize()));
  return surroundingTiles.filter(t => t && t.connections.some(c => t.location.add(c).is(tile.location)));
}

type ParsedMap = {
  startTile: Tile;
  tiles: Map<string, Tile>;
}

const parseInput = (input: string[]): ParsedMap => {
  const tiles = new Map<string, Tile>();
  let startTile;
  for (let y = 0; y < input.length; y++) {
    for (let x = 0; x < input[y].length; x++) {
      const char = input[y][x];
      const tileType = tileTypes[char];
      if (!tileType) throw new Error(`Unknown tile type ${char}`);
      const location = new Vector(x, y);
      const tile = {
        ...tileType,
        location,
      };
      tiles.set(location.serialize(), tile);
      if (tileType.isStart) {
        if (startTile) throw new Error(`Multiple start tiles found`);
        startTile = tile;
      }
    }
  }
  if (!startTile) throw new Error(`No start tile found`);
  return { tiles, startTile };
}