import { Day, sum } from "../aoc";

export class Day15 extends Day {
  day = (): number => 15;
  cmds = () => this.input[0].split(',');
  part1 = () => this.cmds().map(HASHAlgorithm).reduce(sum, 0)
  part2 = () => HASHMAP(this.cmds().map(parseCommand))
}

const HASHAlgorithm = (input: string): number => {
  let hash = 0;
  for (let i = 0; i < input.length; i++) {
    hash += input.charCodeAt(i);
    hash *= 17;
    hash %= 256;
  }
  return hash;
}

type Lens = { label: string, focalLength: number };

const HASHMAP = (commands: Command[]): number => {
  const boxes = new Map<number, Lens[]>();

  commands.forEach(command => {
    const box = HASHAlgorithm(command.label);
    const content = boxes.get(box) ?? [];
    switch (command.op) {
      case 'set':
        const labelIdx = content.findIndex(b => b.label === command.label);
        (labelIdx >= 0)
          ? (content[labelIdx] = command)
          : content.push(command);
        boxes.set(box, content);
        break;
      case 'sub':
        boxes.set(box, content.filter(b => b.label !== command.label));
        break;
    }
  });

  return Array.from(boxes.entries())
    .map(([box, lenses]) =>
      lenses
        .map(({ focalLength }, slot) => (1 + box) * (1 + slot) * focalLength)
        .reduce(sum, 0))
    .reduce(sum, 0);
}

type Command = { op: 'set', label: string, focalLength: number } | { op: 'sub', label: string };

const parseCommand = (raw: string): Command => {
  if (raw.includes('=')) {
    const [label, value] = raw.split('=', 2);
    return { op: 'set', label, focalLength: Number(value) };
  }
  if (raw.includes('-')) return { op: 'sub', label: raw.substring(0, raw.length - 1) };
  throw new Error(`Unknown command ${raw}`);
}