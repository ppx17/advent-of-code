<?php


namespace Ppx17\Aoc2023\Aoc\Days;


use Ppx17\AocRunner\DayInterface;
use Ppx17\AocRunner\AbstractDay as BaseAbstractDay;

abstract class AbstractDay extends BaseAbstractDay implements DayInterface
{
    public function getInputIntCode(): array
    {
        return array_map('intval', $this->getInputCsv());
    }
}
