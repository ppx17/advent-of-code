<?php


namespace Ppx17\Aoc2023\Aoc\Days;


class Day1 extends AbstractDay
{
    public function dayNumber(): int
    {
        return 1;
    }

    public function part1(): string
    {
      return collect($this->getInputLines())
        ->map(fn($line) => (int)findFirstDigit($line) . findLastDigit($line))
        ->sum();
    }

    public function part2(): string
    {
      return collect($this->getInputLines())
        ->map(fn($line) => (int)findFirstDigitWithWords($line) . findLastDigitWithWords($line))
        ->sum();
    }
}

function findFirstDigit(string $line): string {
  return collect(str_split($line))->first(fn($c) => ctype_digit($c));
}

function findLastDigit(string $line): string {
  return collect(str_split($line))->last(fn($c) => ctype_digit($c));
}

function words(): array {
  return [
    'one' => '1',
    'two' => '2',
    'three' => '3',
    'four' => '4',
    'five' => '5',
    'six' => '6',
    'seven' => '7',
    'eight' => '8',
    'nine' => '9'
  ];
}

function findFirstDigitWithWords(string $line): string {
  for($i = 0; $i < strlen($line); $i++) {
    if (ctype_digit($line[$i])) return $line[$i];
    foreach(words() as $word => $digit) {
      if (substr($line, $i, strlen($word)) === $word) return $digit;
    }
  }
  return '';
}

function findLastDigitWithWords(string $line): string {
  for($i = strlen($line) - 1; $i >= 0; $i--) {
    if (ctype_digit($line[$i])) return $line[$i];
    foreach(words() as $word => $digit) {
      if (substr($line, $i, strlen($word)) === $word) return $digit;
    }
  }
  return '';
}