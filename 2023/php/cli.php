#!/usr/bin/php
<?php

use Illuminate\Container\Container;
use Ppx17\AocRunner\Commands\DayCommand;
use Ppx17\AocRunner\Commands\NewCommand;
use Ppx17\AocRunner\Commands\RunCommand;
use Ppx17\AocRunner\DayInterface;
use Ppx17\AocRunner\DayLoader;
use Ppx17\AocRunner\DayRepository;
use Ppx17\AocRunner\Validator\ResultValidator;
use Symfony\Component\Console\Application as ConsoleApplication;
ini_set('memory_limit', '1024M');
require_once __DIR__ . '/vendor/autoload.php';

$app = new Container();

$app->singleton(DayRepository::class, function (Container $app) {
    $repository = new DayRepository();
    $loader = $app->make(DayLoader::class);
    $loader
        ->load(
            __DIR__ . '/src/Aoc/Days',
            __DIR__ . '/../input'
        )
        ->each(function (DayInterface $day) use ($repository) {
            $repository->addDay($day);
        });

    return $repository;
});

$app->singleton(ResultValidator::class, function (Container $app) {
    return new ResultValidator(__DIR__ . '/../expected');
});

/** @var ConsoleApplication $cli */
$cli = $app->build(ConsoleApplication::class);
$cli->add($app->build(RunCommand::class));
$cli->add($app->build(DayCommand::class));
$cli->add($app->build(NewCommand::class));

$cli->run();
