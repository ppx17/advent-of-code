#[macro_use]
extern crate lazy_static;

use aoc_common::input::read;


fn main() {
    println!("{}", part1());
    println!("{}", part2());
}

fn part1() -> String { solve(format(false))}

fn part2() -> String { solve(format(true)) }

fn solve(formatter: impl Fn(&str) -> String) -> String {
    read(1)
        .lines()
        .map(|line| formatter(&line))
        .map(|s| {
            let num: Result<i32, _> = s.parse();
            num.unwrap_or_else(|_| 0)
        })
        .sum::<i32>()
        .to_string()
}

fn format(check_words: bool) -> impl Fn(&str) -> String {
    move |str: &str| -> String {
        format!("{}{}", first_number(str, check_words), last_number(str, check_words))
    }
}

lazy_static! {
    static ref DIGIT_MAP: Vec<(&'static str, char)> = vec![
        ("zero", '0'),
        ("one", '1'),
        ("two", '2'),
        ("three", '3'),
        ("four", '4'),
        ("five", '5'),
        ("six", '6'),
        ("seven", '7'),
        ("eight", '8'),
        ("nine", '9'),
    ];
}

fn first_number(str: &str, check_words: bool) -> char { get_number(str, str.char_indices(), check_words) }

fn last_number(str: &str, check_words: bool) -> char { get_number(str, str.char_indices().rev(), check_words) }

fn get_number<I>(str: &str, indices: I, check_words: bool) -> char where I: Iterator<Item=(usize, char)> {
    for (i, c) in indices {
        if c.is_ascii_digit() {
            return c;
        }
        if check_words {
            for (word, digit) in DIGIT_MAP.iter() {
                if str.len() > i && str[i..].starts_with(word) {
                    return *digit;
                }
            }
        }
    }
    panic!("No number found in {}", str);
}

#[cfg(test)]
mod tests {
    use aoc_common::common::Part;
    use aoc_common::expected::answer_for_day;
    use super::*;

    #[test]
    fn test_first_number_p1() {
        assert_eq!(first_number("abc123", false), '1');
        assert_eq!(first_number("xyz789", false), '7');
        assert_eq!(first_number("no numbers here", false), ' ');
    }

    #[test]
    fn test_last_number_p1() {
        assert_eq!(last_number("abc123", false), '3');
        assert_eq!(last_number("xyz789", false), '9');
        assert_eq!(last_number("no numbers here", false), ' ');
    }

    #[test]
    fn test_first_number_p2() {
        assert_eq!(first_number("abcone23", true), '1')
    }

    #[test]
    fn test_part1() {
        assert_eq!(part1(), answer_for_day(1, Part::One));
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), answer_for_day(1, Part::Two));
    }
}