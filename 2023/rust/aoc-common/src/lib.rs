pub mod common {
    pub enum Part {
        One,
        Two,
    }
}

pub mod path {
    use std::path::PathBuf;

    pub fn input_folder() -> PathBuf {
        look_for_directory("input").unwrap_or_else(|err| panic!("{}", err))
    }

    pub fn expected_folder() -> PathBuf {
        look_for_directory("expected").unwrap_or_else(|err| panic!("{}", err))
    }

    fn look_for_directory(name: &str) -> Result<PathBuf, String> {
        // look from current dir down, maximum 3 levels to find a directory with the given name
        let mut dir = std::env::current_dir()
            .unwrap();
        for _ in 0..3 {
            if dir.join(name).exists() {
                return Ok(dir.join(name));
            }
            dir = dir.join("../");
        }

        Err(format!("Could not find directory {} as subdirectory of {} looking up to 3 levels deep", name, dir.to_str().unwrap()))
    }
}

pub mod input {
    use std::path::PathBuf;
    use crate::path::input_folder;

    pub fn read(day: u8) -> String {
        match read_from_folder(day, input_folder()) {
            Ok(data) => data,
            Err(err) => panic!("Input file for day {} could not be found: {}", day, err)
        }
    }

    fn read_from_folder(day: u8, folder: PathBuf) -> Result<String, String> {
        let binding = folder
            .join(format!("input-day{}.txt", day));
        let path = binding
            .to_str()
            .unwrap();
        match std::fs::read_to_string(path) {
            Ok(data) => Ok(data),
            Err(_) => Err(format!("Could not read input file for day {} at {}", day, path))
        }
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn test_input() {
            assert_eq!(read_from_folder(1, PathBuf::new().join("src/test-statics")), "Line 1\nLine 2\nLine 3\n");
        }
    }
}


pub mod expected {
    use crate::common::Part;
    use crate::path::expected_folder;

    pub fn answer_for_day(day: u8, part: Part) -> String {
        if let Ok(data) = std::fs::read_to_string(expected_folder().join(format!("day{}.txt", day))) {
            let lines: Vec<&str> = data.split("\n").collect();
            match part {
                Part::One => lines[0].to_string()[8..].to_string(),
                Part::Two => lines[1].to_string()[8..].to_string(),
            }
        } else {
            eprintln!("Could not read expected answers for day {}", day);
            String::from("")
        }
    }
}