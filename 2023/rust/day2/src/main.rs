use aoc_common::input::read;


fn main() {
    println!("{}", part1());
    println!("{}", part2());
}

fn part1() -> String {
    let bag_content: Hand = Hand { blue: 14, green: 13, red: 12 };
    read(2)
        .lines()
        .map(|line| parse_game(line).unwrap())
        .filter(|game| game.hands.iter().all(|hand| hand.blue <= bag_content.blue && hand.green <= bag_content.green && hand.red <= bag_content.red))
        .map(|game| game.id)
        .sum::<i32>()
        .to_string()
}

fn part2() -> String {
    read(2)
        .lines()
        .map(|line| parse_game(line).unwrap())
        .map(|game|
            game.hands
                .iter()
                .cloned()
                .reduce(|a, b| Hand { blue: a.blue.max(b.blue), green: a.green.max(b.green), red: a.red.max(b.red) })
                .unwrap_or(Hand { blue: 0, green: 0, red: 0 })
                .product()
        )
        .sum::<i32>()
        .to_string()
}

struct Game {
    id: i32,
    hands: Vec<Hand>,
}

#[derive(Debug, PartialEq, Clone)]
struct Hand {
    blue: i32,
    green: i32,
    red: i32,
}

impl Hand {
    fn product(&self) -> i32 {
        self.blue * self.green * self.red
    }
}

fn parse_game(input: &str) -> Result<Game, String> {
    let parts: Vec<&str> = input.splitn(2, ": ").collect();
    if parts.len() != 2 {
        return Err(format!("Could not parse game, missing ': ': {}", input));
    }

    let id = parts[0]
        .split_whitespace()
        .nth(1)
        .unwrap()
        .parse::<i32>()
        .unwrap();

    return Ok(Game {
        id,
        hands: parts[1]
            .split("; ")
            .map(|hand| {
                let parts: Vec<Vec<&str>> = hand.split(", ").map(|s| s.split_whitespace().collect()).collect();
                return Hand {
                    blue: get_color(&parts, "blue"),
                    green: get_color(&parts, "green"),
                    red: get_color(&parts, "red"),
                };
            })
            .collect(),
    });
}

fn get_color(parts: &Vec<Vec<&str>>, color: &str) -> i32 {
    parts
        .iter()
        .find(|s| s[1] == color)
        .unwrap_or(&vec!["0"])
        .first()
        .unwrap_or(&"0")
        .parse::<i32>()
        .unwrap_or(0)
}

#[cfg(test)]
mod tests {
    use aoc_common::common::Part;
    use aoc_common::expected::answer_for_day;
    use super::*;

    #[test]
    fn test_parse_game_id() {
        assert_eq!(parse_game("Game 1: 10 blue, 20 red").unwrap().id, 1);
    }

    #[test]
    fn test_parse_game_hand() {
        assert_eq!(parse_game("Game 1: 10 blue, 20 red").unwrap().hands[0], Hand { blue: 10, red: 20, green: 0 });
    }

    #[test]
    fn test_part1() {
        assert_eq!(part1(), answer_for_day(2, Part::One));
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), answer_for_day(2, Part::Two));
    }
}