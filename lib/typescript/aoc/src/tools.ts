import * as fs from "fs";

export class Tools {
    private static cache: Map<number, string> = new Map();
    static inputString(day: number): string {
        if(this.cache.has(day)) return this.cache.get(day);
        const path = fs.existsSync('../../input') ? `../../input/input-day${day}.txt` : `../input/input-day${day}.txt`;
        const data = fs.readFileSync(path, 'utf8');
        this.cache.set(day, data);
        return data;
    }

    static input(day: number): string[] {
        return this.inputString(day).trim().split(/\r?\n/);
    }

    static expected(day: number, part: number): null | string {
        const path = fs.existsSync('../../expected') ?`../../expected/day${day}.txt` : `../expected/day${day}.txt`;

        if( ! fs.existsSync(path)) return null;

        const lines = fs.readFileSync(path, 'utf8').split(/\r?\n/);

        return lines.find((line) => line.startsWith(`Part ${part}: `))?.split(': ', 2)[1] ?? null;
    }
}