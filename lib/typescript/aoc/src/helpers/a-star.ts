import { desc } from "./common";
import { Vec } from "./vector";

export interface NodeGenerator {
  calculateEstimatedCost(a: AStarNode, b: AStarNode);
  calculateRealCost(a: AStarNode, b: AStarNode);
  generateAdjacentNodes(node: AStarNode): AStarNode[];
}

export class PriorityQueue<T extends { id: string }> {
  private prioMap = new Map<number, T[]>();
  private idMap = new Map<string, T>();
  extractBest(): T {
    const p = Array.from(this.prioMap.keys()).sort(desc)[0];
    const item = this.prioMap.get(p).shift();
    this.idMap.delete(item.id);
    if (this.prioMap.get(p).length === 0) this.prioMap.delete(p);
    return item;
  }
  isEmpty = (): boolean => this.idMap.size === 0;
  add(item: T, prio: number) {
    if (!this.prioMap.has(prio)) {
      this.prioMap.set(prio, []);
    }
    this.prioMap.get(prio).push(item);
    this.idMap.set(item.id, item);
  }
  contains = (item: T) => this.idMap.has(item.id);
  get = (item: T): T => this.idMap.get(item.id);
  get size(): number { return this.idMap.size; }
}

export class UniqueNodeList {
  private map: Map<string, AStarNode> = new Map();
  add = (node: AStarNode) => this.map.set(node.id, node);
  has = (node: AStarNode) => this.map.has(node.id);
  remove = (node: AStarNode) => this.map.delete(node.id);
  get = (node: AStarNode) => this.map.get(node.id);
}

export class AStarNode {
  constructor(public id: string, public parent?: AStarNode) {
  }
  public G: number;
  public H: number;
  public get F(): number {
    return this.G + this.H;
  }
  public vector?: Vec;
}

export class AStar {
  public openList: PriorityQueue<AStarNode>;
  public closedList = new UniqueNodeList();
  public hasReachedGoal = (currentNode: AStarNode, goal: AStarNode): boolean => (currentNode.id === goal.id);

  constructor(private nodeProvider: NodeGenerator, private visit?: (node: AStarNode) => void) {
    this.openList = new PriorityQueue<AStarNode>();
    this.closedList = new UniqueNodeList();
  }

  public run(start: AStarNode, goal: AStarNode, maxG: number = Infinity): AStarNode[] {
    start.G = 0;
    start.H = this.nodeProvider.calculateEstimatedCost(start, goal);

    this.openList.add(start, -start.F);

    while (!this.openList.isEmpty()) {
      const currentNode = this.openList.extractBest();
      this.closedList.add(currentNode);
      if (this.visit !== undefined) this.visit(currentNode);

      if (this.hasReachedGoal(currentNode, goal)) {
        return this.generatePathFromStartNodeTo(currentNode);
      }

      const successors = this.computeAdjacentNodes(currentNode, goal);

      for (const successor of successors) {
        if(successor.G > maxG) continue;

        if (this.openList.contains(successor)) {
          const successorInOpenList = this.openList.get(successor);

          if (successor.G >= successorInOpenList.G) continue;
        }

        if (this.closedList.has(successor)) {
          const successorInClosedList = this.closedList.get(successor);
          if (successor.G >= successorInClosedList.G) continue;
        }

        successor.parent = currentNode;
        this.closedList.remove(successor);
        this.openList.add(successor, -successor.F);
      }
    }

    return [];
  }

  private generatePathFromStartNodeTo(node: AStarNode): AStarNode[] {
    const path = [];

    for (let currentNode = node; currentNode !== undefined; currentNode = currentNode.parent)
      path.unshift(currentNode);

    return path;
  }

  private computeAdjacentNodes(node: AStarNode, goal: AStarNode): AStarNode[] {
    const nodes = this.nodeProvider.generateAdjacentNodes(node);

    for (const adjacentNode of nodes) {
      adjacentNode.G = (node.G + this.nodeProvider.calculateRealCost(node, adjacentNode));
      adjacentNode.H = (this.nodeProvider.calculateEstimatedCost(adjacentNode, goal));
    }

    return nodes;
  }
}