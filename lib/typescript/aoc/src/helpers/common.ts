export function sum(a: number, b: number): number;
export function sum(a: bigint, b: bigint): bigint;
export function sum(a: string, b: string): string;
export function sum(a: any, b: any): any {
  return a + b;
}

export function product(a: number, b: number): number {
  return a * b;
}

export const desc = (x,y) => y - x;
export const asc = (x,y) => x - y;

export const range = function*(from: number, to: number, step = 1) {
  for(let curr = from; curr <= to; curr += step) yield curr;
}

export function reduce<T, Acc>(it: IterableIterator<T>, fn: (acc: Acc, curr: T) => Acc, initial: Acc = undefined) {
  for(const el of it) initial = fn(initial, el);
  return initial;
}

export const logObject = (obj: any) => console.log(JSON.stringify(obj, null, 2));