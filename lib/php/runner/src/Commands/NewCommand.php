<?php


namespace Ppx17\AocRunner\Commands;


use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NewCommand extends AocCommand
{
  private OutputInterface $output;

  protected function configure()
  {
    $this->setName('new')
      ->addArgument('day', InputArgument::REQUIRED)
      ->setDescription('Creates file for a day');
  }

  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    $trace = collect(debug_backtrace())->last()['file'];
    $year = preg_match('/(\d{4})/', $trace, $matches) ? $matches[1] : date('Y');

    $this->output = $output;
    $dayNumber = $input->getArgument('day');
    $this->assertPath("../input/input-day{$dayNumber}.txt", '');
    $this->assertPath("../expected/day{$dayNumber}.txt", "Part 1: \nPart 2: \n");
    $this->assertPath("./src/Aoc/Days/Day{$dayNumber}.php", $this->template($dayNumber, $year));

    return 0;
  }

  protected function assertPath(string $path, string $content): void
  {
    if (!file_exists($path)) {
      $this->output->writeln('Creating ' . basename($path));
      file_put_contents($path, $content);
      chown($path, 1000);
      chgrp($path, 1000);
      exec('git add "{$path}"');
    } else {
      $this->output->writeln('Already found ' . basename($path));
    }
  }

  private function template(int $day, int $year)
  {
    return "<?php


namespace Ppx17\\Aoc{$year}\\Aoc\\Days;


class Day{$day} extends AbstractDay
{
    public function dayNumber(): int
    {
        return {$day};
    }

    public function part1(): string
    {
        return '';
    }

    public function part2(): string
    {
        return '';
    }
}
";
  }
}