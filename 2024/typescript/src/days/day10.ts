import { Day, sum, Vec, Vector } from "../aoc";

export class Day10 extends Day {
  day = (): number => 10;
  part1 = () => scoreTrailsBySummit(parseMap(this.input))
  part2 = () => scoreTrailsByUniqueRoutes(parseMap(this.input))
}

const parseMap = (input: string[]): number[][] =>
  input.map(l => l.split('').map(Number));

const scoreTrailsBySummit = (map: number[][]): number => {
  const directions = [Vector.north(), Vector.east(), Vector.south(), Vector.west()];
  const mapSize = new Vector(map[0].length - 1, map.length - 1);


  const summitsReachableFrom = (pos: Vec): string[] =>
    directions
      .map(dir => pos.add(dir))
      .filter(next => next.within(mapSize))
      .filter(next => map[next.y][next.x] === map[pos.y][pos.x] + 1)
      .flatMap(next => map[next.y][next.x] === 9 ? [next.serialize()] : summitsReachableFrom(next));

  let score = 0;
  for(let y = 0; y < map.length; y++) {
    for(let x = 0; x < map[y].length; x++) {
      if(map[y][x] === 0) {
        const summits = new Set<string>(summitsReachableFrom(new Vector(x, y)));
        score += summits.size;
      }
    }
  }

  return score;
}

const scoreTrailsByUniqueRoutes = (map: number[][]): number => {
  const directions = [Vector.north(), Vector.east(), Vector.south(), Vector.west()];
  const mapSize = new Vector(map[0].length - 1, map.length - 1);

  const summitsReachableFrom = (pos: Vec): number =>
    directions
      .map(dir => pos.add(dir))
      .filter(next => next.within(mapSize))
      .filter(next => map[next.y][next.x] === map[pos.y][pos.x] + 1)
      .map(next => map[next.y][next.x] === 9 ? 1 : summitsReachableFrom(next))
      .reduce(sum, 0);

  let score = 0;
  for(let y = 0; y < map.length; y++) {
    for(let x = 0; x < map[y].length; x++) {
      if(map[y][x] === 0) {
        score += summitsReachableFrom(new Vector(x, y));
      }
    }
  }

  return score;
}