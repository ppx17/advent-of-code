import { Day } from "../aoc";

export class Day17 extends Day {
  day = (): number => 17;
  part1 = () => {
    const c = new Computer();

    const { program, A, B, C } = parseProgram(this.rawInput());

    const output = c.executeProgram(program, A, B, C);

    return output.join(',')
  }
  part2 = () => {
    const c = new Computer();

    const { program } = parseProgram(this.rawInput());

    // program 2,4,1,7,7,5,0,3,4,0,1,7,5,5,3,0


    const attempt = (a: number) => {
      console.log(a);
      try {
        const output = c.executeProgram(program, a, 0, 0);
        console.log(`${a.toString(2)}: ${output.join(',')}`);
        return output[0];
      }catch(e) {
        console.log(`${a}: ${e.message}`);
        return -1;
      }
    }

    let fullA = 0b0
    for(const lookingFor of [...program].reverse()) {
      console.log(`Looking for ${lookingFor}`);
      const inputs = [];
      for(let i = 0b000; i <= 0b111; i++) {
        const shiftedI = i << 3;
        if(attempt(fullA + shiftedI) === lookingFor) {
          console.log(`Found ${lookingFor} with ${shiftedI.toString(2)}, fullA going from ${fullA.toString(2)} to ${((fullA << 3) + i).toString(2)}`);
          inputs.push(i);
          fullA <<= 3;
          fullA += shiftedI;
          break;
        }
        if(i === 0b111) {
          console.log('No valid input found');
          return '';
        }
      }
    }

    // for(const outcome of program.reverse()) {
    //   fullA <<= 3;
    //   console.log(`FullA: ${fullA.toString(2)}`);
    //   console.log(`Looking for ${outcome}`);
    //   for (let candidate = 0b000; candidate <= 0b111; candidate++) {
    //     try {
    //       const shifted = candidate << 3;
    //       console.log(`A: ${shifted.toString(2)}`);
    //       const output = c.executeProgram(program, shifted, 0, 0);
    //       console.log(`Candidate ${candidate.toString(2)} produced ${output.join(',')}`);
    //       if (output[0] === outcome) {
    //         fullA += candidate;
    //         break;
    //       }
    //     }catch(e) {
    //       console.log(e.message);
    //       // invalid program generated
    //     }
    //   }
    // }
    // const outputb = c.executeProgram(program, a, 0, 0);

    // return output.join(',');
    return '';
  }
}

class Computer {
  public A: number;
  public B: number;
  public C: number;
  public ptr: number;
  public isRunning: boolean;
  public program: number[];
  public output: number[];

  public executeProgram(program: number[], A = 0, B = 0, C = 0): number[] {
    this.program = program;
    this.isRunning = true;
    this.ptr = 0;
    this.output = [];
    this.A = A;
    this.B = B;
    this.C = C;

    while(this.ptrIsValid()) this.executeInstruction();

    return this.output;
  }

  private comboOperand(ptr: number) {
    const v = this.program[ptr];
    if(v >= 0 && v <= 3) return v;
    if(v === 4) return this.A;
    if(v === 5) return this.B;
    if(v === 6) return this.C;
    throw new Error(`Invalid operand ${v}`);
  }

  private literalOperand(ptr: number) {
    return this.program[ptr];
  }

  private ptrIsValid() {
    return this.ptr >= 0 && this.ptr < this.program.length;
  }

  private writeA = (value: number) => this.A = Math.floor(value);
  private writeB = (value: number) => this.B = Math.floor(value);
  private writeC = (value: number) => this.C = Math.floor(value);

  private executeInstruction() {
    const opcodes = {
      0: (ptr: number) => {
        // adv Division to A reg
        this.writeA(this.A / Math.pow(2, this.comboOperand(ptr + 1)));
        return ptr + 2;
      },
      1: (ptr: number) => {
        // bxl Bitwise XOR
        this.writeB(this.B ^ this.literalOperand(ptr + 1));
        return ptr + 2;
      },
      2: (ptr: number) => {
        // bst Combo mod 8
        this.writeB(this.comboOperand(ptr + 1) % 8);
        return ptr + 2;
      },
      3: (ptr: number) => {
        // jnz JumpNonZero
        if(this.A === 0) return ptr + 2;
        return this.literalOperand(ptr + 1);
      },
      4: (ptr: number) => {
        // bxc Bitwise XOR
        this.writeB(this.B ^ this.C);
        return ptr + 2;
      },
      5: (ptr: number) => {
        // out Output
        this.output.push(this.comboOperand(ptr + 1) % 8);
        return ptr + 2;
      },
      6: (ptr: number) => {
        // bdv Division to B reg
        this.writeB(this.A / Math.pow(2, this.comboOperand(ptr + 1)));
        return ptr + 2;
      },
      7: (ptr: number) => {
        // cdv Division to C reg
        this.writeC(this.A / Math.pow(2, this.comboOperand(ptr + 1)));
        return ptr + 2;
      }
    }

    const op = this.program[this.ptr];
    if(op === null) throw new Error(`Invalid opcode at ${this.ptr}: ${op}`);

    this.ptr = opcodes[op](this.ptr);
  }
}

const parseProgram = (input: string):{ program: number[], A: number, B: number, C: number } => {
  const [registers, code] = input.split('\n\n');
  const regs = registers.match(/Register A: (?<A>\d+)\nRegister B: (?<B>\d+)\nRegister C: (?<C>\d+)/)?.groups;
  const program = code.split(':')[1].split(',').map(Number);

  return {
    program,
    A: Number(regs['A']),
    B: Number(regs['B']),
    C: Number(regs['C'])
  }
}