import { Day } from "../aoc";

export class Day2 extends Day {
  day = (): number => 2;
  part1 = () =>
    this.reports().filter(isSafe).length;
  part2 = () =>
    this.reports().filter(isSafeDampened).length;

  reports = () => this.input.map(l => l.split(/\s+/).map(Number));
}


export const isSafe = (report: number[]) => {
  const sign = Math.sign(report[1] - report[0]);
  for (let i = 0; i < report.length - 1; i++) {
    const d = report[i + 1] - report[i];
    if (Math.abs(d) > 3 || Math.abs(d) < 1 || Math.sign(d) !== sign) {
      return false;
    }
  }

  return true;
}

export const isSafeDampened = (report: number[]) => {
  if(isSafe(report)) return true;

  for(let i = 0; i < report.length; i++) {
    const removed = removeIndex(report, i);
    if(isSafe(removed)) return true;
  }

  return false;
}

export const removeIndex = (report: number[], index: number) =>
  [...report.slice(0, index), ...report.slice(index + 1)];
