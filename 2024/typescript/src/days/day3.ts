import { Day, sum } from "../aoc";

export class Day3 extends Day {
  day = (): number => 3;
  part1 = () => {
    return [...this.rawInput()
      .matchAll(/mul\((\d{1,3}),(\d{1,3})\)/g)]
      .map(m => Number(m[1]) * Number(m[2]))
      .reduce(sum);
  }
  part2 = () => {
    const matches = this.rawInput()
      .matchAll(/mul\((\d{1,3}),(\d{1,3})\)|do\(\)|don't\(\)/g);

    let enabled = true;
    let sum = 0;
    for(const m of matches) {
      if (m[0] === 'do()') enabled = true;
      else if (m[0] === 'don\'t()') enabled = false;
      else if (enabled) sum += Number(m[1]) * Number(m[2]);
    }
    return sum;
  }
}
