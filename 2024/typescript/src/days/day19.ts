import { Day, sum } from "../aoc";

export class Day19 extends Day {
  day = (): number => 19;
  part1 = () => {
    const { towels, patterns } = parseTowels(this.input);
    return patterns.filter(pattern => canMakePatternMemo(pattern, towels)).length;
  }
  part2 = () => {
    const { towels, patterns } = parseTowels(this.input);
    return patterns.map(pattern => combinationsAvailableMemo(pattern, towels)).reduce(sum);
  }
}

type Towel = string;
type Pattern = string;

const parseTowels = (input: string[]): { towels: Towel[], patterns: Pattern[] } => {
  const [towels, _blank, ...patterns] = input;

  return {
    towels: towels.split(', '),
    patterns: patterns,
  };
}

const memo = new Map<string, boolean>();
const canMakePatternMemo = (pattern: Pattern, towels: Towel[]): boolean => {
  if (memo.has(pattern)) return memo.get(pattern);

  const canMake = canMakePattern(pattern, towels);
  memo.set(pattern, canMake);
  return canMake;
}

const canMakePattern = (pattern: Pattern, towels: Towel[]): boolean =>
  pattern === ''
  || towels.some(towel => pattern.startsWith(towel) && canMakePatternMemo(pattern.slice(towel.length), towels));

const combiMemo = new Map<string, number>();
const combinationsAvailableMemo = (pattern: Pattern, towels: Towel[]): number => {
  if (combiMemo.has(pattern)) return combiMemo.get(pattern);

  const combinations = combinationsAvailable(pattern, towels);
  combiMemo.set(pattern, combinations);
  return combinations;
}

const combinationsAvailable = (pattern: Pattern, towels: Towel[]): number => {
  if (pattern === '') return 1;

  return towels
    .filter(towel => pattern.startsWith(towel))
    .map(towel => combinationsAvailableMemo(pattern.slice(towel.length), towels))
    .reduce(sum, 0);
}