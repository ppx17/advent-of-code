import { Day, sum, Vec, Vector } from "../aoc";

export class Day15 extends Day {
  day = (): number => 15;
  part1 = () => {
    const { movements, environment } = parseInput(this.rawInput());

    const resultingEnvironment = movements.reduce((env, movement) => moveRobot(env, movement), environment);

    return sumOfBoxCoordinates(resultingEnvironment);
  }
  part2 = () => {
    return '';
  }
}

type Environment = {
  map: string[][],
  robot: Vec,
}

const parseInput = (input: string): {environment: Environment, movements: Vec[] } => {
  const [mapInput, movementInput] = input.split('\n\n');
  const dirs = {
    '^': Vector.north(),
    'v': Vector.south(),
    '<': Vector.west(),
    '>': Vector.east(),
  }
  let robot: Vec;
  const map = mapInput
    .split('\n')
    .map(((l, y) => l.split('').map((c, x) => {
      if (c === '@') {
        robot = new Vector(x, y);
      }
      return c;
    })))
  const movements = movementInput.split('').filter(c => c in dirs).map(c => dirs[c]);

  return { environment: { map, robot }, movements };
}

const readMap = (env: Environment, p: Vec): string => {
  return env.map[p.y][p.x];
}
const setMap = (env: Environment, p: Vec, value: string): Environment => {
  env.map[p.y][p.x] = value;
  return env;
}

const moveRobot = (env: Environment, movement: Vec): Environment => {
  const targetPosition = env.robot.add(movement);

  const nextSquare = readMap(env, targetPosition);
  if(nextSquare === '#') {
    return env;
  }

  if(nextSquare === '.') {
    env = setMap(env, env.robot, '.');
    env = setMap(env, targetPosition, '@');
    env.robot = targetPosition;
    return env;
  }

  const boxes: Vec[] = [targetPosition];
  let nextBox = targetPosition;
  let canPush: boolean | null = null;
  while(true) {
    nextBox = nextBox.add(movement);
    const nextSquare = readMap(env, nextBox);
    if(nextSquare === '#') {
      canPush = false;
      break;
    }
    if(nextSquare === '.') {
      boxes.push(nextBox);
      canPush = true;
      break;
    }
    boxes.push(nextBox);
  }

  if(!canPush) {
    return env;
  }

  env = setMap(env, targetPosition, '@');
  env = setMap(env, boxes[boxes.length - 1], 'O');
  env = setMap(env, env.robot, '.');
  env.robot = targetPosition;

  return env;
}

const renderMap = (environment: Environment): string =>
  environment.map.map(l => l.join('')).join('\n');

const renderMovement = (movement: Vec): string => {
  if(movement.is(Vector.north())) return '^';
  if(movement.is(Vector.south())) return 'v';
  if(movement.is(Vector.east())) return '>';
  if(movement.is(Vector.west())) return '<';
  return ' ';
}

const sumOfBoxCoordinates = (env: Environment): number =>
  env.map.flatMap((line, y) => line.map((c, x): number => c === 'O' ? y * 100 + x : 0)).reduce(sum)