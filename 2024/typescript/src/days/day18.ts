import { AStar, AStarNode, Day, NodeGenerator, Vec, Vector } from "../aoc";

export class Day18 extends Day {
  day = (): number => 18;

  mapEdgeLength = 70;
  p1Bytes = 1024;


  part1 = () => this.routeLength(this.route(this.p1Bytes))
  part2 = () => {
    // let's start by taking the route we know is valid now
    let route = this.route(this.p1Bytes);
    let nodesUsed = new Set<string>(route.map(n => n.id));

    for(let i = this.p1Bytes+1; i < this.input.length; i++) {
      // As long as the next hurdle is not part of our route, it's still valid
      if(!nodesUsed.has(this.incomingBytes()[i].serialize())) continue;

      // Calculate new route around the hurdle
      route = this.route(i);

      // If no route is found, return the byte that blocked us
      if(route.length === 0) return this.input[i - 1];

      // Otherwise, update the nodes used for the new route
      nodesUsed = new Set<string>(route.map(n => n.id));
    }

    return 'invalid';
  }

  private route = (numBytes: number): AStarNode[] => {
    const start = new Vector(0, 0);
    const mapSize = new Vector(this.mapEdgeLength, this.mapEdgeLength);

    const aStar = new AStar(new MemoryNodeGenerator(mapSize, this.incomingBytes().slice(0, numBytes)));
    return aStar.run(nodeFromVector(start), nodeFromVector(mapSize));
  }

  private routeLength = (route: AStarNode[]): number | undefined => route[route.length - 1]?.G;

  private incomingBytesCache: Vector[];
  private incomingBytes = () => this.incomingBytesCache ??= this.input.map(l => l.split(',').map(Number)).map(([x, y]) => new Vector(x, y));
}

class MemoryNodeGenerator implements NodeGenerator {
  private fallenBytes = new Set<string>();
  constructor(private mapSize: Vec, incomingBytes: Vec[]) {
    this.fallenBytes = new Set(incomingBytes.map(v => v.serialize()));
  }

  calculateEstimatedCost(a: AStarNode, b: AStarNode) {
    return Vector.deserialize(a.id).manhattan(Vector.deserialize(b.id));
  }
  calculateRealCost(a: AStarNode, b: AStarNode) {
    return this.calculateEstimatedCost(a, b);
  }
  generateAdjacentNodes(node: AStarNode): AStarNode[] {
    const current = node.vector;
    return [
      Vector.north(),
      Vector.south(),
      Vector.east(),
      Vector.west()
    ]
      .map(dir => current.add(dir))
      .filter(v => v.within(this.mapSize))
      .filter(v => !this.fallenBytes.has(v.serialize()))
      .map(nodeFromVector);
  }
}

const nodeFromVector = (vector: Vec): AStarNode => {
  const n = new AStarNode(vector.serialize());
  n.vector = vector;
  return n;
}