import { Day, sum } from "../aoc";

export class Day13 extends Day {
  day = (): number => 13;
  part1 = () => this.rawInput()
    .split(/\n\n/)
    .map(parseMachine)
    .map(solveMachineNaive(100))
    .reduce(sum, 0);
  part2 = () => this.rawInput()
    .split(/\n\n/)
    .map(parseMachine)
    .map(fixUnitConversionError(10_000_000_000_000))
    .map(solveMachineSmart)
    .reduce(sum, 0);
}

const parseMachine = (input: string): Machine => {
  const regex = /Button A: X\+(?<ax>\d+), Y\+(?<ay>\d+)\s+Button B: X\+(?<bx>\d+), Y\+(?<by>\d+)\s+Prize: X=(?<px>\d+), Y=(?<py>\d+)/
  const { groups: g } = regex.exec(input);
  return {
    A: { X: Number(g.ax), Y: Number(g.ay) },
    B: { X: Number(g.bx), Y: Number(g.by) },
    Prize: { X: Number(g.px), Y: Number(g.py) }
  };
}

const fixUnitConversionError = (error: number) => ({ Prize, ...rest }: Machine): Machine => ({
  ...rest,
  Prize: {
    X: Prize.X + error,
    Y: Prize.Y + error,
  }
})

const solveMachineNaive = (maxPresses: number) =>({ A, B, Prize }: Machine): number => {
  const maxAPresses = Math.min(Math.floor(Prize.X / A.X), Math.floor(Prize.Y / A.Y), maxPresses);

  for (let aPresses = maxAPresses; aPresses >= 0; aPresses--) {
    const remainingX = Prize.X - aPresses * A.X;
    const remainingY = Prize.Y - aPresses * A.Y;

    const bPresses = Math.min(Math.floor(remainingX / B.X), maxPresses);
    if (remainingX - (bPresses * B.X) !== 0) continue;
    if (remainingY - (bPresses * B.Y) !== 0) continue;

    return aPresses * 3 + bPresses;
  }

  return 0;
}

const solveMachineSmart = ({A, B, Prize}: Machine): number => {
  const aPresses = (Prize.X * B.Y - Prize.Y * B.X) / (A.X * B.Y - A.Y * B.X);
  const bPresses = (A.X * Prize.Y - A.Y * Prize.X) / (A.X * B.Y - A.Y * B.X);

  return isWhole(aPresses) && isWhole(bPresses) ? aPresses * 3 + bPresses : 0;
}

const isWhole = (n: number) => n === Math.round(n);

type XY = { X: number, Y: number };
type Machine = {
  A: XY;
  B: XY;
  Prize: XY;
}