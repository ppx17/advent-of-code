import { Day, sum, Vector } from "../aoc";

export class Day12 extends Day {
  day = (): number => 12;
  part1 = () => {
    const map = this.parseMap();
    return getPlots(map).map(plot => borderLength(plot, map) * plot.length).reduce(sum);
  }
  part2 = () => {
    const map = this.parseMap();
    return getPlots(map).map(plot => {

      const s = sides(plot, map);
      // console.log(readMap(map, plot[0]), s);

      return s * plot.length;
    }).reduce(sum);
  }

  private parseMap = () => this.input.map(l => l.split(''));
}

const directions = [Vector.north(), Vector.east(), Vector.south(), Vector.west()];
// const directions = [Vector.north()];

const getMapSize = (map: string[][]) => new Vector(map[0].length - 1, map.length - 1);
const readMap = (map: string[][], pos: Vector): string | undefined => map[pos.y]?.[pos.x];

const getPlots = (map: string[][])=> {
  const visited = new Set<string>();
  const mapSize = getMapSize(map);
  const plots: Vector[][] = [];
  for (let y = 0; y < map.length; y++) {
    for (let x = 0; x < map[y].length; x++) {
      const p = new Vector(x, y);
      if (visited.has(p.serialize())) continue;
      const c = readMap(map, p);
      const queue = [p];
      const plot: Vector[] = [];
      while (queue.length) {
        const current = queue.shift();
        if (readMap(map, current) !== c) continue;
        if (visited.has(current.serialize())) continue;
        visited.add(current.serialize());
        plot.push(current);
        directions
          .map(d => current.add(d))
          .filter(p => p.within(mapSize))
          .filter(p => !visited.has(p.serialize()))
          .forEach(n => queue.push(n));
      }
      plots.push(plot);
    }
  }
  return plots;
};


const borderLength = (plot: Vector[], map: string[][]): number => {
  const plotCrop = readMap(map, plot[0]);
  return plot.map(plot => directions.map(d => plot.add(d)).filter(p => readMap(map, p) !== plotCrop).length).reduce(sum);
}

const sides = (plot: Vector[], map: string[][]): number => {
  const plotCrop = readMap(map, plot[0]);
  let sides = 0;
  const considered = new Set<string>();

  const serialize = ([dir, pos]: [Vector, Vector]) => `${dir.serialize()} - ${pos.serialize()}`;
  const isPlotCrop = (p: Vector) => readMap(map, p) === plotCrop;

  for(const square of plot) {
    const neighbours = directions.map(d => [d, square.add(d)]);
    const borderSquares = neighbours.filter(([_, p]) => !isPlotCrop(p));

    for(const [dir, _] of borderSquares) {
      if(considered.has(serialize([dir, square]))) continue;
      // Starting a new side
      sides++;

      // Now figure out how many borders we can tick off with this side
      considered.add(serialize([dir, square]));

      // Check if we can go left or right from this position
      for(const move of [dir.rotateLeft(), dir.rotateRight()]) {
        let neighbor = square.add(move);
        while(isPlotCrop(neighbor) && !isPlotCrop(neighbor.add(dir))) {
          considered.add(serialize([dir, neighbor]));
          neighbor = neighbor.add(move);
        }
      }
    }
  }

  return sides;
}