import { Day, sum } from "../aoc";

export class Day1 extends Day {
  day = (): number => 1;
  part1 = () => {
    let [listA, listB] = this.getLists();

    let totalDistance = 0;

    while(listA.length > 0) {
      const iA = indexOfLowestNumber(listA);
      const iB = indexOfLowestNumber(listB);
      totalDistance += distance(listA[iA], listB[iB]);
      listA = removeIndexFromList(listA, iA);
      listB = removeIndexFromList(listB, iB);
    }

    return totalDistance;
  }
  part2 = () => {
    const [listA, listB] = this.getLists();
    return listA.map((id) => id * countId(listB, id)).reduce(sum);
  }

  private getLists = () => rotateMatrix(this.input.map(l => l.split(/\s+/).map(Number)));
}


const rotateMatrix = (matrix: number[][]) =>
  matrix[0].map((_, i) => matrix.map(row => row[i]));

const indexOfLowestNumber = (list: number[]) => list.indexOf(Math.min(...list));

const removeIndexFromList = (list: number[], index: number) => list.slice(0, index).concat(list.slice(index + 1));

const distance = (a: number, b: number) => Math.abs(a - b);

const countId = (list: number[], id: number) =>
  list.reduce((count, currentId) => count + (currentId === id ? 1 : 0), 0);