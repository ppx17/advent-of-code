import { AStar, AStarNode, Day, NodeGenerator, Vec, Vector } from "../aoc";

export class Day20 extends Day {
  day = (): number => 20;
  part1 = () => {

    const cheatCutOff = 100;

    const { map, start, finish } = parseMap(this.input);

    const regularRoute = this.findBestRouteLength(map, start, finish);
    const maxG = regularRoute - (cheatCutOff - 1);

    // This is a naive approach for now, it considers each wall once, whilst the text describes that each direction
    // a wall gets traversed is a different cheat.

    // Still gave the right answer though, only problem is that it takes 442 seconds to find 1323 cheats...
    let cheats = 0;
    for(let y = 1; y < map.length - 1; y++) {
      console.log(`Checking row ${y}`);
      for(let x = 1; x < map[0].length - 1; x++) {
        const v = new Vector(x, y);
        if(getFromMap(map, v) === '.') continue;

        const canMoveHorizontal = [Vector.east(), Vector.west()].every(d => getFromMap(map, v.add(d)) === '.');
        const canMoveVertical = [Vector.south(), Vector.north()].every(d => getFromMap(map, v.add(d)) === '.');

        // No need to find a route if you can't move through this wall anyway
        if(!canMoveHorizontal && !canMoveVertical) continue;

        const mapCopy = map.map(l => l.slice());
        mapCopy[y][x] = '.';

        const l = this.findBestRouteLength(mapCopy, start, finish, maxG);
        if(l < maxG) {
          console.log(`Found cheat that saves ${regularRoute - l} picoseconds.`);
          cheats++;
        }
      }
    }

    return cheats;
  }
  part2 = () => {
    return '';
  }

  private findBestRouteLength = (map: Map, start: Vec, finish: Vec, maxG: number = Infinity): number => {
    const aStar = new AStar(new MapNodeGenerator(map));

    const startNode = nodeFromVector(start);
    const finishNode = nodeFromVector(finish);

    const path = aStar.run(startNode, finishNode, maxG);

    return path[path.length - 1]?.G ?? Infinity;
  }
}

// Hello day 18!
class MapNodeGenerator implements NodeGenerator {
  constructor(private map: Map) {
  }

  calculateEstimatedCost(a: AStarNode, b: AStarNode) {
    return Vector.deserialize(a.id).manhattan(Vector.deserialize(b.id));
  }
  calculateRealCost(a: AStarNode, b: AStarNode) {
    return this.calculateEstimatedCost(a, b);
  }
  generateAdjacentNodes(node: AStarNode): AStarNode[] {
    const current = node.vector;
    return [
      Vector.north(),
      Vector.south(),
      Vector.east(),
      Vector.west()
    ]
      .map(dir => current.add(dir))
      .filter(v => this.canMoveTo(v))
      .map(nodeFromVector);
  }

  private canMoveTo = (p: Vec): boolean => getFromMap(this.map, p) === '.';
}

const getFromMap = (map: Map, p: Vec): '.' | '#' | undefined => map[p.y]?.[p.x]

const nodeFromVector = (vector: Vec): AStarNode => {
  const n = new AStarNode(vector.serialize());
  n.vector = vector;
  return n;
}


// Hello day 16!
type Map = ('.' | '#')[][];

const parseMap = (input: string[]): { map: Map, start: Vec, finish: Vec } => {
  let start: Vec;
  let finish: Vec;
  const m = input.map((l, y) => l.split('').map((c, x) => {
    if (c === 'S') {
      start = new Vector(x, y);
      return '.';
    }
    if (c === 'E') {
      finish = new Vector(x, y);
      return '.';
    }
    return c;
  }));

  return { map: m as Map, start, finish };
}