import { isSafe, isSafeDampened, removeIndex } from "./day2";

describe(`day2`, () => {
  describe(`isSafe`, () => {
    it('fails when the numbers all increment, but one difference is lower than 1', () => {
      expect(isSafe([1, 2, 2, 3])).toBe(false);
    });

    it('fails when the numbers all increment, but one difference is higher than three', () => {
      expect(isSafe([1, 2, 6, 7])).toBe(false);
    });

    it('fails when the numbers all decrement, but one difference is lower than 1', () => {
      expect(isSafe([3, 2, 2, 1])).toBe(false);
    });

    it('fails when the numbers all decrement, but one difference is higher than three', () => {
      expect(isSafe([7, 6, 2, 1])).toBe(false);
    });

    it('fails when not all numbers increment or decrement', () => {
      expect(isSafe([1, 2, 4, 3])).toBe(false);
    });

    it('succeeds when all numbers increment by one', () => {
      expect(isSafe([1, 2, 3])).toBe(true);
    });

    it('succeeds when all numbers decrement by one', () => {
      expect(isSafe([3, 2, 1])).toBe(true);
    });

    it('succeeds when all numbers increment by three', () => {
      expect(isSafe([1, 4, 7])).toBe(true);
    });

    it('succeeds when all numbers decrement by three', () => {
      expect(isSafe([7, 4, 1])).toBe(true);
    });
  });

  describe(`removeIndex`, () => {
    it('removes the index', () => {
      expect(removeIndex([1, 2, 3], 1)).toEqual([1, 3]);
    });

    it('can remove the first index', () => {
      expect(removeIndex([1, 2, 3], 0)).toEqual([2, 3]);
    });

    it('can remove the last index', () => {
      expect(removeIndex([1, 2, 3], 2)).toEqual([1, 2]);
    });
  });

  describe(`slice`, () => {
    it('slices the array', () => {
      const i = 1;
      expect([1, 2, 3, 4, 5].slice(0, i)).toEqual([1]);
      expect([1, 2, 3, 4, 5].slice(i + 1)).toEqual([3, 4, 5]);
    });
  });

  describe('isSafeDampened', () => {
    it.each([
      [[7, 6, 4, 2, 1], true],
      [[1, 2, 7, 8, 9], false],
      [[9, 7, 6, 2, 1], false],
      [[1, 3, 2, 4, 5], true],
      [[8, 6, 4, 4, 1], true],
      [[1, 3, 6, 7, 9], true],
    ])('should remain unsafe', (report, result) => {
      expect(isSafeDampened(report)).toBe(result);
    })
  })
});
