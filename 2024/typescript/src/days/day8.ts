import { Day, Vec, Vector } from "../aoc";

export class Day8 extends Day {
  day = (): number => 8;


  part1 = () => this.solve((antennaA, antennaB) => {
    const distance = antennaB.sub(antennaA);
    return [antennaA.sub(distance), antennaB.add(distance)];
  });

  part2 = () => this.solve((antennaA, antennaB, mapSize: Vec) => {
    const distance = antennaB.sub(antennaA);
    const antiNodes = [];
    let antiNodeA = antennaA;
    while (antiNodeA.within(mapSize)) {
      antiNodes.push(antiNodeA);
      antiNodeA = antiNodeA.sub(distance);
    }
    let antiNodeB = antennaB;
    while (antiNodeB.within(mapSize)) {
      antiNodes.push(antiNodeB);
      antiNodeB = antiNodeB.add(distance);
    }

    return antiNodes;
  });

  private solve = (generateAntiNodes: (antennaA: Vec, antennaB: Vec, mapSize: Vec) => Vec[]) => {
    const map = this.parseMap();
    const mapSize = new Vector(map.length - 1, map[0].length - 1);

    const antennae = findAntennae(map);
    const antiNodes = new Set<string>();

    for (const antennaeForFrequency of Object.values(antennae)) {
      const antennaePairs = uniquePairs(antennaeForFrequency);
      for (const [a, b] of antennaePairs) {
        generateAntiNodes(a, b, mapSize)
          .forEach(antiNode => {
            if (antiNode.within(mapSize)) antiNodes.add(antiNode.serialize());
          });
      }
    }

    return antiNodes.size
  }

  parseMap = () => this.input.map(l => l.split(''));
}

const findAntennae = (map: string[][]): Record<string, Vec[]> => {
  const antennae = {};
  for (let y = 0; y < map.length; y++) {
    for (let x = 0; x < map[y].length; x++) {
      if (map[y][x].match(/[A-Za-z0-9]/)) {
        antennae[map[y][x]] ??= [];
        antennae[map[y][x]].push(new Vector(y, x));
      }
    }
  }
  return antennae;
};

const uniquePairs = <T>(arr: T[]): [T, T][] => {
  const pairs = [];

  arr.forEach((a, ai) => {
    arr.forEach((b, bi) => {
      if (ai >= bi) return;
      pairs.push([a, b]);
    });
  })

  return pairs;
}