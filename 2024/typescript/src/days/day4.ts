import { Day, Vec, Vector } from "../aoc";

export class Day4 extends Day {
  private grid: string[][];
  day = (): number => 4;
  part1 = () => {
    this.grid = this.parseGrid();
    const validDirections = [
      Vector.north(), Vector.east(), Vector.south(), Vector.west(), Vector.NE(), Vector.NW(), Vector.SE(), Vector.SW()];

    let count = 0;
    this.iterateGrid((pos) => {
      if (this.readGrid(pos) !== 'X') return;

      for (const dir of validDirections) {
        if (this.readsXmas(pos, dir)) count++
      }
    });
    return count;
  }
  part2 = () => {
    this.grid = this.parseGrid();

    let count = 0;

    this.iterateGrid((pos) => {
      count += Number(
        this.readGrid(pos) === 'A'
        && this.containsMS([
          this.readGrid(pos.add(Vector.NE())),
          this.readGrid(pos.add(Vector.SW())),
        ])
        && this.containsMS([
          this.readGrid(pos.add(Vector.NW())),
          this.readGrid(pos.add(Vector.SE())),
        ]));
    });
    return count;
  }

  private containsMS = (letters: string[]) => letters.includes('M') && letters.includes('S');

  private readGrid = (v: Vector): string | undefined => this.grid[v.y]?.[v.x];

  private readsXmas = (pos: Vector, dir: Vector) =>
    ['X', 'M', 'A', 'S'].every((c, i) => this.readGrid(pos.add(dir.times(i))) === c);

  private iterateGrid = (callback: (pos: Vector) => void) => {
    for (let x = 0; x < this.grid[0].length; x++) {
      for (let y = 0; y < this.grid.length; y++) {
        callback(new Vector(x, y));
      }
    }
  }

  private parseGrid = () => this.input.map(l => l.split(''));
}

