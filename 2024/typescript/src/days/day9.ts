import { Day, sum } from "../aoc";

export class Day9 extends Day {
  day = (): number => 9;
  part1 = () => checksum(compress(loadMemory(this.input[0])))
  part2 = () => checksum(compressCompleteBlocks(loadMemory(this.input[0])))
}

const loadMemory = (input: string): string[] =>
  input.split('')
    .flatMap((n, i) => i % 2 === 1
      ? repeatElement('.', Number(n))
      : repeatElement(`${i / 2}`, Number(n))
    )

const compress = (mem: string[]): string[] => {
  const workingMem = [...mem];
  for (let dest = 0, src = workingMem.length - 1; dest < src;) {
    if (workingMem[src] === '.') {
      src--;
      continue;
    }

    if (workingMem[dest] === '.') {
      workingMem[dest] = workingMem[src];
      workingMem[src] = '.';
      dest++;
      src--;
      continue;
    }

    dest++;
  }

  return workingMem;
}

const repeatElement = (element: string, count: number): string[] => Array.from({ length: count }).map(() => element);

const checksum = (mem: string[]): number => {
  return mem.map((n, i) => n === '.' ? 0 : Number(n) * i).reduce(sum);
}

const compressCompleteBlocks = (mem: string[]): string[] => {
  const workingMem = [...mem];

  let currentFileId = null;
  let lastUsedIndex = workingMem.length - 1;

  for (let src = workingMem.length - 1; (currentFileId === null || currentFileId >= 0) && src >= 0;) {
    if (workingMem[src] === '.') {
      src--;
      continue;
    }

    currentFileId ??= Number(workingMem[src]);

    if (currentFileId < 0) break;

    const lastIndex = workingMem.lastIndexOf(currentFileId.toString(), lastUsedIndex);
    lastUsedIndex = lastIndex;
    const firstIndex = workingMem.indexOf(currentFileId.toString(), lastIndex - 9);
    const length = lastIndex - firstIndex + 1;

    const newIndex = findEmptySpaceBeforeCurrent(workingMem, length, firstIndex);

    if(newIndex === NO_EMPTY_SPACE_FOUND) {
      currentFileId--;
      src -= length;
      continue;
    }

    for (let i = 0; i < length; i++) {
      workingMem[newIndex + i] = workingMem[firstIndex + i];
      workingMem[firstIndex + i] = '.';
    }

    src -= length;
    currentFileId--;
  }

  return workingMem;
}

const findEmptySpaceBeforeCurrent = (mem: string[], length: number, currentLocation: number): number => {

  let emptyStartIndex = null;
  let size = 0;

  for (let i = 0; i < currentLocation; i++) {
    if (mem[i] === '.') {
      if (emptyStartIndex === null) {
        emptyStartIndex = i;
        size = 1;
        if(size >= length) return emptyStartIndex;
        continue;
      }
      size++;
      if(size >= length) return emptyStartIndex;
      continue;
    }

    emptyStartIndex = null;
    size = 0;
  }

  return NO_EMPTY_SPACE_FOUND;
}

const NO_EMPTY_SPACE_FOUND = -1;