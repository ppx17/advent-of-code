import { Day, product, sum, Vec, Vector } from "../aoc";

export class Day14 extends Day {
  day = (): number => 14;
  part1 = () => {
    let robots = this.input.map(parseRobot);

    for (let i = 0; i < 100; i++) {
      robots = robots.map(r => moveRobot(r, mapSize));
    }

    const counts = countMap(robots, mapSize);
    const quadrants = splitQuadrants(counts, mapSize);

    return quadrants.reduce(product, 1);
  }
  part2 = () => {
    let robots = this.input.map(parseRobot);
    const minScore = robots.length * 2; // On average, all robots should have 1 neighbor minimum.

    for (let second = 1; second < 100_000; second++) {
      robots = robots.map(r => moveRobot(r, mapSize));

      const countedMap = countMap(robots, mapSize);
      const score = treeScore(countedMap, mapSize, robots);
      if (score >= minScore) {
        if (process.env.RENDER === 'true') {
          console.log(`\n\n\n\n`);
          console.log(`Round ${second}, score: ${score} / ${robots.length}`);
          console.log(renderMap(countedMap));
        }
        return second;
      }
    }
  }
}

const mapSize = new Vector(101, 103);

type Robot = {
  p: Vec;
  v: Vec;
}

const parseRobot = (input: string): Robot => {
  const m = input.match(/p=(?<px>-?\d+),(?<py>-?\d+) v=(?<vx>-?\d+),(?<vy>-?\d+)/)
  if (!m) throw new Error(`Could not parse robot: ${input}`);

  return {
    p: new Vector(Number(m.groups!.px), Number(m.groups!.py)),
    v: new Vector(Number(m.groups!.vx), Number(m.groups!.vy)),
  }
}

const moveRobot = ({ v, p }: Robot, mapSize: Vector): Robot => {
  return {
    p: p.add(v).add(mapSize).mod(mapSize),
    v,
  };
};

const countMap = (robots: Robot[], mapSize: Vector): number[][] => {
  const map = Array.from({ length: mapSize.y }, () => Array.from({ length: mapSize.x }, () => 0));
  robots.forEach(({ p }) => map[p.y][p.x]++);
  return map;
}

const renderMap = (countedMap: number[][]): string =>
  countedMap.map(l => l.map(c => c > 0 ? '#' : ' ').join('')).join('\n');

const windDirections = [Vector.north(), Vector.east(), Vector.south(), Vector.west()];

const treeScore = (map: number[][], mapSize: Vector, robots: Robot[]): number =>
  // In a drawing, you usually don't have random illuminated pixels, but rather, they tend to form lines
  // As such, the assumption is that most robots (pixels) should have at least one illuminated neighbor
  robots
    .map(robot => windDirections
      .map(d => robot.p.add(d))
      .filter(p => (map[p.y]?.[p.x] ?? 0) > 0)
      .length
    )
    .reduce(sum);

const splitQuadrants = (map: number[][], mapSize: Vector): number[] => {
  const quadrants = Array.from({ length: 4 }, () => 0);
  const centerPoint = mapSize.divide(2).floor();
  for (let y = 0; y < mapSize.y; y++) {
    for (let x = 0; x < mapSize.x; x++) {
      if (y === centerPoint.y || x === centerPoint.x) continue;
      const qX = x > centerPoint.x ? 1 : 0;
      const qY = y > centerPoint.y ? 1 : 0;
      quadrants[qX + qY * 2] += map[y][x];
    }
  }
  return quadrants;
}