import { AStar, AStarNode, Day, NodeGenerator, Vec, Vector } from "../aoc";

export class Day16 extends Day {
  private p1Cache: number;
  day = (): number => 16;
  part1 = () => this.p1Cache ??= this.findBestRoute()
  part2 = () => {
    const maxCost = this.part1();

    // now that we have the cheapest route, it might not be too expensive to do a breadth-first search to find all the routes of the same cost

    const { map, start, finish } = parseMap(this.input);

    type PossibleRoute = {
      reindeer: Reindeer,
      cost: number,
      visited: Set<string>,
    }

    const queue: PossibleRoute[] = [];

    const firstRoute: PossibleRoute = {
      reindeer: new Reindeer(start, Vector.east()),
      cost: 0,
      visited: new Set<string>([start.serialize()]),
    };

    const hasBeenVisitedAtCost = new Map<string, number>();

    queue.push(firstRoute);

    const cheapestRoutes: PossibleRoute[] = [];

    for(let i = 0; queue.length > 0; i++) {
      const current = queue.shift();
      if (current.cost > maxCost) continue;
      if (current.reindeer.pos.is(finish)) {
        if (current.cost === maxCost) {
          cheapestRoutes.push(current);
        }
        continue;
      }
      const serializeReindeer = (r: Reindeer) => r.pos.serialize() + ',' + r.dir.serialize();
      if(hasBeenVisitedAtCost.has(serializeReindeer(current.reindeer))) {
        const previousCost = hasBeenVisitedAtCost.get(serializeReindeer(current.reindeer));
        if (previousCost < current.cost) continue;
      }
      hasBeenVisitedAtCost.set(serializeReindeer(current.reindeer), current.cost);

      const { pos, dir } = current.reindeer;

      ([
        [new Reindeer(pos.add(dir), dir), 1], // "They can move forward one tile at a time"
        [new Reindeer(pos.add(dir.rotateLeft()), dir.rotateLeft()), 1001], // "They can also rotate clockwise or counterclockwise 90 degrees at a time"
        [new Reindeer(pos.add(dir.rotateRight()), dir.rotateRight()), 1001],
      ])
        .filter(([r, _]: [Reindeer, number]) => map[r.pos.y]?.[r.pos.x] === '.')
        .filter(([r, _]: [Reindeer, number]) => !current.visited.has(this.placeId(r.pos.x, r.pos.y)))
        .filter(([_, c]: [Reindeer, number]) => current.cost + c <= maxCost)
        .forEach(([r, c]: [Reindeer, number]) => {
          if(hasBeenVisitedAtCost.has(serializeReindeer(r)) && hasBeenVisitedAtCost.get(serializeReindeer(r)) < current.cost + c) return;
          const visited = new Set(current.visited);
          visited.add(this.placeId(r.pos.x, r.pos.y));
          queue.push({ reindeer: r, cost: current.cost + c, visited });
        });
    }

    const tilesWithGoodView = new Set<string>();
    cheapestRoutes.forEach(({ visited }) => visited.forEach(v => tilesWithGoodView.add(v)));

    return tilesWithGoodView.size;
  }

  private placeId = (x: number, y: number): string => `${x}:${y}`;

  private findBestRoute() {
    const { map, start, finish } = parseMap(this.input);
    const aStar = new AStar(new ReindeerMazeNodeGenerator(map));

    return [Vector.north(), Vector.east(), Vector.south(), Vector.west()]
      .map(finishDir  => aStar.run(new Reindeer(start, Vector.east()).aStarNode(), new Reindeer(finish, finishDir).aStarNode()))
      .map(route => route[route.length - 1]?.G ?? Infinity)
      .reduce((a, b) => Math.min(a, b), Infinity);
  }
}

type Map = ('.' | '#')[][];

const parseMap = (input: string[]): { map: Map, start: Vec, finish: Vec } => {
  let start: Vec;
  let finish: Vec;
  const m = input.map((l, y) => l.split('').map((c, x) => {
    if (c === 'S') {
      start = new Vector(x, y);
      return '.';
    }
    if (c === 'E') {
      finish = new Vector(x, y);
      return '.';
    }
    return c;
  }));

  return { map: m as Map, start, finish };
}

class Reindeer {
  public pos: Vec;
  public dir: Vec;

  constructor(pos: Vec, dir: Vec) {
    this.pos = pos;
    this.dir = dir;
  }

  aStarNode(parent?: AStarNode): AStarNode {
    return new AStarNode(`${this.pos.serialize()},${this.dir.serialize()}`, parent);
  }

  static fromAStarNode(node: AStarNode): Reindeer {
    const [pos, dir] = node.id.split(',');
    return new Reindeer(Vector.deserialize(pos), Vector.deserialize(dir));
  }
}

class ReindeerMazeNodeGenerator implements NodeGenerator {
  constructor(private map: Map) {
  }

  calculateEstimatedCost(a: AStarNode, b: AStarNode) {
    return Reindeer.fromAStarNode(a).pos.manhattan(Reindeer.fromAStarNode(b).pos);
  }
  calculateRealCost(a: AStarNode, b: AStarNode) {
    const ra = Reindeer.fromAStarNode(a);
    const rb = Reindeer.fromAStarNode(b);

    return this.distanceCost(ra, rb) + this.rotationalCost(ra, rb);
  }
  generateAdjacentNodes(node: AStarNode): AStarNode[] {
    const current = Reindeer.fromAStarNode(node);
    return [
      new Reindeer(current.pos.add(current.dir), current.dir), // "They can move forward one tile at a time"
      new Reindeer(current.pos, current.dir.rotateLeft()), // "They can also rotate clockwise or counterclockwise 90 degrees at a time"
      new Reindeer(current.pos, current.dir.rotateRight()),
    ]
      .filter(r => this.map[r.pos.y]?.[r.pos.x] === '.')
      .map(r => r.aStarNode(node));
  }

  private distanceCost(a: Reindeer, b: Reindeer): number {
    return a.pos.manhattan(b.pos);
  }

  private rotationalCost(a: Reindeer, b: Reindeer): number {
    if (a.dir.is(b.dir)) return 0;
    if (a.dir.rotateRight().is(b.dir)) return 1000;
    if (a.dir.rotateLeft().is(b.dir)) return 1000;
    return 2000;
  }
}