import { Day, sum } from "../aoc";

export class Day7 extends Day {
  day = (): number => 7;
  part1 = () =>
    this.input
      .map(parseEquation)
      .filter(equationCanBeSolved(applyPlus, applyTimes))
      .map(equation => equation.testValue)
      .reduce(sum, 0);
  part2 = () =>
    this.input
      .map(parseEquation)
      .filter(equationCanBeSolved(applyPlus, applyTimes, applyConcat))
      .map(equation => equation.testValue)
      .reduce(sum, 0);
}

type Equation = {
  testValue: number;
  numbers: number[];
}

const parseEquation = (input: string): Equation => {
  const [testValue, numbers] = input.split(': ');
  return { testValue: Number(testValue), numbers: numbers.split(/\s+/).map(Number) };
}

const equationCanBeSolved = (...solvers: Solver[]) =>
  (equation: Equation): boolean => {
    if (equation.numbers.length === 1) return equation.numbers[0] === equation.testValue;
    if (equation.testValue < equation.numbers[0]) return false;

    return solvers.some(solver => equationCanBeSolved(...solvers)(solver(equation)));
  }

type Solver = (equation: Equation) => Equation;

const applyPlus: Solver = (equation: Equation): Equation => {
  const [first, second, ...rest] = equation.numbers;
  return { testValue: equation.testValue, numbers: [first + second, ...rest] };
}

const applyTimes: Solver = (equation: Equation): Equation => {
  const [first, second, ...rest] = equation.numbers;
  return { testValue: equation.testValue, numbers: [first * second, ...rest] };
}

const applyConcat: Solver = (equation: Equation): Equation => {
  const [first, second, ...rest] = equation.numbers;
  return { testValue: equation.testValue, numbers: [Number(`${first}${second}`), ...rest] };
}
