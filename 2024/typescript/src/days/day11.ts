import { Day, sum } from "../aoc";

export class Day11 extends Day {
  day = (): number => 11;
  part1 = () => this.transformTimes(25);
  part2 = () => this.transformTimes(75);
  private transformTimes = (times: number) => this.getStones().map(s => transformStone(s, times)).reduce(sum);
  private getStones = () => this.input[0].split(/\s+/).map(Number);
}

const memory = new Map<string, number>();

const transformStone = (stone: number, times: number): number => {
  if(times === 0) return 1; // If we cannot transform, this stone will always be 1
  const k = `${stone}:${times}`;
  if(memory.has(k)) return memory.get(k);
  if(stone === 0) return withMemory(k, () => transformStone(1, times - 1));
  if(String(stone).length % 2 === 0) {
    const s = String(stone);

    return withMemory(k, () =>  [s.substring(0, s.length / 2), s.substring(s.length / 2)]
      .map(Number)
      .map(stone => transformStone(stone, times - 1))
      .reduce(sum, 0));
  }

  return withMemory(k, () => transformStone(stone * 2024, times - 1));
}

const withMemory = (key: string, fn: () => number): number => {
  const res = fn();
  memory.set(key, res);
  return res;
}