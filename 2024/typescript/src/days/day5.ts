import { Day, sum } from "../aoc";

export class Day5 extends Day {
  day = (): number => 5;
  part1 = () => {
    const [rules, updates] = parseInput(this.input);
    const ruleIndex = indexRules(rules);

    return updates
      .filter(update => isValidUpdate(update, ruleIndex))
      .map(middleElement)
      .reduce(sum);
  }
  part2 = () => {
    const [rules, updates] = parseInput(this.input);
    const ruleIndex = indexRules(rules);

    return updates
      .filter(update => !isValidUpdate(update, ruleIndex))
      .map((update) => sortUpdate(update, ruleIndex))
      .map(middleElement)
      .reduce(sum);
  }
}

type Rule = [number, number];
type RuleIndex = Record<number, Rule[]>;
type Update = number[];

const parseInput = (input: string[]): [Rule[], Update[]] => {
  const rules: Rule[] = [];
  const updates: Update[] = [];

  for (const line of input) {
    if (line === '') continue;
    if (line.includes('|')) {
      rules.push(line.split('|', 2).map(Number) as Rule);
      continue;
    }
    updates.push(line.split(',').map(Number));
  }

  return [rules, updates];
}

const indexRules = (rules: Rule[]): RuleIndex => {
  const indexedRules = {};
  rules.forEach((rule) => {
    indexedRules[rule[0]] ??= [];
    indexedRules[rule[0]].push(rule);
  });
  return indexedRules;
}

const middleElement = (arr: number[]): number => arr[Math.floor(arr.length / 2)];

const isValidUpdate = (update: Update, ruleIndex: RuleIndex) => {
  const seen = new Set<number>();

  for (const n of update) {
    for (const ruleForN of (ruleIndex[n] ?? [])) {
      if (seen.has(ruleForN[1])) return false;
    }
    seen.add(n);
  }

  return true;
}

const sortUpdate = (update: Update, ruleIndex: RuleIndex) =>
  update.sort((a, b) => compare(a, b, ruleIndex));

const compare = (a: number, b: number, ruleIndex: RuleIndex) =>
  (ruleIndex[a] ?? [])
    .some(r => r[1] === b) ? -1 : 1