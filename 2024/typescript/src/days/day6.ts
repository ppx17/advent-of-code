import { Day, Vec, Vector } from "../aoc";

export class Day6 extends Day {
  day = (): number => 6;
  part1 = () => travel(this.getMap(), findGuard(this.getMap()), Vector.north()).visitedLocations
  part2 = () => {
    const start = findGuard(this.getMap());
    const potentialSpots = travel(this.getMap(), start, Vector.north()).visitedVectors;

    let loopCounter = 0;
    const uniqueSpots = new Set<string>();
    for (const spot of potentialSpots) {
      if (spot.is(start)) continue;
      if (uniqueSpots.has(spot.serialize())) continue;
      uniqueSpots.add(spot.serialize());
      const map = this.getMap();
      map[spot.y][spot.x] = '#';
      const result = travel(map, start, Vector.north());
      loopCounter += Number(result.loopDetected);
    }

    return loopCounter;
  }

  private getMap = (): string[][] => this.input.map(l => l.split(''));
}

const findGuard = (map: string[][]): Vec => {
  for (let y = 0; y < map.length; y++) {
    for (let x = 0; x < map[y].length; x++) {
      if (map[y][x] === '^') return new Vector(x, y);
    }
  }
  throw new Error('No guard found');
};

type TravelResult = {
  visitedVectors: Vector[];
  loopDetected: boolean;
  visitedLocations: number;
}

const travel = (map: string[][], pos: Vec, dir: Vec): TravelResult => {
  const visitedLocations = new Set<string>();
  const loopDetection = new Set<string>();
  const visitedVectors = [pos];
  const posDir = (pos: Vec, dir: Vec) => `${pos.serialize()},${dir.serialize()}`;
  const atPos = (v: Vec) => map[v.y]?.[v.x];
  const mapSize = new Vector(map.length - 1, map[0].length - 1);
  const inside = (v: Vec) => v.within(mapSize);
  const isObstacle = (v: Vec) => atPos(v) === '#';
  visitedLocations.add(pos.serialize());
  loopDetection.add(posDir(pos, dir));

  while (true) {
    let next = pos.add(dir);
    if (!inside(next) || loopDetection.has(posDir(next, dir))) {
      return {
        visitedVectors,
        loopDetected: loopDetection.has(posDir(next, dir)),
        visitedLocations: visitedLocations.size
      };
    }

    if (isObstacle(next)) {
      dir = dir.rotateRight();
      continue;
    }

    if(!visitedLocations.has(next.serialize())) visitedVectors.push(next);
    visitedLocations.add(next.serialize());
    loopDetection.add(posDir(next, dir));

    pos = next;
  }
}
