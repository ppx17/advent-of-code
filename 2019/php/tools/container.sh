
TAG="aoc-php:7.4-cli"


# Get repo root
repo_root=$(git rev-parse --show-toplevel)

full_path_tools=$(realpath "$(dirname "$0")")
full_path=$(realpath "${full_path_tools}/..")
relative_path=$(realpath --relative-to="${repo_root}" "${full_path}")

IMAGES=$(docker image ls -q "${TAG}")

if [[ -z "${IMAGES}" ]]; then
    docker build -t "${TAG}" "${full_path_tools}"
fi

echo "Running ${TAG} in ${relative_path}..."

docker run --pull=never --volume="${repo_root}:/opt/advent" \
            -w "/opt/advent/${relative_path}" \
            --env PHP_MEMORY_LIMIT=-1 \
            -ti \
            "${TAG}" \
            "$@"
echo ""
